# -*- coding: utf-8 -*-
"""OVOK interface base class definition."""

import logging
from database import db_wrapper as db
from context.contextmanager import ContextManager
from configuration.configuration import Configuration

logger = logging.getLogger(__name__)


class OVOKInterface(object):
    """
    Defines a shared interface for different platforms.

    The OVOKInterface provides Course object related functionality, but
    platform specific API calls are implemented in subclasses.
    """

    def __init__(self):
        """Construct an OVOKInterface instance."""
        self._platform = None
        self._contextmanager = ContextManager()
        self._config = Configuration()

    @property
    def config(self):
        """
        Return current configuration for the client.

        Returns:
            Configuration: Current configuration.
        """
        return self._config

    @config.setter
    def config(self, new_config):
        if not isinstance(new_config, Configuration):
            raise TypeError("Provided value {} is not of expected type "
                            " {}"
                            .format(str(type(new_config)),
                                    str(type(Configuration))))
        self._config = new_config

        # Set database information for contextmanager
        if "database" in self._config and self._config["database"]:
            self._contextmanager.databases = \
                [db for db in self.config["database"]]

    def get_platform(self):
        """Return current platform."""
        return self._platform

    def _require_context(func):
        """Enforce course context requirement on a function."""
        def _decorate(self, *args, **kwargs):
            """Decorate the function."""
            if not self._contextmanager.context:
                # TODO: Raise a specific ContextNotSetException
                raise Exception("Course context not set.")
            return func(self, *args, **kwargs)
        return _decorate

    def set_context(self, context):
        """
        Set the course to work with.

        Args:
            context(CourseContext): Course context to work with.
        """
        self._contextmanager.context = context

    def get_context_course(self, course):
        """
        Get a context for the specified course.

        Args:
            course(Course): Course to search context for.

        Returns:
            list(Context): List of contexts matching the course.
        """
        return self._contextmanager.get_context_course(course)

    def get_contexts(self):
        """Return all available course contexts."""
        return self._contextmanager.get_contexts()

    def get_context_hosts(self):
        """Return a list of SQLAclhemy URLs for all configured hosts."""
        return self._contextmanager.get_hosts()

    def create_context(self, course, db_uri):
        """
        Create a new context.

        Args:
            course(Course): Course to create a context for.
            db_uri(str): Database uri for connecting.

        Returns:
            Context: The created context object.
        """
        return self._contextmanager \
            .create_context(course, db_uri)

    def query(self, cls):
        """
        Query class objects.

        Args:
            cls(Class): Which class to query.

        Returns:
            list[Object]: List of objects queried.
        """
        return db.get_class_objects(self._contextmanager.session(), cls)

    def execute(self, clause, **kwargs):
        """
        Run compiled SQL strings and other executable queries.

        Args:
            clause(object): See SQLAlchemy documentetion execute(). Compiled
                            SQL queries and Query objects.

        Returns:
            ResultProxy: ResultProxy for the query.
        """
        return db.execute(self._contextmanager.session(), clause, **kwargs)

    def session(self):
        """
        Return current database session for custom queries.

        Custom query example:
            ovok = OVOKInterface()
            session = ovok.session()
            result_list = session.query(Student).all()
        """
        return self._contextmanager.session()

    @_require_context
    def merge(self, obj):
        """
        Merge an object.

        Args:
            obj(Object): Object to merge.

        Returns:
            Object: The merged object.
        """
        return db.merge(self._contextmanager.session(), obj)

    @_require_context
    def merge_bulk(self, objs):
        """
        Merge an object.

        Args:
            objs(list(Object)): List objects to merge.

        Returns:
            list(Object): List of merged objects.
        """
        return db.merge_bulk(self._contextmanager.session(), objs)

    @_require_context
    def delete(self, obj):
        """
        Delete an object.

        Args:
            obj(Object): Object to delete.
        """
        return db.delete(self._contextmanager.session(), obj)
