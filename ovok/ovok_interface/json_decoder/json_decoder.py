"""Module for decoding JSON into OVOK ORM objects."""

import datetime
import json_decoder_bitbucket
import logging
import pytz

from tzlocal import get_localzone
from pprint import pprint
from ..database.ovok_orm_definitions import Course
from ..database.ovok_orm_definitions import Person
from ..database.ovok_orm_definitions import Employee
from ..database.ovok_orm_definitions import Repository
from ..database.ovok_orm_definitions import Student
from ..database.ovok_orm_definitions import StudentGroup
from ..database.ovok_orm_definitions import Username
from ..database.ovok_orm_definitions import Assesment
from ..database.ovok_orm_definitions import Assignment
from ..database.ovok_orm_definitions import Submission
from ..database.ovok_orm_definitions import Feedback
from ..database.ovok_orm_definitions import Grading


# Setup logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# TODO: Transform into a class and inherit platform specific handlers from this
# "baseclass"?
# TODO: How to handle platform specific objects with __type__ in decode()?
# TODO: If this module is not converted into a class, implement a
# _mappings_platform() which holds the logic for platform specific decoding.

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


def _mappings_decoding():
    """
    Return functions used to decode OVOK ORM objects.

    Returns:
        dict: Dictionary of functions used to decode JSON into objects.
              The dictionary key is provided as a lowercase string of class
              name.
    """
    return {
        Student.__name__.lower(): _decode_student,
        Employee.__name__.lower(): _decode_employee,
        StudentGroup.__name__.lower(): _decode_student_group,
        Course.__name__.lower(): _decode_course,
        Repository.__name__.lower(): _decode_repository,
        Username.__name__.lower(): _decode_username,
        Assesment.__name__.lower(): _decode_assesment,
        Assignment.__name__.lower(): _decode_assignment,
        Submission.__name__.lower(): _decode_submission,
        Feedback.__name__.lower(): _decode_feedback,
        Grading.__name__.lower(): _decode_grading
    }


def dump_object(obj):
    """Pretty print the provided object."""
    if "__dict__" in dir(obj):
        pprint(vars(obj))


def combine_list_of_dictionaries(list_of_dictionaries):
    """
    Return a dictionary merged from a list of dictionaries.

    Returns:
        dict: Merged dictionary.
    """
    return dict((key, value) for dic in list_of_dictionaries
                for (key, value) in dic.items())


def decode(obj, **kwargs):
    """
    Decode JSON into OVOK ORM objects.

    This function should be used as a JSON decoder hook.
    The function also assumes that the json input holds a "__type__" attribute.
    If you cannot provide "__type__" with the json object, see the function
    decode_as_object().

    Example:
        orm_objects = json.loads(json_string, object_hook=json_decoder.decode)

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        object: Object instance built from the provided dictionary.
    """
    # The json class representation has to have a __type__ field
    # to identify which object is built from it
    if "__type__" in obj:

        # Retrieve dictionary containing which functions are to be used in
        # decoding different classes.
        mappings = _mappings_decoding()
        obj_type = obj["__type__"].lower()

        if obj_type in mappings:
            return mappings[obj_type](obj, **kwargs)
        else:
            logger.warning("No decoding mapping found for {}"
                           .format(obj_type))
    else:
        logger.warning("No __type__ attribute found from input {}"
                       .format(obj))

    return obj


def decode_as_object(obj, obj_class, platform=None):
    """
    Decode JSON into specific objects.

    Args:
        obj(str): JSON string you wish to decode.
        obj_class(Class): Class you wish to decode JSON into.
        platform(str): Specific platform for object.

    Returns:
        object: Object instance built from the provided JSON string.
    """
    result = None
    if not platform:
        mappings = _mappings_decoding()
        classname = obj_class.__name__.lower()
        result = mappings[classname](obj) if classname in mappings else None

    elif platform.lower() == "bitbucket":
        result = json_decoder_bitbucket.decode_as_object(obj, obj_class)
    else:
        logger.warning("Unknown platform: {}".format(platform))

    return result


def _decode_repository(obj):
    """
    Build a Repository object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Repository instance built from the provided dictionary.
    """
    repository = Repository(obj["uuid"],
                            obj["owner"],
                            obj["description"],
                            obj["name"],
                            obj["platform"])

    return repository


def _decode_username(obj):
    """
    Build a Username object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Username instance built from the provided dictionary.
    """
    username = Username(obj["username"], obj["platform"])

    return username


def _decode_person(obj):
    """
    Build a Person object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Person instance built from the provided dictionary.
    """
    person = Person(obj["firstnames"],
                    obj["lastname"],
                    obj["email"])

    for username in obj["usernames"]:
        person.usernames.append(username)

    return person


def _decode_student(obj):
    """
    Build a Student object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Student instance built from the provided dictionary.
    """
    student = Student(obj["firstnames"],
                      obj["lastname"],
                      obj["student_id"],
                      obj["email"])

    if "usernames" in obj:
        for username in obj["usernames"]:
            student.repositories.append(username)

    if "submissions" in obj:
        for submission in obj["submissions"]:
            student.feedbacks.append(submission)

    return student


def _decode_employee(obj):
    """
    Build an Employee object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Employee instance built from the provided dictionary.
    """
    employee = Employee(obj["firstnames"],
                        obj["lastname"],
                        obj["email"])

    if "usernames" in obj:
        for username in obj["usernames"]:
            employee.usernames.append(username)

    if "feedbacks" in obj:
        for feedback in obj["feedbacks"]:
            employee.feedbacks.append(feedback)

    return employee


def _decode_student_group(obj):
    """
    Build a StudentGroup object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: StudentGroup instance built from the provided dictionary.
    """
    group = StudentGroup()
    if "id" in obj:
        group.id = obj["identifier"]

    if "students" in obj:
        for student in obj["students"]:
            group.students.append(student)

    if "repositories" in obj:
        for repository in obj["repositories"]:
            group.repositories.append(repository)

    if "primary_repository" in obj:
        group.primary_repository = obj["primary_repository"]

    return group


def _decode_course(obj):
    """
    Build a Course object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Course instance built from the provided dictionary.
    """
    course = Course(obj["code"],
                    obj["prefix"],
                    obj["name"],
                    obj["year"])

    if "platforms" in obj:
        course.platforms = obj["platforms"]

    if "assignments" in obj:
        course.assignments = obj["assignments"]

    return course


def _decode_assesment(obj):
    """
    Build an Assesment object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Assesment instance built from the provided dictionary.
    """
    assesment = Assesment(obj["student"],
                          obj["assignment"])

    if "feedbacks" in obj:
        for feedback in obj["feedback"]:
            assesment.feedbacks.append(feedback)

    if "grading" in obj:
        assesment.grading = obj["grading"]

    if "submission" in obj:
        assesment.submission = obj["submission"]

    if "finished" in obj:
        assesment.finished = obj["finished"]

    return assesment


def _decode_assignment(obj, **kwargs):
    """
    Build an Assignment object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Assignment instance built from the provided dictionary.
    """
    assignment = Assignment(obj["tag"],
                            obj["description"],
                            _string_to_datetime(obj["deadline"]))

    if "feedbacks" in obj:
        for feedback in obj["feedback"]:
            assignment.feedbacks.append(feedback)

    if "grading" in obj:
        assignment.grading = obj["grading"]

    if "submission" in obj:
        assignment.submission = obj["submission"]

    if "finished" in obj:
        assignment.finished = obj["finished"]

    if "not_before" in obj and len(obj["not_before"]) > 0:
        assignment.finished = _string_to_datetime(obj["not_before"])

    if "root_folder" in obj:
        assignment.root_folder = obj["root_folder"]

    if "jenkins_job" in obj:
        assignment.jenkins_job = obj["jenkins_job"]

    if "course_code" in kwargs:
        assignment.course_code = kwargs["course_code"]

    if "course_year" in kwargs:
        assignment.course_year = kwargs["course_year"]

    if "required_folders" in kwargs \
       and hasattr(assignment, "required_folders"):
        assignment.required_folders = kwargs["required_folders"]

    return assignment


def _decode_submission(obj):
    """
    Build a Submission object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Submission instance built from the provided dictionary.
    """
    submission = Submission(obj["assignment"],
                            obj["assesment"],
                            obj["repository"],
                            _string_to_datetime(obj["date"]))

    if "location" in obj:
        submission.location = obj["location"]

    if "feedbacks" in obj:
        for feedback in obj["feedback"]:
            submission.feedbacks.append(feedback)

    if "grading" in obj:
        submission.grading = obj["grading"]

    return submission


def _decode_feedback(obj):
    """
    Build a Feedback object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Feedback instance built from the provided dictionary.
    """
    feedback = Feedback(obj["owner"],
                        obj["submission"],
                        obj["content"],
                        _string_to_datetime(obj["date"]))

    return feedback


def _decode_grading(obj):
    """
    Build a Grading object from dictionary.

    Args:
        obj(dict): Dictionary to build instance from.

    Returns:
        Course: Grading instance built from the provided dictionary.
    """
    feedback = Feedback(obj["assignment"],
                        obj["creator"],
                        obj["submission"],
                        _string_to_datetime(obj["date"]),
                        obj["points"])

    return feedback


def _datetime_to_string(datetime_obj):
    """
    Build a datetime object from string.

    Args:
        str(str): String containing a datetime.

    Returns:
        datetime: Converted datetime object.
    """
    # TODO: Implement if needed
    raise Exception("Not implemented")


def _string_to_datetime(string_datetime):
    """
    Convert string to datetime object.

    The time string is read and localized as local time. It is then converted
    as UTC and the timezone info is removed from the datetime.

    Args:
        datetime_obj(datetime): datetime object to convert.

    Returns:
        str: Converted (naive) datetime object.
    """
    # Get naive datetime without specified timezone
    naive_datetime = datetime.datetime.strptime(string_datetime,
                                                DATETIME_FORMAT)

    # Create local timezone references and localize the naive datetime to UTC
    local_timezone = get_localzone()
    local_datetime = local_timezone.localize(naive_datetime, is_dst=None)
    utc_datetime = local_datetime.astimezone(pytz.utc)

    # Remove the timezone information to make it naive again
    utc_datetime = utc_datetime.replace(tzinfo=None)

    return utc_datetime
