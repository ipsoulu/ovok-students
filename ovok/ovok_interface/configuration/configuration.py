# -*- coding: utf-8 -*-
"""Configuration class for OVOK system."""

import yaml
import logging

logger = logging.getLogger(__name__)

_smtp_conf = {
    "server": "",
    "port": "",
    "username": "",
    "password": "",
    "tls": True
}

_mysql = {
    "username": "",
    "password": "",
    "server": "",
    "port": ""
}

_database_conf = {
    "mysql": _mysql
}

_bitbucket_conf = {
    "consumer_key": "",
    "consumer_secret": "",
    "namespace": ""
}

_default_configuration = {
    "smtp:": _smtp_conf,
    "database": _database_conf,
    "bitbucket": _bitbucket_conf,
    "logging": ""
}


class Configuration(dict):
    """Configuration class."""

    DEFAULT_FILENAME = "configuration.yml"

    def __init__(self, values=None):
        super(Configuration, self).__init__(values or _default_configuration)

    def from_yaml(self, path):
        try:
            with open(path, "r") as f:
                self.update(yaml.load(f))
        except IOError:
            pass

    def from_dict(self, values):
        self.update(values)
