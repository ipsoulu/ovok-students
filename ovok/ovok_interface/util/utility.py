# -*- coding: utf-8 -*-
"""Utility functions for the OVOK system."""

import yaml
from sqlalchemy import and_
from ..database import ovok_orm_definitions as orm
from ..database import ovok_bitbucket_definitions as orm_bb
from ..emailer.emailer import Emailer
import commands as cmd
from ..core import RepositoryError


def get_students_without_groups(ovok):
    """
    Return all students without a group.

    Args:
        ovok(OVOKInterface): OVOK object to run queries with.

    Returns:
        list(Student): List of students without a group.
    """
    return ovok.session() \
        .query(orm.Student) \
        .filter(~orm.Student.groups.any()) \
        .all()


def get_groups_without_repositories(ovok):
    """
    Return all studentgroups without repositories.

    Args:
        ovok(OVOKInterface): OVOK object to run queries with.

    Returns:
        list(StudentGroup): List of studentgroups without repositories.
    """
    return ovok.session().query(orm.StudentGroup) \
        .filter(and_(~orm.StudentGroup.repositories.any())) \
        .all()


def autogroup_students(ovok, students, group_size):
    """
    Automatically assign students into groups.

    Args:

    Returns:
    """
    groups = []
    if group_size < 1:
        return groups

    for i in xrange(0, len(students), group_size):

        # Determine which students to place in to same group
        end_index = \
            i + group_size \
            if i + group_size <= len(students) \
            else len(students)

        # Create a new group and associations for all students
        group = orm.StudentGroup()
        for student in students[i:end_index]:
            group.students.append(student)

        # The group is now done
        groups.append(group)

    # Update groups to database
    return ovok.merge_bulk(groups)


def autogroup_students_cmd(ovok, students, group_size):
    """
    Automatically assign students into groups.

    Args:

    Returns:
    """
    commands = cmd.CommandList()
    if group_size > 0:

        session = ovok.session()
        for i in xrange(0, len(students), group_size):

            # Determine which students to place in to same group
            end_index = \
                i + group_size \
                if i + group_size <= len(students) \
                else len(students)

            # Create a new group and associations for all students
            group = orm.StudentGroup()

            cmd_create = cmd.CreateCommand(session, group)
            commands.append(cmd_create)
            for student in students[i:end_index]:
                cmd_join = cmd.JoinGroupCommand(session, group, student)
                commands.append(cmd_join)

    return commands


def sync_repository_permissions(ovok, repository, platform):
    """
    Return commands to synchronize repository permissions.

    Args:
        ovok(OVOKClientBitBucket): OVOK bitbucket client.
        repository(GitRepository): Repository object to synchronize.
        platfrom(str): Platform to use.

    Returns:
        list(RepositoryPermissionCommand): List of permission commands to
                                           execute.
    """
    commands = cmd.CommandList()
    session = ovok.session()

    admins = ovok.query(orm.Employee)

    try:
        permissions = ovok.get_repository_permissions(repository)
    except RepositoryError:
        raise

    if permissions:
        existing_permissions = [p.username.lower() for p in permissions]
    else:
        existing_permissions = []
    for group in repository.groups:

        for student in group.students:

            bb_username = None
            for username in student.usernames:
                if username.platform == platform:
                    bb_username = username
                    break
            else:
                continue

            if bb_username.username.lower() not in existing_permissions \
               and repository.owner.lower() != bb_username.username.lower():

                privilege = orm_bb.RepositoryPermission.WRITE
                perm = orm_bb.RepositoryPermission(bb_username.username,
                                                   privilege,
                                                   repository.name,
                                                   namespace=repository.owner)

                cmd_permission = \
                    cmd.RepositoryPermissionCommand(ovok,
                                                    perm,
                                                    session=session)

                commands.append(cmd_permission)

        for admin in admins:

            bb_username = None
            for username in admin.usernames:
                if username.platform == platform:
                    bb_username = username
                    break
            else:
                continue

            if bb_username.username.lower() not in existing_permissions \
               and repository.owner.lower() != bb_username.username.lower():

                privilege = orm_bb.RepositoryPermission.ADMIN
                perm = orm_bb.RepositoryPermission(bb_username.username,
                                                   privilege,
                                                   repository.name,
                                                   namespace=repository.owner)

                cmd_permission = \
                    cmd.RepositoryPermissionCommand(ovok,
                                                    perm,
                                                    session=session)

                commands.append(cmd_permission)

    return commands


def send_email(smtp_username, smtp_password, email, smtp_addr, smtp_port=25):
    """
    Send email through SMTP.

    Args:
        smtp_username(str): Username for SMTP.
        smtp_password(str): Password for SMTP.
        smtp_sender(str): Sender email address for SMTP.
        email(Email): The email to send.
    """
    emailer = Emailer(smtp_username,
                      smtp_password,
                      smtp_addr,
                      smtp_port=smtp_port)
    return emailer.send_email(email)


def load_config(path):
    """
    Load configurations from file.

    Args:
        path(str): Path to the file to read.

    Returns:
        dict: Loaded values as a dictionary.
    """
    try:
        return yaml.load(read_file(path))
    except IOError:
        raise


def read_file(path):
    """
    Read file contents as string.

    Args:
        path(str): Contents read as string.

    Returns:
        str: File contents as string.

    Raises:
        IOError: If file could not be read.
    """
    try:
        with open(path, "r") as f:
            return f.read()
    except IOError:
        raise
