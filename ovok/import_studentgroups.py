#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Script for importing student groups from file."""

import argparse
import os
import logging
import logging.config
from sqlalchemy import and_

from ovok_interface import utility_scripts
from ovok_interface import orm
from ovok_interface import get_client
from ovok_interface import commands as cmd
from ovok_interface import Configuration
from ovok_interface.util import utility

# Setup logging
logger = logging.getLogger(__name__)
logging.captureWarnings(True)


class DuplicateUsernameException(Exception):
    """Exception for duplicate usernames in input."""

    def __init__(self, msg, duplicates):
        """
        Construct the exception.

        Args:
            duplicates(list(str)): List of duplicate usernames.
        """
        self.duplicates = duplicates
        super(DuplicateUsernameException, self).__init__(msg)


def read_input_groups(path):
    """
    Read student groups from input file.

    Args:
        path(str): Path to the file containg student groups.

    Returns:
        list(list): Return a list of lists, where the upper level represents
                    groups and the inner level represents students within
                    groups.

    Note: The file should contain one BitBucket username per line and use blank
          lines as separators for new groups.
    """
    with open(path, "r") as f:

        index = 0
        groups = []
        for raw_line in f:

            line = raw_line.strip()

            # Blank line is the group separator
            if len(line) == 0:
                if index < len(groups):
                    index += 1
                continue
            else:
                if len(groups) <= index:
                    groups.append([])
                groups[index].append(line.lower())

        return groups


def find_duplicates(lists):
    """
    Find duplicate strings within a list of lists.

    Args:
        lists(list(list)): List of lists containing strings.

    Returns:
        generator: Generator that outputs duplicate strings found.
    """
    seen = set()
    for lst in lists:
        for item in lst:
            item = str(item).strip().lower()
            if item in seen:
                yield item
            seen.add(item)


def find_student_for_username(students, username, platform):
    """
    Return student object with specified username.

    Args:
        students(list(Student)): List of students to search.
        username(str): Username to search.
        platform(str): Platform username to search.

    Returns:
        Student: Return the student if found, else None
    """
    for student in students:
        for u in student.usernames:
            if u.platform.lower() == platform.lower() \
               and u.username == username.lower:
                return u
    return None


def filter_students(ovok, group_input, platform="bitbucket"):
    """
    Read and validate students from input.

    Args:
        ovok(OVOKInterface): OVOK client for data queries.
        group_input(list(list(str))): List of lists containing usernames.

    Returns:
        list(Student): List of students to group
    """
    # Check for duplicates
    duplicates = list(find_duplicates(group_input))
    if len(duplicates) > 0:
        raise DuplicateUsernameException("The given username list contains "
                                         "duplicates",
                                         duplicates)

    formed_groups = []
    for group_usernames in group_input:
        group_students = ovok.session() \
            .query(orm.Student) \
            .join(orm.Username) \
            .filter(and_(orm.Username.platform == platform,
                         orm.Username.username.in_(group_usernames))) \
            .all()
        if len(group_students) > 0:
            formed_groups.append(group_students)

    return formed_groups


def form_groups(session, groups):
    """
    Form groups for students.

    Args:
        session(Session): SQLAlchemy database session.
        groups(list(Student)): List of students to group.

    Returns:
        CommandList: List of commands to execute.
    """
    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()
    for grp in groups:

        # Check which groups the students already belong to, if any.
        # Only include each group once.
        members_in = []
        for student in grp:
            for sg in student.groups:
                if sg not in members_in:
                    members_in.append(sg)

        if len(members_in) == 0:

            # No student belongs to an existing group,
            # create a new group for all of them
            cmd_create = cmd.CreateCommand(session, orm.StudentGroup())
            commands.append(cmd_create)

            for student in grp:
                cmd_join = \
                    cmd.JoinGroupCommand(session, cmd_create.obj, student)
                commands.append(cmd_join)

        elif len(members_in) == 1:

            # Someone already belongs to a group, make others join it
            grp_to_join = members_in[0]
            for student in grp:

                if student not in grp_to_join.students:
                    cmd_join = \
                        cmd.JoinGroupCommand(session, grp_to_join, student)
                    commands.append(cmd_join)

        else:

            # In this situation multiple students belong to multiple groups.
            # The final grouping should be resolved by the user
            print("")
            print "else"
            print members_in

            # Multiple students
            pass

    return commands


def group_students(group_input_file_path, conf):
    """
    Import student groups.

    Args:
        group_input_file_path(str): Path to the group configuration file.
        conf(Configuration): Configuration settings for OVOK client.

    Returns:
        None.
    """
    # Read student group data from input
    group_input = read_input_groups(group_input_file_path)
    if len(group_input) == 0:
        print("Input did not contain any groups, exiting.")
        return
    else:
        print("Read {} groups from input file".format(len(group_input)))

    # Create the OVOK Interface client
    ovok = get_client()
    ovok.config = conf
    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    # Get Course from database
    course = ovok.session().query(orm.Course).one_or_none()
    if not course:
        print("Couldn't get Course instance from database, please check "
              "that you have imported the course data!")
        return

    groups_to_form = filter_students(ovok, group_input, platform="bitbucket")
    commands = form_groups(ovok.session(), groups_to_form)

    if len(commands) == 0:
        print("No actions are required, groups are as they should be")
        return

    print("The following actions are to be taken:")
    print commands.describe()

    ans = raw_input('Continue? [y/n] ')
    if ans not in (["Y", "y"]):
        print("User cancellation of script.")
        return
    else:
        commands.execute()
        ovok.session().commit()

    print("Groups created.")


if __name__ == "__main__":

    # Parse command line arguments

    parser = argparse.ArgumentParser()

    group_help_string = "Path to file containing group data for course."
    parser.add_argument("groups",
                        help=group_help_string,
                        action="store")

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    # Import student groups
    group_students(args.groups, conf)

    logger.info("End of script {}".format(__file__))
