"""Module containing the HTTPServerHandler for serving OAuth requests."""
from BaseHTTPServer import BaseHTTPRequestHandler

MESSAGE_TEMPLATE_SUCCESS = \
    """
    <!DOCTYPE html>
    <html>
    <body onload="loaded()">

        <p>Authentication succesful, this window will now try to close itself automatically...</p>

        <script>
        function loaded() {
            window.setTimeout(function() {
                do_close();
            }, 5000);
        }
        function do_close() {
            window.open('','_parent','');
            window.close();
        }
        </script>

    </body>
    </html>
    """


class HTTPServerHandler(BaseHTTPRequestHandler):
    """Handler for OAuth verifier requests."""

    # URL where to redirect OAuth requests
    REDIRECT_URL = 'http://localhost:8080/'

    # OAuth providers ###

    # Add providers with:
    # PROVIDER_OTHER = "OTHER"
    # etc...
    PROVIDER_BITBUCKET = "bitbucket"

    # Default provider used
    PROVIDER_DEFAULT = PROVIDER_BITBUCKET

    def __init__(self,
                 request,
                 address,
                 server,
                 access_key,
                 access_secret,
                 provider=PROVIDER_DEFAULT):
        """
        Construct an HTTPServerHandler.

        Args:
            request(SocketObject): HTTP request to serve.
            address(tuple): Address to server.
            server(instance): The server to serve.
        """
        # Register OAuth values
        self.app_id = access_key
        self.app_secret = access_secret
        self.provider = provider

        # Map functions per provider
        self._PARSE_TOKEN_MAPPINGS = \
            {self.PROVIDER_BITBUCKET: self.get_token_bitbucket}

        # Construct parent class
        BaseHTTPRequestHandler.__init__(self, request, address, server)

    def do_GET(self):
        """
        Do an HTTP GET.

        This function stores the OAuth access token as an attribute to the
        server. Get the token from attribute "httpServer.access_token"
        """
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        if self.provider in self._PARSE_TOKEN_MAPPINGS:

            # Parse access token
            self.server.access_token = \
                self._PARSE_TOKEN_MAPPINGS[self.provider]()

            # Write output for the user
            if self.server.access_token:
                message = MESSAGE_TEMPLATE_SUCCESS
            else:
                message = "<html><h1>Error in automatic authentication. " \
                          "Please use manual authentication instead!" \
                          "</h1></html>"
            self.wfile.write(message)

    def get_token_bitbucket(self):
        """
        BitBucket specific OAuth token parsing for URLs.

        Returns:
            str: Parsed OAuth token from URL.
        """
        if 'oauth_verifier' in self.path:
            # TODO: Read proper key from URL
            return self.path.split('=')[1].split("&")[0]
        else:
            return None
