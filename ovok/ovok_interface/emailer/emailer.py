# -*- coding: utf-8 -*-
"""Email related classes."""

import smtplib
from smtplib import SMTPServerDisconnected
import logging

# Setup logging
logger = logging.getLogger(__name__)


class Email():
    """Email that can be sent by the Emailer."""

    def __init__(self, from_addr, to_addr, subject, body):
        """
        Construct an Email object.

        Args:
            from_addr(str): Sender email address.
            to_addr(str): Recipient addresses.
            subject(str): Email subject.
            body(str: Email body.
        """
        self.from_addr = from_addr
        self.to_addr = to_addr
        self.subject = subject
        self.body = body

    def get_message(self):
        """
        Return the email as text for SMTP.

        Returns:
            str: Email as text.
        """
        field_from = "From: {}".format(self.from_addr)
        field_subject = "Subject: {}".format(self.subject)
        field_body = self.body

        msg = "\r\n".join([
            field_from,
            field_subject,
            "",
            field_body])

        return msg


class Emailer():
    """Class for sending emails."""

    # How many times we should retry sending a single email
    _SEND_RETRY_COUNT = 3

    def __init__(self,
                 username,
                 password,
                 smtp_server_addr,
                 smtp_port=25,
                 use_tls=False):
        """
        Construct and Emailer.

        Args:
            username(str): SMTP username.
            password(str): SMTP password.

        Kwargs:
            smtp_server_addr(str): SMTP server address.
        """
        self.username = username
        self.password = password
        self.smtp_server_addr = smtp_server_addr
        self.smtp_port = smtp_port
        self.use_tls = use_tls

    def set_smtp_server_address(self, address):
        """
        Set SMTP server address.

        Args:
            address (str): Address of the server, given as <url>:<port>.
                           For example 'puru.oulu.fi:587'
        """
        self.smtp_server_add = address

    def send_email(self, email):
        """
        Send emails.

        Args:
            email (Email, list(Email)): Email object to send. Can be a list.
        """
        # TODO: Refactor this
        try:

            # Connect and authenticate to the SMTP server
            server = smtplib.SMTP(self.smtp_server_addr,
                                  self.smtp_port)
            # server.set_debuglevel(True)

            if self.use_tls is True:
                logger.debug("Starting SMTP with TLS")
                server.ehlo()
                server.starttls()
                server.login(self.username, self.password)
            logger.debug("SMTP connection established.")

            # Convert single emails to lists
            if type(email) is not list:
                email = [email]

            mailcount = -1

            # Send all emails in the list
            for mail in email:

                mailcount += 1

                # Retry sending for a certain times
                for retry_count in xrange(self._SEND_RETRY_COUNT):

                    try:

                        server.sendmail(mail.from_addr,
                                        mail.to_addr,
                                        mail.get_message())
                        logger.info("Email sent to {}"
                                    .format(mail.to_addr))

                        # Break from the retry loop if the email was sent
                        break

                    except SMTPServerDisconnected as e:
                        # We have propably reached the send limit

                        # Reset connection and try again
                        try:
                            logger.error("SMTP connection disconnected, "
                                         "error: {}".format(repr(e)))
                            logger.info("Trying to reconnect SMTP "
                                        "connection...")
                            server = smtplib.SMTP(self.smtp_server_addr)
                            # server.set_debuglevel(1)
                            server.ehlo()
                            server.starttls()
                            server.login(self.username, self.password)
                            logger.info("SMTP connection established")
                        except Exception as e:
                            logger.error("Couldn't restart SMTP connection, "
                                         "error: {err}"
                                         .format(err=repr(e)))

                    except Exception as e:
                        logger.exception("Error sending email from {frm} to "
                                         "{to} (retry:{curr}/{max})"
                                         .format(frm=mail.from_addr,
                                                 to=mail.to_addr,
                                                 curr=retry_count,
                                                 max=self._SEND_RETRY_COUNT,
                                                 err=repr(e)))

                else:
                    raise Exception("Error sending email from {frm} to "
                                    "{to} (retry:{curr}/{max})"
                                    .format(frm=mail.from_addr,
                                            to=mail.to_addr,
                                            curr=retry_count,
                                            max=self._SEND_RETRY_COUNT,
                                            err=repr(e)))
        except Exception as e:
            logger.exception("General error sending email")
            raise
        finally:
            server.quit()
