#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for downloading student assignments from Bitbucket."""

import logging
import logging.config
import argparse
import os
import ovok_interface
import getpass
from ovok_interface import orm
from ovok_interface.util import utility
from ovok_interface.util import utility_scripts
from ovok_interface import commands as cmd
from ovok_interface import Configuration

# Setup logging
logger = logging.getLogger(__name__)
logging.captureWarnings(True)


def prepare_downloads(ovok,
                      groups,
                      root_path,
                      tag,
                      username,
                      password,
                      strategy):
    """
    Create commands to encapsulate download process.

    Args:
        ovok(OVOKInterface): OVOK client to carry out the operations.
        groups(list(StudentGroup)): List of studentgroups.
        root_path(str): Root path for downloads.
        tag(str): Tag to download.
        username(str): Git username.
        password(str) Git password.
        strategy(int): Git download strategy.

    Returns:
        list(Command): List of commands to download all matching repositories.
    """
    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()
    for group in groups:

        if not group.primary_repository:
            logger.warning("The group {grp} doesn't have "
                           "a primary repository. Skipping download."
                           .format(grp=group.id))
            continue

        repo_name = group.primary_repository.name
        repo_url = group.primary_repository.url
        repo_path = os.path.join(root_path, repo_name)

        # Download the repository
        cmd_download = cmd.DownloadAssignmentCommand(ovok,
                                                     repo_url,
                                                     repo_path,
                                                     username,
                                                     password,
                                                     strategy=strategy)
        commands.append(cmd_download)

        # Checkout the proper TAG
        cmd_checkout = cmd.CheckoutAssignmentCommand(ovok, repo_path, tag)
        commands.append(cmd_checkout)

    return commands


def download_assignment(assignment_tag, path, manualauth=False):
    """
    Download all student assignments related to certain tag.

    Args:
        assignment_tag(str): Tag to download.
        path(str): Root path where all the repositories will be cloned to.

    Kwargs:
        manualauth(bool): Skip automatic authentication and use manual instead.
    """
    key = conf["bitbucket"]["consumer_key"]
    secret = conf["bitbucket"]["consumer_secret"]

    # Create the OVOK Interface ovok
    ovok = ovok_interface.get_client(platform="bitbucket",
                                     consumer_key=key,
                                     consumer_secret=secret)
    ovok.config = conf

    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    # Get Course from database
    course = ovok.session().query(orm.Course).one_or_none()
    if not course:
        print("Couldn't get Course instance from database, please check "
              "that you have imported the course data!")
        return

    assignments = ovok.session().query(orm.Assignment).all()
    assignment = utility_scripts \
        .select_assignment(assignments,
                           assignment_tag=assignment_tag)

    if not assignment:
        print("Couldn't get matching assignment, exiting.")
        return

    groups = ovok.session().query(orm.StudentGroup).all()

    print("Please provide your Git username: ")
    username = raw_input()
    # username = ovok.get_current_authenticated_user()
    print("Please provide your Git password: ")
    password = getpass.getpass()

    strategy = utility_scripts.select_git_strategy()
    commands = prepare_downloads(ovok,
                                 groups,
                                 path,
                                 assignment.tag,
                                 username,
                                 password,
                                 strategy)

    if len(commands) == 0:
        print("There are no repositories to download. Exiting.")
        return
    else:

        print("The following actions are to be taken:")
        print commands.describe()

        ans = raw_input('Continue? [y/n] ')
        if ans in (["Y", "y"]):
            commands.execute()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    oauth_manual_helpstr = "Perform OAuth authentication steps manually."
    parser.add_argument("--manualauth",
                        help=oauth_manual_helpstr,
                        action="store_true",
                        required=False)

    namespacehelpstr = "Namespace where the repositories are created. " \
                       "Given as BitBucket user. " \
                       "If the namespace is not provided, " \
                       "the repositories are created under the " \
                       "authenticated user. " \
                       "For example: --namespace user1"
    parser.add_argument("--namespace",
                        help=namespacehelpstr,
                        type=str,
                        default=None,
                        required=False)

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    help_string_assignment_tag = "Download specific assignment without " \
                                 "prompting user input."
    parser.add_argument("--tag",
                        help=help_string_assignment_tag,
                        action="store",
                        required=False)

    help_string_root_path = "Root path where the repositories are " \
                            "downloaded to."
    parser.add_argument("--rootpath",
                        help=help_string_root_path,
                        action="store",
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Overwrite specific configuration values
    if args.namespace:
        conf["bitbucket"]["namespace"] = args.namespace

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    if not args.rootpath:
        args.rootpath = os.path.join(curr_dir, "repositories")

    download_assignment(args.tag, args.rootpath, args.manualauth)

    logger.info("End of script {}".format(__file__))
