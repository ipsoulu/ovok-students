""" Defines a client class for working with a specific BitBucket repository. """

from urls import (repository_branches_url, repository_tags_url, repository_branches_tags_url,
                  repository_manifest_url, repository_path_contents_url,
                  repository_path_raw_contents_url, repository_main_branch_url,
                  repository_give_permission_url, repository_deletion_url,
                  repository_list_all_user_permissions_url, repository_download_url,
                  repository_list_all_files_in_main_branch, repository_url,
                  repository_commits_url)

from deploykeys import BitBucketRepositoryDeployKeysClient
from links import BitBucketRepositoryLinksClient
from services import BitBucketRepositoryServicesClient
from changesets import BitBucketRepositoryChangeSetsClient
from webhooks import BitBucketRepositoryWebhooksClient

class BitBucketRepositoryClient(object):
  """ Client class representing a repository in bitbucket. """
  def __init__(self, dispatcher, access_token, access_token_secret, namespace, repository_name):
    self._dispatcher = dispatcher
    self._access_token = access_token
    self._access_token_secret = access_token_secret
    self._namespace = namespace
    self._repository_name = repository_name

  @property
  def namespace(self):
    """ Returns the namespace. """
    return self._namespace

  @property
  def repository_name(self):
    """ Returns the repository name. """
    return self._repository_name

  def changesets(self):
    """ Returns a resource for managing the changesets under this repository. """
    return BitBucketRepositoryChangeSetsClient(self._dispatcher, self._access_token,
                                              self._access_token_secret, self._namespace,
                                              self._repository_name)

  def webhooks(self):
    """ Returns a resource for managing the webhooks under this repository. """
    return BitBucketRepositoryWebhooksClient(self._dispatcher, self._access_token,
                                             self._access_token_secret, self._namespace,
                                             self._repository_name)

  def services(self):
    """ Returns a resource for managing the services under this repository. """
    return BitBucketRepositoryServicesClient(self._dispatcher, self._access_token,
                                             self._access_token_secret, self._namespace,
                                             self._repository_name)

  def links(self):
    """ Returns a resource for managing the links under this repository. """
    return BitBucketRepositoryLinksClient(self._dispatcher, self._access_token,
                                          self._access_token_secret, self._namespace,
                                          self._repository_name)

  def deploykeys(self):
    """ Returns a resource for managing the deploy keys under this repository. """
    return BitBucketRepositoryDeployKeysClient(self._dispatcher, self._access_token,
                                               self._access_token_secret, self._namespace,
                                               self._repository_name)

  def get_main_branch(self):
    """ Returns the main branch for this repository. """
    url = repository_main_branch_url(self._namespace, self._repository_name)
    return self._dispatcher.dispatch(url, access_token=self._access_token,
                                          access_token_secret=self._access_token_secret)

  def get_branches(self):
    """ Returns the list of branches in this repository. """
    url = repository_branches_url(self._namespace, self._repository_name)
    return self._dispatcher.dispatch(url, access_token=self._access_token,
                                          access_token_secret=self._access_token_secret)

  def get_tags(self):
    """ Returns the list of tags in this repository. """
    url = repository_tags_url(self._namespace, self._repository_name)
    return self._dispatcher.dispatch(url, access_token=self._access_token,
                                          access_token_secret=self._access_token_secret)

  def get_branches_and_tags(self):
    """ Returns the list of branches and tags in this repository. """
    url = repository_branches_tags_url(self._namespace, self._repository_name)
    return self._dispatcher.dispatch(url, access_token=self._access_token,
                                          access_token_secret=self._access_token_secret)

  def get_manifest(self, revision='default'):
    """ Returns the manifest for the repository. """
    url = repository_manifest_url(self._namespace, self._repository_name, revision)
    return self._dispatcher.dispatch(url, access_token=self._access_token,
                                          access_token_secret=self._access_token_secret)

  def get_path_contents(self, path, revision='default'):
    """ Returns the contents of the given path. If the path ends in a /, it is treated as a
        directory and the contents of the directory are returned.
    """
    url = repository_path_contents_url(self._namespace, self._repository_name, revision, path)
    return self._dispatcher.dispatch(url, access_token=self._access_token,
                                          access_token_secret=self._access_token_secret)

  def get_raw_path_contents(self, path, revision='default'):
    """ Returns the raw contents of the given path. If the path ends in a /, it is treated as a
        directory and the contents of the directory are returned.
    """
    url = repository_path_raw_contents_url(self._namespace, self._repository_name, revision, path)
    return self._dispatcher.dispatch(url, access_token=self._access_token,
                                          access_token_secret=self._access_token_secret)

  def give_user_permission(self, user, permission):
    """ Extension to the CoreOS wrapper. Give the specified person write permission to the repository """
    url = repository_give_permission_url(self._namespace, self._repository_name, user)
    return self._dispatcher.dispatch(url,
      access_token=self._access_token,
      access_token_secret=self._access_token_secret,
      method='PUT',
      json_body=False,
      plaintext=permission)

  def delete(self):
    """ Extension to the CoreOS wrapper. Delete the specified repository """
    url = repository_deletion_url(self._namespace, self._repository_name)
    return self._dispatcher.dispatch(url,
      access_token=self._access_token,
      access_token_secret=self._access_token_secret,
      method='DELETE')

  def list_user_permissions(self):
    """ Extension to the CoreOS wrapper. Returns a list of permissions for the specified user. """
    url = repository_list_all_user_permissions_url(self._namespace, self._repository_name)
    return self._dispatcher.dispatch(url,
      access_token=self._access_token,
      access_token_secret=self._access_token_secret)

  def get_main_branch_file_listing(self):
    """ Extension to the CoreOS wrapper. Returns a list of main branch files. """
    url = repository_list_all_files_in_main_branch(self._namespace, self._repository_name)
    return self._dispatcher.dispatch(url,
      access_token=self._access_token,
      access_token_secret=self._access_token_secret)

  def get_information(self):
    """ Extension to the CoreOS wrapper. Returns a list of main branch files. """
    url = repository_url(self._namespace, self._repository_name)
    return self._dispatcher.dispatch(url,
      access_token=self._access_token,
      access_token_secret=self._access_token_secret)

  def get_commits(self, branchortag=None, page=None, include=None, exclude=None):
    """ Returns the list of tags in this repository. """
    # TODO: Figure out how to give the dispatcher multiple params
    # with same name! Meaning include and excludes.
    url = repository_commits_url(self._namespace, self._repository_name)

    params = {}
    if branchortag:
        params["branchortag"] = branchortag

    if page:
        params["page"] = page

    if include:
        params["include"] = include

    if exclude:
        params["exclude"] = exclude

    if len(params) < 1:
        params = None

    return self._dispatcher.dispatch(url, access_token=self._access_token,
                                          access_token_secret=self._access_token_secret,
                                          params=params)
