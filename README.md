# OVOK-project #

This workspace is for the OVOK-project which aims to provide tools for computer
assisted evaluation of programming assignments. In this repository you will find
tools for managing course related records and creating Git (Bitbucket) resources
for your students.

## What's this all about? ##

Since the idea of these tools is to manage course related student repositories,
the user should be provided with an easy way of performing operations on
multiple different course instances which might be hosted on different computers.

These tools help you to create and manage courses, create git repositories and 
download student assignments.

A demo of this application can be run after it has been properly installed.
The demo instructions can be found from the " An example of creating a course
instance and its related Git resources" section.

### Features ###

What can you do with these tools?

- Create and manage course instances.
- Create and manage student teams.
- Create Git repositories for student teams.
- Define assignments or deliverables.
- Download student assignments from repositories.

### Workflow ###

The default workflow with the tools is as follows:

1. Import course data from file. This creates a course context that can be later used to continue working with the same course instance.
2. Import other data (student groups, assignments, employees) to an existing course instance.
3. Create repositories for existing student groups.
4. Wait for assignment submissions to repositories.
5. Download student repositories matching assignments.

## How do I get set up? ##

In order to use the OVOK tools, you have to install some dependencies. The
required packages are listed in the "Dependencies" section of this chapter.

Proper configuration is also needed for all the features to work, such as
configuring smtp, database connections and Bitbucket consumer keys.
OVOK tools are configured via configuration.yml file located in root of the
repository. The file uses [YAML](http://www.yaml.org/start.html) format.
In order to properly use these tools, you should fill out the configuration
file as instructed in its comments. An example configuration file can be
found in the "examples" folder.

A python [virtual environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/) is strongly recommended.

### Dependencies ###

These steps have been written for Linux and Mac, Windows installation might vary.

This help assumes that you have [Python 2.7.x](https://www.python.org/downloads/) and [Pip](https://pip.pypa.io/en/stable/installing/) installed.

Again, python virtual environment strongly encouraged. If you are not using
one, run the pip commands with sudo.

1. Installing [Pygit2](http://www.pygit2.org/install.html)

    Install [libgit2](https://github.com/libgit2/libgit2#building-libgit2---using-cmake):

    Needs libraries: pkg-config libssh2-1-dev libhttp-parser-dev libssl-dev libz-dev
    > sudo aptitude install pkg-config libssh2-1-dev libhttp-parser-dev libssl-dev libz-dev

    Get the correct version of libgit2 (0.23.3):
    > wget https://github.com/libgit2/libgit2/archive/v0.23.3.tar.gz

    Extract the packet somewhere:
    > tar xzf v0.23.3.tar.gz

    Go to the directory:
    > cd libgit2-0.23.3/

    Create cmake configuration (note the dot):
    > cmake .

    Run make:
    > make

    Install the packet with make:
    > sudo make install

    Finally Install pygit2:
    > pip install pygit2==0.23.3

2. Install MySQL dependencies

	MySQL connections require the following development library:
	> sudo apt-get install libmysqlclient-dev

3. Install rest of the dependencies

    > pip install -r requirements.txt

How to run tests

    TODO: Write tests.

Deployment instructions

    TODO: Write deployment instructions.

### Bitbucket ###

You should also have a working Bitbucket [account](https://bitbucket.org/account/signup/).

Some of these tools rely on using the [Bitbucket API](https://developer.atlassian.com/bitbucket/api/2/reference/),
which requires authentication. The chosen authentication method for these tools is [OAuth](https://en.wikipedia.org/wiki/OAuth),
which Bitbucket offers for us. In order to use Bitbucket OAuth you need to create a [consumer](https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html).

This consumer is used to authenticate to the Bitbucket API, so it needs permissions to carry out
operations to the API. The recommended settings for a consumer are:

	- Account: Read
	- Repositories: Admin
	- Webhooks: Read and write

You should mark the consumer to be private.

### Database permissions ###

This section is **optional** to those users who only want to use local Sqlite databases.

Currently the only supported databases besides Sqlite are MySQL and MariaDB. In order to
automatically create databases to remote hosts, the database has to have a user called "ovok".

See [creating users](http://dev.mysql.com/doc/refman/5.7/en/adding-users.html)

Apply these commands to the database:
>- CREATE USER 'ovok'@'localhost' IDENTIFIED BY 'password';
>- GRANT ALL PRIVILEGES ON \`OVOK\_%\` . * to 'ovok'@'%';

These commands create a local user called ovok and grant it the permission to
create and manage tables with the "OVOK_" prefix.

## An example of creating a course instance and its related Git resources. ##

You can try these tools out with demo files and a demo OAuth consumer.

Normally the configuration.yml is automatically read, and in the example cases we want
to specify the example configuration so we use the --config flag.


In order to create a new course instance the user can import it from a [WebOodi](https://weboodi.oulu.fi/oodi/)
XML dump.

Change folder to the "ovok" folder where the scripts are.

> cd ovok


Now you can start by importing the test course instance:

> python import_course.py --config ../examples/demo_configuration.yml  ../examples/demo_course.xml

The script will prompt you to select a host store the course in. You can just use the default
sqlite path for now.

After importing your first course, you are ready to import other data:

> - python import_assignments.py --config ../examples/demo_configuration.yml  ../examples/demo_assignments.json
> - python import_studentgroups.py --config ../examples/demo_configuration.yml  ../examples/demo_studentgroups.txt
> - python import_employees.py --config ../examples/demo_configuration.yml  ../examples/demo_employees.json

Finally you can create repositories (Please replace 'yourusername' with your Bitbucket username!):

> python create_repositories.py --config ../examples/demo_configuration.yml --namespace yourusername

## Contribution guidelines ##

1. Create an issue with a description of your idea.
2. Fork the repository.
3. Create a feature branch and commit your changes.
4. Create a Pull request.

## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact
