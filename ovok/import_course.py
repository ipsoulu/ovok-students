#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for importing courses to the OVOK system."""

# Libraries
import logging
import logging.config
import argparse
import csv
import os

# OVOK interface wrapper for BitBucket
from ovok_interface import OVOKInterface
from ovok_interface import utility
from ovok_interface import utility_scripts
from ovok_interface import xml_decoder
from ovok_interface import orm
from ovok_interface.util import commands as cmd
from ovok_interface import Configuration

# Setup logging
logging.captureWarnings(True)
logger = logging.getLogger(__name__)


def create_output(course, groups, filename=None):
    """
    Create csv output from the course.

    Args:
        groups(list(StudentGroup)): List of studentgroups in the course.
    """
    # TODO: Create sorted output by lastname

    # Use a dictionary with email as key to get all unique
    # repositories per student
    student_repo_dict = {}
    for group in groups:

        repositories = []
        if len(group.repositories) > 0:
            repositories = [repo.name for repo in group.repositories]

        for student in group.students:

            if student.email not in student_repo_dict:
                student_repo_dict[student.email] = {}

            student_repo_dict[student.email]["lastname"] = \
                student.lastname.encode('utf-8').strip()
            student_repo_dict[student.email]["firstnames"] = \
                student.firstnames.encode('utf-8').strip()
            student_repo_dict[student.email]["student_number"] = \
                str(student.student_id).encode('utf-8').strip()

            if "repositories" not in student_repo_dict[student.email]:
                student_repo_dict[student.email]["repositories"] = []

            # Append only unique items
            for repo in repositories:
                if repo not in \
                   student_repo_dict[student.email]["repositories"]:
                            student_repo_dict[student.email]["repositories"] \
                                .append(repo.encode('utf-8').strip())

    # Output file will be named by course name and year
    if not filename:
        filename = "_".join([course.code, str(course.year)])
        filename += ".csv"

    try:

        with open(filename, 'wb') as output:
            writer = csv.writer(output)

            for key, value in student_repo_dict.items():
                writer.writerow([value["lastname"],
                                 value["firstnames"],
                                 value["student_number"],
                                 value["repositories"]])

    except Exception:
        logger.exception("Error writing output csv")
    else:
        print("Output written as {}".format(filename))


def query_prefix():
    """
    Query Course prefix from the user.

    Returns:
        str: Prefix queried from the user.
    """
    prefix = raw_input("Course prefix has not been set, please provide a "
                       "course prefix (a short description of the course used "
                       "in repository names, such as 'Ohjelmistotekniikka' "
                       "-> 'OTE'): ")
    return prefix.lower().strip()


def import_course(ovok, course, courseprefix):
    """
    Create missing repositories for student groups in a specific course.

    Args:
        ovok(OVOKClientBitBucket): OVOK client.
        course(Course): Course to import.
        courseprefix(str): Prefix for course repository names.

    Returns:
        CommandList: List of commands that will import the course.
    """
    commands = cmd.CommandList()
    session = ovok.session()

    # Query existing course data
    existing_course = ovok.query(orm.Course)

    # Check how we need to create / modify the course instance
    if len(existing_course) == 0:

        # Set the course prefix and create as new

        # Query course prefix from the user
        if not courseprefix:
            course.prefix = query_prefix()
        else:
            course.prefix = courseprefix

        commands.append(cmd.CreateCommand(session, course))

    else:

        # Update the existing course
        # It can't be straight merged, as some attributes need to be
        # persisted. An example would be course prefix and relations.

        # TODO: Databases should only contain one course instance, but
        # we should propably check what if the list contains multiple

        # Since we only need to update a few attributes, modify
        # the existing instance so that all other attributes remain.
        # Otherwise merge would delete the "missing attributes"

        existing_course = existing_course[0]
        cmd_modify = cmd.ModifyCommand(session, existing_course)
        cmd_modify.modify("code", course.code)
        cmd_modify.modify("year", course.year)
        cmd_modify.modify("name", course.name)

        if courseprefix and (courseprefix != existing_course.prefix):
            cmd_modify.modify("prefix", courseprefix.lower().strip())
        elif not existing_course.prefix:
            cmd_modify.modify("prefix", query_prefix())

        if cmd_modify.dirty():
            commands.append(cmd_modify)

    return commands


def get_existing_student(student, students):
    """
    Return existing student from the provided list of students.

    Args:
        student(Student): Student to find.
        students(list(Student)): List of students to search.

    Returns:
        Student: The existing student from the list. None if not found.
    """
    for s in students:
        if s.student_id == student.student_id:
            return s
    return None


def import_students(ovok, students):
    """
    Imported students to database.

    Args:
        ovok(OVOKInterface): Ovok client.
        sutdents(list(Student)): List of students.

    Returns:
        CommandList: List of commands that will import all students.
    """
    commands = cmd.CommandList()
    session = ovok.session()

    # Query existing students
    existing_students = ovok.query(orm.Student)

    # Cross check all students for updates and create new ones if missing
    for student in students:

        # Check if we need to update
        existing_student = get_existing_student(student, existing_students)
        if existing_student:

            cmd_modify = cmd.ModifyCommand(session, existing_student)
            cmd_modify.modify("email", student.email)
            cmd_modify.modify("firstnames", student.firstnames)
            cmd_modify.modify("lastname", student.lastname)
            if cmd_modify.dirty():
                commands.append(cmd_modify)

            # Check for new usernames
            # TODO: Change the database implementation of usernames to
            # association proxy dictionary. All this can and should be avoided.
            for username in student.usernames:
                for uname in existing_student.usernames:
                    if uname.platform == username.platform:
                        if uname.username != username.username:
                            c = cmd.ModifyCommand(session, uname)
                            c.modify("username", username.username)
                            commands.append(c)
                        break
                else:

                    # Append a new username to existing user
                    uname_new = orm.Username(username.username,
                                             username.platform)
                    existing_student.usernames.append(uname_new)
                    commands.append(cmd.CreateCommand(session, uname_new))

        else:

            # Create student as new
            commands.append(cmd.CreateCommand(session, student))

    return commands


def import_all(course_xml_path,
               courseprefix,
               config,
               autogroup=0,
               output=None):
    """
    Import course and student data and autogroup students if necessary.

    Args:
        course_xml_path(str): Path to the XML file.
        courseprefix(str): Prefix for course repository names.
        config(Configuration): Configuration settings for OVOK client.
        autogroup(int): Size of automatically created student groups. Zero or
                        under for no automatic grouping.
        output(str): If specified, output will be written to the file.
    """
    # Start by setting the Course context

    # Create the OVOK client
    ovok = OVOKInterface()
    ovok.config = config

    # Read course from XML file
    # TODO: Get the intended platfrom from somewhere
    course, students = xml_decoder.decode_weboodi(course_xml_path,
                                                  extract_usernames=True,
                                                  platform="bitbucket")

    # Check if we have an existing context for the course
    contexts = ovok.get_context_course(course)
    if len(contexts) == 0:
        # Create a new context
        # context = ovok.create_context(course, utility.create_db_url(course))
        print("The course has no existing instances.")
        context = utility_scripts.create_context_to_host(ovok, course)
    else:
        print("The course is already hosted somewhere. "
              "Please choose the database.")
        context = utility_scripts.select_context_from_list(ovok,
                                                           course,
                                                           contexts)
        if not context:
            ans = raw_input("No context selected. Create new? [y/n] ")
            if ans in ["Y", "y"]:
                context = utility_scripts.create_context_to_host(ovok, course)

            else:
                print("No valid context selected. Please adjust your "
                      "database configurations.")
                return

    ovok.set_context(context)

    # Create a list of commands to perform
    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()

    # Import course and student data from XML
    commands.extend(import_course(ovok, course, courseprefix))
    commands.extend(import_students(ovok, students))

    if len(commands) > 0:
        print("The following steps are taken:")
        print commands.describe()
        ans = raw_input('Continue? [y/n] ')
        if ans not in (["Y", "y"]):
            print("User cancellation of script.")
            return
        else:
            commands.execute()
            ovok.session().commit()
            print("Performed {} operations".format(len(commands)))
        print("")
    else:
        print("There are no new objects to import")

    # Check if we need to autogroup students
    students_wo_grp = utility.get_students_without_groups(ovok)
    commands_group = utility.autogroup_students_cmd(ovok,
                                                    students_wo_grp,
                                                    autogroup)
    commands_group.describer = cmd.CommandDescriber()

    if len(commands_group) > 0:
        print("The following autogrouping will be performed:")
        print commands_group.describe()
        ans = raw_input('Continue? [y/n] ')
        if ans not in (["Y", "y"]):
            print("User cancellation of script.")
            return
        else:
            commands_group.execute()
            ovok.session().commit()
            print("Performed {} operations".format(len(commands_group)))
        print("")

    # Create csv output
    if output:
        create_output(course, ovok.query(orm.StudentGroup), filename=output)

if __name__ == '__main__':

    # Parse command line arguments
    parser = argparse.ArgumentParser()

    # Course XML file path
    xml_help_string = \
        "Path to file containing an XML representation of the course"
    parser.add_argument("coursexml",
                        help=xml_help_string,
                        action="store")

    # Automatically group students? Indicates the size of the group.
    autohelpstr = "Assign students to parameter sized groups automatically. " \
                  "For example divide users into groups of one: --autogroup 1"
    parser.add_argument("--autogroup",
                        help=autohelpstr,
                        type=int,
                        default=-1,
                        required=False)

    # Custom course prefix, for example "Ohjelmistotekniikka" -> "OTE"
    help_string_courseprefix = "Custom course prefix, for example " \
                               "'Ohjelmistotekniikka' -> 'OTE' "
    parser.add_argument("--courseprefix",
                        help=help_string_courseprefix,
                        type=str,
                        default=None,
                        required=False)

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    help_string_output = "Write student repository data to file."
    parser.add_argument("--output",
                        help=help_string_output,
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    import_all(args.coursexml,
               args.courseprefix,
               conf,
               autogroup=args.autogroup,
               output=args.output)

    logger.info("End of script {}".format(__file__))
