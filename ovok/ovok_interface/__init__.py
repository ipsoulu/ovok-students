# -*- coding: utf-8 -*-
"""Package imports."""

from ovok_interface import OVOKInterface
from ovok_interface_bitbucket import OVOKClientBitBucket

from json_decoder import json_decoder
from xml_decoder import xml_decoder
from database import ovok_orm_definitions as orm
from database import ovok_bitbucket_definitions as orm_bb
from context import constants as db_const
from util import utility
from util import utility_scripts
from util import commands
from core import VerificationError
from core import RepositoryError
from emailer.emailer import Email
from configuration.configuration import Configuration


def get_client(platform=None, *args, **kwargs):
    """
    Return proper client for each platform.

    Kwargs:
        platform(str): Platform to get the client for.

    Returns:
        OVOKInterface: Subclass of OVOKInterface that implements functionality
                       for the specified platform.

    Raises:
        ValueError: Raised if platform is not found.
    """
    clients = {
        None: OVOKInterface,
        "bitbucket": OVOKClientBitBucket
    }
    if platform not in clients:
        raise ValueError("Unknown platform {}".format(str(platform)))
    return clients[platform](*args, **kwargs)
