# -*- coding: utf-8 -*-
"""Module for handling GIT operations."""

import pygit2
import os
import shutil
import re
import logging

# Setup logging
logger = logging.getLogger(__name__)
logging.captureWarnings(True)


class GitStrategy():
    """
    Local Git repository creation strategies.

    DEFAULT: If exists do nothing. Else create new.
    APPEND: If exists pull. Else create new.
    OVERWRITE: If exists remove and create new. Else create new.
    RENAME_NEW: If exists create new folder with different name.
                Else create new.
    """

    DEFAULT = 0
    APPEND = 1
    OVERWRITE = 2
    RENAME_NEW = 3

    STRATEGIES = [DEFAULT, APPEND, OVERWRITE, RENAME_NEW]
    STRATEGY_DESCRIPTIONS = \
        {DEFAULT: "DEFAULT",
         APPEND: "APPEND",
         OVERWRITE: "OVERWRITE",
         RENAME_NEW: "RENAME_NEW"}
    STRATEGY_DESCRIPTIONS_LONG = \
        {DEFAULT: "DEFAULT: If exists do nothing. Else create new.",
         APPEND: "APPEND: If exists pull. Else create new.",
         OVERWRITE: "OVERWRITE: If exists remove and create new. "
                    "Else create new.",
         RENAME_NEW: "RENAME_NEW: If exists create new folder with different "
                     "name. Else create new"}


def clone(url, path, callbacks=None, strategy=GitStrategy.DEFAULT):
    """
    Clone repository to path.

    Args:
        path(str): Path for repository.

    Kwargs:
        callbacks(pygit2.RemoteCallbacks): Callbacks for authentication
                                           progress output.
        strategy(int): Git cloning strategy.

    Returns
        pygit2.Repository: The cloned pygit2 repository.
    """
    if strategy not in GitStrategy.STRATEGIES:
        raise Exception("Unkown git strategy {}".format(str(strategy)))
    logger.debug("Cloning {} to path {} with strategy {}"
                 .format(url,
                         path,
                         GitStrategy.STRATEGY_DESCRIPTIONS[strategy]))
    if strategy == GitStrategy.DEFAULT:
        if os.path.exists(path):
            return None
    elif strategy == GitStrategy.APPEND:
        if os.path.exists(path):
            return pull(path, callbacks=callbacks)
    elif strategy == GitStrategy.OVERWRITE:
        if os.path.exists(path):
            shutil.rmtree(path)
    elif strategy == GitStrategy.RENAME_NEW:
        if os.path.exists(path):

            path_head, dirname = os.path.split(path)
            dir_highest = 1

            folders = os.listdir(path_head)
            for folder in folders:
                if not folder.startswith(dirname):
                    continue
                m = re.search(r'\(([^)]*)\)[^(]*$', folder)
                if m:
                    dir_num = int((m.group()
                                   .replace("(", ""))
                                  .replace(")", ""))
                    if (dir_num + 1) > dir_highest:
                        dir_highest = dir_num + 1

            new_dirname = "".join([dirname, "(", str(dir_highest), ")"])
            new_path = os.path.join(path_head, new_dirname)
            path = new_path

    return pygit2.clone_repository(url, path, callbacks=callbacks)


def pull(path, remote="origin", callbacks=None):
    """
    Pull repository in path.

    Kwargs:
        remote(str): Git remote.
        callbacks(pygit2.RemoteCallbacks): Callbacks for authentication
                                           progress output.
    Returns:
        pygit2.Repository: The git repository.
    """
    repository = pygit2.Repository(path)

    # Fetch remote
    repository.remotes[remote].fetch(callbacks=callbacks)

    # Resolve which is the current branch.
    # Check if it has a remote HEAD.
    # Then pull it.
    # See:
    # http://www.pygit2.org/references.html#the-head
    return repository


def checkout_sha(path, sha, strategy=GitStrategy.DEFAULT):
    """
    Checkout Git SHA.

    Args:
        path(str): Path to checkout the repository.
        sha(str): Commit SHA to checkout.

    Kwargs:
        strategy(int): Git strategy to checkout with.

    Returns:
        pygit2.Repository: The git repository.
    """
    try:

        # Create the repo object from folder
        repo = pygit2.Repository(path)

        # Create a Commit-object based on commit's sha
        # TODO: Test what will be thrown in case commit with this
        # sha isn't found.
        commit = repo[sha]

        # Force working directory to be like at the time of commit.
        # Without this checkout might fail.
        repo.checkout_tree(commit, strategy=pygit2.GIT_CHECKOUT_FORCE)

        # Set DETACHED HEAD
        repo.set_head(pygit2.Oid(hex=sha))

    except Exception:
        raise
    return repo


def checkout_tag(path, tag):
    """
    Checkout Git tag from local repository.

    Args:
        path(str): Path to the repository.
        tag(str): Tag to checkout.
    """
    repo = pygit2.Repository(path)
    return repo.checkout(tag)
