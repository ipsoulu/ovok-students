# -*- coding: utf-8 -*-
"""Module for mapping OVOK objects to SQLAlchemy objects."""

from sqlalchemy import create_engine
from sqlalchemy import event
from sqlalchemy import Column
from sqlalchemy import Table
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Boolean
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import UniqueConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.orm import backref
from sqlalchemy.orm import Session
from sqlalchemy.ext.declarative import declarative_base

# Base class for creating ORM objects
SQLAlchemyBaseClass = declarative_base()

#####################################################
# Association tables for many-to-many relationships
#####################################################

# Student <-> StudentGroup
student_studentgroup_association_table = \
    Table("student_studentgroup_association",
          SQLAlchemyBaseClass.metadata,
          Column("student_id", Integer, ForeignKey("student.student_id")),
          Column("studentgroup_id", Integer, ForeignKey("studentgroup.id")))

# Employee <-> Assesment
employee_assesment_association_table = \
    Table("employee_assesment_association",
          SQLAlchemyBaseClass.metadata,
          Column("employee_email",
                 String(50),
                 ForeignKey("employee.email")),
          Column("assesment_assignment_tag",
                 Integer,
                 ForeignKey("assesment.id")))

# Repository <-> StudentGroup
# Which groups have access to which repositories
repository_studentgroup_association_table = \
    Table("repository_studentgroup_association",
          SQLAlchemyBaseClass.metadata,
          Column("repository_id",
                 String(60),
                 ForeignKey("repository.identifier")),
          Column("studentgroup_id",
                 Integer,
                 ForeignKey("studentgroup.id")))

#########################
# ORM class definitions
#########################


class Person(SQLAlchemyBaseClass):
    """A base class representing persons. Inherit only."""

    # Table definitions
    __tablename__ = "person"

    email = Column(String(50), primary_key=True)
    firstnames = Column(String(50), nullable=False)
    lastname = Column(String(50), nullable=False)

    # Person has multiple usernames, one for each platform
    # The username has only one Person, so if the Person is
    # deleted, also delete the Username
    usernames = relationship("Username",
                             back_populates="person",
                             cascade="all, delete-orphan")

    # Person acts as a base class
    polymorph_type = Column(String(50))
    __mapper_args__ = {
        'polymorphic_identity': 'person',
        'polymorphic_on': polymorph_type
    }

    def __init__(self,
                 firstnames,
                 lastname,
                 email,
                 usernames=None):
        """
        Construct a Person.

        Args:
            firstnames(str): First names of a person.
            lastname(str): Last name of a person.
            email(str): Email of a person.
            usernames(list(Username)): List of usernames.
        """
        self.firstnames = firstnames
        self.lastname = lastname
        self.email = email
        self.usernames = usernames if usernames else []

    def get_readable_name(self):
        """
        Return a readable name of the person, formatted 'last name, firstname'.

        Returns:
            Formatted name of the person.
        """
        return ", ".join([self.lastname, self.firstnames])


class Employee(Person):
    """A class representing an Employee."""

    __tablename__ = "employee"

    # Inherits from Person, subclassed as employee
    __mapper_args__ = {
        'polymorphic_identity': 'employee',
    }

    # Refer to the Person email as a foreign primary key
    email = Column(String(50), ForeignKey("person.email"), primary_key=True)

    # Many-to-many relationship between employees and assesments.
    assesments = relationship("Assesment",
                              back_populates="employees",
                              secondary=employee_assesment_association_table)

    feedbacks = relationship("Feedback", back_populates="employee")

    def __init__(self,
                 firstnames,
                 lastname,
                 email,
                 usernames=None,
                 feedbacks=None):
        """
        Construct an Employee.

        Args:
            firstnames(str): First names of an employee.
            lastname(str): Last name of an employee.
            email(str): Email of an employee.
            usernames(list(Username)): List of usernames.
            feedbacks(list(Feedback)): List of Feedbacks.
        """
        super(Employee, self).__init__(firstnames,
                                       lastname,
                                       email,
                                       usernames=usernames)
        self.feedbacks = feedbacks if feedbacks else []


class Student(Person):
    """A class representing a Student."""

    __tablename__ = "student"
    __table_args__ = (UniqueConstraint('email', name='_email_uc'),)

    # Inherits from Person
    __mapper_args__ = {
        'polymorphic_identity': 'student',
    }

    # Set a new primary key as student id number
    student_id = Column(Integer, primary_key=True)

    # Refer to the Person email as a foreign key
    email = Column(String(50), ForeignKey("person.email"))

    submissions = relationship("Submission", back_populates="student")

    groups = relationship("StudentGroup",
                          back_populates="students",
                          secondary=student_studentgroup_association_table)

    def __init__(self,
                 firstnames,
                 lastname,
                 student_id,
                 email,
                 usernames=None,
                 submissions=None):
        """
        Construct a Student.

        Args:
            firstnames(str): First names of a student.
            lastname(str): Last name of a student.
            student_id(int): Student id number of a student.
            email(str): Email of a student.
            usernames(list(Username)): List of usernames.
            submissions(list(Submission)): List of submissions.
        """
        super(Student, self).__init__(firstnames,
                                      lastname,
                                      email,
                                      usernames=usernames)
        self.student_id = student_id
        self.submissions = submissions if submissions else []


class StudentGroup(SQLAlchemyBaseClass):
    """A class representing a Student Group."""

    __tablename__ = "studentgroup"

    # Table primary key
    id = Column(Integer, primary_key=True)

    # Refer to the parent course to which this group belongs
    # course_code = Column(String(50))
    # course_year = Column(Integer)
    # __table_args__ = \
    #     (ForeignKeyConstraint([course_code, course_year],
    #                           ["course.code", "course.year"]),
    #      {})

    # Many-to-many relationship between student groups and students
    students = relationship("Student",
                            back_populates="groups",
                            secondary=student_studentgroup_association_table)

    # Many-to-many relationship between student groups and repositories
    repositories = \
        relationship("Repository",
                     back_populates="groups",
                     secondary=repository_studentgroup_association_table)

    # TODO: Does primary repository need to be excluded from "repositories"
    # attribute? I would think not, since it's just another repo that the
    # group has access to.

    # One-to-one relationship between repository and sutdentgroup.
    # Since the group has access to multiple repositories within the Course,
    # we need to know which one of those is the "main" repository for this
    # specific group.
    primary_repository_id = Column(String(60),
                                   ForeignKey('repository.identifier'))
    primary_repository = \
        relationship("Repository",
                     backref=backref("primary_group", uselist=False))

    def __init__(self,
                 students=None,
                 repositories=None):
        """
        Construct a StudentGroup.

        Args:
            students(list(Student)): List of Students in the group.
            repositories(list(Repository)): List of Repositories the Group
                                            has access to.
        """
        # TODO: How to process primary repo in constructor.
        self.students = students if students else []
        self.repositories = repositories if repositories else []


class Course(SQLAlchemyBaseClass):
    """A class representing a Course."""

    __tablename__ = "course"

    # A course is identified by a code and year combination. The same course
    # can be run on mutliple years.
    code = Column(String(50), primary_key=True)
    year = Column(Integer, primary_key=True, autoincrement=False)

    # A short prefix used for quick identifying of courses.
    # Example: Ohjelmistotekniikka -> OTE
    prefix = Column(String(50), nullable=False)

    # Name of the course
    name = Column(String(50), nullable=False)

    # Many-to-one relationship. A course has multiple StudentGroups,
    # but one StudentGroup has one course.
    # groups = relationship("StudentGroup",
    #                       backref="course",
    #                       cascade="all, delete-orphan")

    assignments = relationship("Assignment",
                               back_populates="course",
                               cascade="all, delete-orphan")

    def __init__(self,
                 code,
                 prefix,
                 name,
                 year,
                 assignments=None,
                 platforms=None):
        """
        Construct a Course.

        Args:
            code(str): Code of the Course.
            prefix(str): Short human-readable prefix for course repositories,
                         such as "Ohjelmistotekniikka" -> "OTE".
            name(str): Name of the Course.
            year(int): Year of the Course.
            assignments(list(Assignment)): List of assignments in the Course.
            platforms(list(str)): List of platforms used in the course.
        """
        self.code = code
        self.prefix = prefix
        self.name = name
        self.year = year
        # self.groups = groups if groups else []
        self.assignments = assignments if assignments else []
        self.platforms = platforms if platforms else []

    def repository_prefix(self):
        """
        Return the string used to name all course related repositories.

        Returns:
            Formatted repository name prefix.
        """
        return "-".join([self.prefix.lower(), str(self.year)]).lower()

    def get_repositories(self):
        """
        Return course repositories.

        Returns:
            list(Repository): List of Course Repository objects.
        """
        return [group.repositories for group in self.groups
                if len(group.repositories) > 0]


class Repository(SQLAlchemyBaseClass):
    """A class representing a Repository where Students store submissions."""

    # TODO: Does the repository need to know the primary group?

    __tablename__ = "repository"

    identifier = Column(String(60), primary_key=True)
    description = Column(String(1000), nullable=False)
    name = Column(String(50), nullable=False)
    platform = Column(String(50), nullable=False)
    owner = Column(String(50), nullable=False)

    groups = \
        relationship("StudentGroup",
                     back_populates="repositories",
                     secondary=repository_studentgroup_association_table)

    submissions = relationship("Submission", back_populates="repository")

    polymorph_type = Column(String(50))
    __mapper_args__ = {
        'polymorphic_identity': 'repository',
        'polymorphic_on': polymorph_type
    }

    def __init__(self,
                 identifier,
                 owner,
                 description,
                 name,
                 platform):
        """
        Construct a Repository.

        Args:
            identifier(str): The identifier of the Repository.
            owner(str): The owner of the Repository.
            description(str): Textual description of the Repository.
            name(str): Name of the Repository.
            platform(str): Platform of the Repository.
        """
        self.identifier = identifier
        self.platform = platform
        self.owner = owner
        self.description = description
        self.name = name


class GitRepository(Repository):
    """A class representing a Git repo where Students store submissions."""

    __tablename__ = "gitrepository"

    identifier = Column(String(60),
                        ForeignKey("repository.identifier"),
                        primary_key=True)

    scm = Column(String(50), nullable=False)
    is_private = Column(Boolean, nullable=False)
    fork_policy = Column(String(50), nullable=False)
    url = Column(String(500))

    webhooks = relationship("Webhook",
                            back_populates="repository",
                            cascade="all, delete-orphan")

    __mapper_args__ = {
        'polymorphic_identity': 'gitrepository',
    }

    _DEFAULT_SCM = "git"
    _DEFAULT_IS_PRIVATE = True
    _DEFAULT_FORK_POLICY = "no_public_forks"

    def __init__(self,
                 identifier,
                 owner,
                 description,
                 name,
                 platform,
                 url=None,
                 scm=_DEFAULT_SCM,
                 is_private=_DEFAULT_IS_PRIVATE,
                 fork_policy=_DEFAULT_FORK_POLICY):
        """
        Construct a GitRepository.

        Args:
            identifier(str): The identifier of the Repository. (UUID)
            owner(str): The owner of the Repository.
            description(str): Textual description of the Repository.
            name(str): Name of the Repository.
            platform(str): Platform of the Repository.
            url(str): URL of the GitRepository.
            scm(str): Source control management used (git, mercurial etc).
            is_private(bool): Is the GitRepository private or public.
            fork_policy(str): Forking policy of the GitRepository.
        """
        super(GitRepository, self).__init__(identifier,
                                            owner,
                                            description,
                                            name,
                                            platform)

        self.scm = scm
        self.is_private = is_private
        self.fork_policy = fork_policy
        self.name = name.lower()
        self.url = url


class Username(SQLAlchemyBaseClass):
    """
    A class representing a Username for a specific platform.

    Usernames are paired to certain users, and a user can have multiple
    usernames. The user can't multiple usernames for the same platform.
    """

    __tablename__ = "username"

    # Platform in which this username is used
    platform = Column(String(60), primary_key=True)

    # Refer to the parent person whose username this is
    person_id = Column(String(50),
                       ForeignKey("person.email"),
                       primary_key=True)
    person = relationship("Person", back_populates="usernames")

    # Username
    username = Column(String(100), nullable=False)

    def __init__(self, username, platform):
        """
        Construct a Username.

        Args:
            username(str): The username as a string.
            platform(str): The platform this username is registered to.
        """
        self.username = username
        self.platform = platform


class Assesment(SQLAlchemyBaseClass):
    """An assesment is given to a student as a response to a submission."""

    __tablename__ = "assesment"

    id = Column(Integer, primary_key=True)

    finished = Column(Boolean(), default=False)

    grading = relationship("Grading",
                           back_populates="assesment",
                           uselist=False)

    submissions = relationship("Submission", back_populates="assesment")

    # Many-to-many relationship between employees and assesments.
    employees = relationship("Employee",
                             back_populates="assesments",
                             secondary=employee_assesment_association_table)

    # TODO: Remove student
    # TODO: Assesment -> Assessment

    def __init__(self,
                 student,
                 assignment,
                 feedbacks=None,
                 grading=None,
                 submission=None,
                 finished=False):
        """Construct an assesment."""
        self.student = student
        self.assignment = assignment
        self.submission = submission
        self.feedbacks = feedbacks if feedbacks else []
        self.grading = grading
        self.finished = finished


class Assignment(SQLAlchemyBaseClass):
    # TODO: Should change "deadline" to "not_after" for consistent naming.
    """An assignment describes a course task to be done by students."""

    __tablename__ = "assignment"
    __table_args__ = \
        (ForeignKeyConstraint(["course_code", "course_year"],
                              ["course.code", "course.year"]),
         {})

    tag = Column(String(60), primary_key=True)

    # Refer to the parent course to which this group belongs
    course_code = Column(String(50), nullable=False)
    course_year = Column(Integer, nullable=False)
    course = relationship("Course", back_populates="assignments")

    submissions = relationship("Submission", back_populates="assignment")

    description = Column(String(1000))
    deadline = Column(DateTime())
    not_before = Column(DateTime())
    root_folder = Column(String(50))
    jenkins_job = Column(String(50))
    required_folders = Column(String(1000))

    def __init__(self,
                 tag,
                 description,
                 deadline,
                 not_before=None):
        """
        Construct an assignment.

        Args:
            tag(str): The string used to identify an assignment.
            description(str): Textual representation of the assignment.
            deadline(datetime): Latest time when submissions are accepted for
                                this assignment.
            not_before(datetime): The earliest time when submissions are
                                  accepted for this assignment.
        """
        self.tag = tag
        self.description = description
        self.deadline = deadline
        self.not_before = not_before


class Submission(SQLAlchemyBaseClass):
    """A submission is the work done by a student to a certain assignment."""

    __tablename__ = "submission"

    id = Column(Integer, primary_key=True)

    # One-to-many relationship between a submission and a repository.
    # A repository can have multiple submissions.
    repository_id = Column(String(60), ForeignKey("repository.identifier"))
    repository = relationship("Repository", back_populates="submissions")

    # One-to-many relationship between Student and Submission classes.
    # A Student can have multiple Submissions, but a Submission has one
    # student.
    student_id = Column(Integer, ForeignKey("student.student_id"))
    student = relationship("Student", back_populates="submissions")

    # One-to-many relationship between a Submission and an Assignment.
    assignment_id = Column(String(60), ForeignKey("assignment.tag"))
    assignment = relationship("Assignment", back_populates="submissions")

    assesment_id = Column(Integer, ForeignKey("assesment.id"))
    assesment = relationship("Assesment", back_populates="submissions")

    feedbacks = relationship("Feedback", back_populates="submission")

    location = Column(String(1000))
    date = Column(DateTime())

    def __init__(self,
                 assignment,
                 assesment,
                 repository,
                 date,
                 location=None,
                 feedbacks=None,
                 grade=None):
        """
        Construct a submission.

        Args:
            location(str): The path where the submission is stored in.
            assignment(Assignment): The assignment to which this submission is
                                    a response to.
            assesment(Assesment): The assesment this submission is for.
            repository(Repository): The repository where this submission is
                                    located in.
            date(datetime): Date and time of submission.
            feedbacks(datetime): The earliest time when submissions are
            grade(Grading): The grading given to this submission.
        """
        self.location = location
        self.assignment = assignment
        self.repository = repository
        self.date = date
        self.feedbacks = feedbacks if feedbacks else []
        self.grade = grade


class Feedback(SQLAlchemyBaseClass):
    """Feedback is given to a certain submission."""

    __tablename__ = "feedback"

    id = Column(Integer, primary_key=True)

    # One-to-many relationship between a Submission and a Feedback.
    # A Submission can have multiple Feedbacks.
    submission_id = Column(Integer, ForeignKey("submission.id"))
    submission = relationship("Submission", back_populates="feedbacks")

    # One-to-many relationship between a Submission and an Employee.
    # An Employee can have multiple Feedbacks.
    employee_id = Column(String(50), ForeignKey("employee.email"))
    employee = relationship("Employee", back_populates="feedbacks")

    # TODO: response_to must be defined!

    content = Column(String(1000))
    date = Column(DateTime())

    def __init__(self,
                 creator,
                 submission,
                 content,
                 date):
        """
        Construct a feedback.

        Args:
            creator(Person): The person who gave the feedback.
            submission(Submission): The submission for which this Feedback
                                    was written for.
            content(Repository): The textual representation of this feedback.
            date(datetime): Date and time of feedback submitment.
        """
        self.creator = creator
        self.submission = submission
        self.content = content
        self.date = date


class Grading(SQLAlchemyBaseClass):
    """Grading indicates the quality of a certain assesment."""

    __tablename__ = "grading"

    id = Column(Integer, primary_key=True)

    # TODO: Change primary key to Assessment.id

    # One-to-one relationship between an assesment and a grading.
    assesment_id = Column(Integer, ForeignKey("assesment.id"))
    assesment = relationship("Assesment", back_populates="grading")

    def __init__(self,
                 assignment,
                 creator,
                 submission,
                 date,
                 points):
        """
        Construct a grading.

        Args:
            assignment(Assignment): The assignment this grading is for.
            creator(Person): The person who performed the grading.
            submission(Submission): The submission for which this grading
                                     was written for.
            date(datetime): Date and time of grading submitment.
            points(int): Points indicating the grade on a numerical scale.
        """
        self.assignment = assignment
        self.creator = creator
        self.submission = submission
        self.date = date
        self.points = points


class Artifact(SQLAlchemyBaseClass):
    """Jenkins job artifact."""

    __tablename__ = "artifact"

    id = Column(Integer, primary_key=True, autoincrement=True)

    path = Column(String(200))
    filename = Column(String(100), nullable=False)
    content_type = Column(String(100))

    buildresult_id = Column(String(40),
                            ForeignKey("buildresult.commit_sha"))
    buildresult = relationship("BuildResult",
                               back_populates="artifacts")


class BuildResult(SQLAlchemyBaseClass):
    """Build results from a Jenkins build job."""

    __tablename__ = "buildresult"
    __table_args__ = (UniqueConstraint("commit_sha",
                                       "assignment_tag",
                                       name="_uc_1"),)

    # TODO: This primary key should be changed to allow
    # multiple assignments to be reviewed from the same commit.
    # For example, user creates commit ABC. This commit has
    # assignments 1 and 2. If the tags the commit with
    # Assig1 and Assig2 tag, both of them will be incorrectly
    # stored into this same build result.
    commit_sha = Column(String(40), primary_key=True)
    assignment_tag = Column(String(40))
    repository = Column(String(60))

    datetime = Column(DateTime())
    status = Column(String(40))
    build_number = Column(Integer)
    build_job_version = Column(String(40))
    expiration_date = Column(DateTime(), nullable=False)

    artifacts = relationship("Artifact",
                             back_populates="buildresult",
                             single_parent=True,
                             cascade="all, delete-orphan")


class Webhook(SQLAlchemyBaseClass):
    """Webhook for repositories."""

    __tablename__ = "webhook"

    uuid = Column(String(40), primary_key=True)
    url = Column(String(500))
    created_at = Column(DateTime())
    description = Column(String(500))
    active = Column(Boolean())
    events = Column(String(500))
    subject = Column(String(500))

    repository_id = Column(String(60),
                           ForeignKey("gitrepository.identifier"))
    repository = relationship("GitRepository", back_populates="webhooks")


@event.listens_for(Session, "after_flush")
def delete_studentgroup_orphans(session, ctx):
    """
    Event that is run after flushing.

    If Students are removed from the database,
    remove all studentgroups that have no students.

    TODO: Implement this properly from:
    https://bitbucket.org/zzzeek/sqlalchemy/wiki/UsageRecipes/ManyToManyOrphan
    """
    session.query(StudentGroup) \
        .filter(~StudentGroup.students.any()) \
        .delete(synchronize_session=False)


def create_database(path):
    # Create an engine that stores data in the local directory's
    # sqlalchemy_example.db file.
    engine = create_engine(path)

    # Create all tables in the engine. This is equivalent to "Create Table"
    # statements in raw SQL.
    SQLAlchemyBaseClass.metadata.create_all(engine)
