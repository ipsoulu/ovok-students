#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for creating BitBucket repositories."""

import logging
import logging.config
import argparse
import csv
import sys
import os
from sqlalchemy import and_

import ovok_interface
from ovok_interface import orm
from ovok_interface import orm_bb
from ovok_interface.util import utility
from ovok_interface.util import utility_scripts
from ovok_interface import VerificationError
from ovok_interface import commands as cmd
from ovok_interface import Email
from ovok_interface import Configuration
from ovok_interface import RepositoryError

logger = logging.getLogger(__name__)
logging.captureWarnings(True)


def create_output(course, groups, filename):
    """
    Create csv output from course repositories.

    Args:
        course(Course): Course from where the report is generated from.
        groups(list(StudentGroup)): List of StudentGroup objects in the course.
    """
    # TODO: Create sorted output by lastname

    # Use a dictionary with email as key to get all unique
    # repositories per student
    student_repo_dict = {}
    for group in groups:

        repositories = []
        if len(group.repositories) > 0:
            repositories = [repo.name for repo in group.repositories]

        for student in group.students:

            if student.email not in student_repo_dict:
                student_repo_dict[student.email] = {}

            student_repo_dict[student.email]["lastname"] = \
                student.lastname.encode('utf-8').strip()
            student_repo_dict[student.email]["firstnames"] = \
                student.firstnames.encode('utf-8').strip()
            student_repo_dict[student.email]["student_number"] = \
                str(student.student_id).encode('utf-8').strip()

            if "repositories" not in student_repo_dict[student.email]:
                student_repo_dict[student.email]["repositories"] = []

            # Append only unique items
            for repo in repositories:
                if repo not in \
                   student_repo_dict[student.email]["repositories"]:
                            student_repo_dict[student.email]["repositories"] \
                                .append(repo.encode('utf-8').strip())

    # Output file will be named by course name and year
    # filename = "_".join([course.code, str(course.year)])
    # filename += ".csv"

    try:

        with open(filename, 'wb') as output:
            writer = csv.writer(output)

            for key, value in student_repo_dict.items():
                writer.writerow([value["lastname"],
                                 value["firstnames"],
                                 value["student_number"],
                                 value["repositories"]])

    except Exception as e:
        logger.error("Error writing output csv: {}".format(repr(e)))


def form_repositories(ovok, course, groups, admins, namespace):
    """
    Form repositories for groups.

    Args:
        ovok(OVOKInterface): OVOK client.
        course(Course): Course where the repositories are created.
        groups(list(StudentGroup)): List of studentgroups to create
                                    repositories for.
        admins(list(Employee)): List of employees who will be given
                                admin privileges to all repositories.
        namespace(str): Namespace for the repositories.
                        In other words, the owner account / team etc.

    Returns:
        CommandList: List of commands to execute.
    """
    if not namespace:
        raise Exception("Repository namespace was not specified.")
    session = ovok.session()

    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()
    for group in groups:

        # Create a name for the repository
        id = str(group.id).zfill(3)
        name = "-".join([course.repository_prefix(), id])

        # TODO: Fix hard coded platform
        should_continue = False
        for student in group.students:
            for username in student.usernames:
                if username.platform == "bitbucket":
                    break
            else:
                print(u"Student {} has no registered bitbucket username"
                      .format(student.get_readable_name()))
                break
            should_continue = True
        else:
            print("Not all students in the group have a username "
                  "assigned skipping repository creation.")

        if not should_continue:
            continue

        cmd_create = cmd.CreateRepositoryCommand(ovok,
                                                 group,
                                                 name,
                                                 namespace,
                                                 session=session)
        commands.append(cmd_create)

        # Write permissions for students
        for student in group.students:

            username = None
            for uname in student.usernames:
                if uname.platform == "bitbucket":
                    username = uname.username
                    break
            else:
                print(u"Student {} has no registered username for bitbucket, "
                      "skipping permission."
                      .format(student.get_readable_name()))
                continue

            privilege = orm_bb.RepositoryPermission.WRITE

            perm = orm_bb.RepositoryPermission(username,
                                               privilege,
                                               name,
                                               namespace=namespace)
            cmd_permission = \
                cmd.RepositoryPermissionCommand(ovok, perm, session=session)
            commands.append(cmd_permission)

        # Admin permissions for employees
        for employee in admins:

            username = None
            for uname in employee.usernames:
                if uname.platform == ovok.get_platform():
                    username = uname.username
                    break
            else:
                print(u"Employee {} has no registered username for bitbucket, "
                      "skipping permission."
                      .format(employee.get_readable_name()))

            privilege = orm_bb.RepositoryPermission.ADMIN

            perm = orm_bb.RepositoryPermission(username,
                                               privilege,
                                               name,
                                               namespace=namespace)
            cmd_permission = \
                cmd.RepositoryPermissionCommand(ovok, perm, session=session)
            commands.append(cmd_permission)

    return commands


def scan_repository_permissions(ovok, repositories):
    """
    Scan repositories for missing permissions.

    Args:
        ovok(OVOKClientBitBucket): OVOK bitbucket client.
        repositories(list(GitRepository)): List of repositories to scan.

    Returns:
        CommandList: List of commands to carry out synchronization.
    """
    commands_sync = cmd.CommandList()
    for repository in repositories:
        permissions = utility.sync_repository_permissions(ovok,
                                                          repository,
                                                          "bitbucket")
        if len(permissions) > 0:
            commands_sync.extend(permissions)

    return commands_sync


def get_emails_from_commands(ovok, commands, privileges):
    """
    Get emails for people who got new permissions from commands.

    Args:
        ovok(OVOKClientBitBucket): OVOK bitbucket client.
        commands(list(AbstractCommand)): List of any commands executed.
        privileges(list(str)): List of strings containing permissions levels
                               to send emails for. If for example "WRITE" then
                               all who got WRITE permissions are processed.

    Returns:
        list(str): List of email addresses.
    """
    # Get usernames from permission commands
    recipient_usernames = []
    for command in commands:
        if isinstance(command, cmd.RepositoryPermissionCommand) \
           and command.permission.privilege in privileges:
            recipient_usernames.append(command.permission.username)

    if len(recipient_usernames) == 0:
        return recipient_usernames

    # Query emails by usernames
    recipient_students = ovok.session() \
        .query(orm.Student) \
        .join(orm.Username) \
        .filter(and_(orm.Username.platform == "bitbucket",
                     orm.Username.username.in_(recipient_usernames))) \
        .all()

    return [s.email for s in recipient_students]


def create_repositories(conf, email, manualauth, output=None):
    """
    Create missing repositories for student groups in a specific course.

    Args:
        conf(Configuration): Configuration settings for OVOK client.
        email(Email): Email to be sent on permissions given.
        manualauth(bool): Skip automatic OAuth and do it manually if True.
    """
    key = conf["bitbucket"]["consumer_key"]
    secret = conf["bitbucket"]["consumer_secret"]

    # Create the OVOK Interface ovok
    ovok = ovok_interface.get_client(platform="bitbucket",
                                     consumer_key=key,
                                     consumer_secret=secret)
    ovok.config = conf

    # Set course context
    try:
        utility_scripts.verify_bitbucket(ovok, manual=manualauth)
    except VerificationError, e:
        print("Bitbucket authentication error: {}".format(str(e)))
        return
    except Exception, e:
        print("Suffered an error while authenticating: {}".format(str(e)))
        return

    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    # Get Course from database
    course = ovok.session().query(orm.Course).one_or_none()
    if not course:
        print("Couldn't get Course instance from database, please check "
              "that you have imported the course data!")
        return

    # Get groups without repositories
    groups_missing_repositories = utility.get_groups_without_repositories(ovok)
    admins = ovok.query(orm.Employee)

    # Get the necessary actions to create repositories
    commands = cmd.CommandList()
    commands.extend(form_repositories(ovok,
                                      course,
                                      groups_missing_repositories,
                                      admins,
                                      ovok.config["bitbucket"]["namespace"]))

    if len(commands) > 0:

        print("The following actions are to be taken:")
        print commands.describe()

        ans = raw_input('Continue? [y/n] ')
        if ans in (["Y", "y"]):
            commands.execute()
            ovok.session().commit()

    else:
        print("No need to create new repositories.")

    # If the user wants, scan all the repositories for missing permissions
    repositories = ovok.query(orm.GitRepository)
    commands_sync = cmd.CommandList()
    if len(repositories) > 0:

        question = "Scan repository permissions from all ({}) repositories? " \
            "[y/n] ".format(len(repositories))
        ans = raw_input(question)
        if ans in (["Y", "y"]):

            try:
                commands_sync = scan_repository_permissions(ovok, repositories)
            except RepositoryError, e:
                print("Unable to scan repositories, err: {}".format(str(e)))
            else:

                if len(commands_sync) == 0:
                    print("Repository permissions are as they should be.")
                else:

                    print("The following actions are to be taken:")
                    print commands_sync.describe()

                    ans = raw_input('Continue? [y/n] ')
                    if ans in (["Y", "y"]):
                        commands_sync.execute()
                        print("Done.")

    else:
        print("The course contains no repositories, skipping permission "
              "synchronization.")

    # If the email was specified, try sending it.
    if email:

        # For all the successful permission commands, extract emails for
        # certain permissions given
        successful_commands = []
        successful_commands.extend(commands.get_successful())
        successful_commands.extend(commands_sync.get_successful())

        recipients = \
            get_emails_from_commands(ovok,
                                     successful_commands,
                                     [orm_bb.RepositoryPermission.WRITE])
        email.to_addr = recipients

        # If the user wants, send an email to all recipients
        if len(recipients) > 0:
            ans = raw_input("Send emails to students that got WRITE "
                            "permissions ({}) [y/n]? "
                            .format(len(recipients)))

            if ans in (["Y", "y"]):

                try:
                    utility.send_email(ovok.config["smtp"]["username"],
                                       ovok.config["smtp"]["password"],
                                       email,
                                       ovok.config["smtp"]["server"])
                except Exception, e:
                    print("unable to send email, err {}".format(str(e)))

    # Create human readable student-repository mappings as output
    if output:
        create_output(course, ovok.query(orm.StudentGroup), output)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    oauth_manual_helpstr = "Perform OAuth authentication steps manually."
    parser.add_argument("--manualauth",
                        help=oauth_manual_helpstr,
                        action="store_true",
                        required=False)

    namespacehelpstr = "Namespace where the repositories are created. " \
                       "Given as BitBucket user. " \
                       "If the namespace is not provided, " \
                       "the repositories are created under the " \
                       "authenticated user. " \
                       "For example: --namespace user1"
    parser.add_argument("--namespace",
                        help=namespacehelpstr,
                        type=str,
                        default=None,
                        required=False)

    help_string_smtp_username = "SMTP email server username."
    parser.add_argument("--smtpusername",
                        help=help_string_smtp_username,
                        required=False)

    help_string_smtp_pass = "SMTP email server password."
    parser.add_argument("--smtppassword",
                        help=help_string_smtp_pass,
                        required=False,
                        default=None)

    help_string_smtp_address = "SMTP email sender address."
    parser.add_argument("--smtpsender",
                        help=help_string_smtp_address,
                        required=False)

    help_string_smtp_address = "Email subject as string"
    parser.add_argument("--subject",
                        help=help_string_smtp_address,
                        required=False)

    help_string_email_body = "File path containing the email body text."
    parser.add_argument("--email",
                        help=help_string_email_body,
                        action="store",
                        required=False)

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    help_string_output = "Write student repository data to file."
    parser.add_argument("--output",
                        help=help_string_output,
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Overwrite specific configuration values
    if args.smtpsender:
        conf["smtp"]["sender"] = args.smtpsender

    if args.smtppassword:
        conf["smtp"]["password"] = args.smtppassword

    if args.smtpusername:
        conf["smtp"]["username"] = args.smtpusername

    if args.namespace:
        conf["bitbucket"]["namespace"] = args.namespace

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    email = None
    if None in [conf["smtp"]["server"],
                conf["smtp"]["port"],
                args.email,
                args.subject]:
        ans = raw_input("There are missing parameters for sending emails, "
                        "do you wish to skip sending emails? [y/n] ")
        if ans not in ["y", "Y"]:
            print("Make sure the configuration file contains at least "
                  "server and port values in the [smtp] block. "
                  "The parameters email and subject are also mandatory "
                  "if you want to send emails.")
            sys.exit(1)
    else:

        # Read email body from file
        email_body = None
        if args.email:
            try:
                email_body = utility.read_file(args.email)
            except IOError:
                print("Unable to read email body from {}"
                      .format(str(args.email)))
                sys.exit(1)

        email = Email(conf["smtp"]["sender"],
                      None,
                      args.subject,
                      email_body)

    # Create the repositories
    create_repositories(conf, email, args.manualauth, args.output)

    logger.info("End of script {}".format(__file__))
