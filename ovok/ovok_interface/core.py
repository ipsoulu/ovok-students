# -*- coding: utf-8 -*-
"""Core functionality and definitions for the package."""


def get_config_template():

    smtp_dict = {}
    smtp_dict["server"] = None
    smtp_dict["port"] = None
    smtp_dict["username"] = None
    smtp_dict["password"] = None
    smtp_dict["tls"] = None

    database_dict = {}

    logging_dict = {}

    return {
        "smtp": smtp_dict,
        "database": database_dict,
        "logging": logging_dict
    }


class OVOKException(Exception):
    """
    General package wide exception.

    Since all package exceptions inherit from this Exception class, the user
    can do

        try:
            pass
        except OVOKException:
            raise

    to catch all OVOK generated excpetions.
    """

    pass


class VerificationError(OVOKException):
    """Error raised during OAuth verification."""

    pass


class RepositoryError(OVOKException):
    """Error created accessing repository API."""

    pass
