#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for creating webhooks to repositories."""

import json
import ast
import logging
import logging.config
import argparse
import os
import ovok_interface
from ovok_interface import orm
from ovok_interface.util import utility
from ovok_interface.util import utility_scripts
from ovok_interface import VerificationError
from ovok_interface import commands as cmd
from ovok_interface import Configuration

# Setup logging
logger = logging.getLogger(__name__)
logging.captureWarnings(True)


def prepare_webhooks(ovok,
                     repositories,
                     description,
                     url,
                     events,
                     active):
    """
    Create command objects for webhook creation.

    Args:
        ovok(OVOKInterface): OVOK client to carry out the operations.
        repositories(list(GitRepository)): List of repositories.
        namespace(str): Repository namespace.
        description(str): Webhook description.
        url(str): Webhook url.
        events(dict): Webhook event dictionary.
        active(bool): Is the webhook active or not.

    Returns:
        CommandList: List of commands to create the webhooks.
    """
    events = [unicode(event) for event in events]
    commands = cmd.CommandList()
    for repository in repositories:

        if len(repository.webhooks) == 0:

            cmd_create = cmd.CreateWebhookCommand(ovok,
                                                  repository,
                                                  description,
                                                  url,
                                                  events,
                                                  active)
            commands.append(cmd_create)

        else:

            for webhook in repository.webhooks:

                hook_events = json.loads(webhook.events)
                diff = list(set(hook_events).symmetric_difference(events))
                # Search for an existing webhook. Update if necessary.
                #
                # TODO: What if there are multiple already created?
                # It wouldn't be smart to update them all.
                if webhook.description != description \
                   or webhook.url != url \
                   or len(diff) >= 1 \
                   or webhook.active != active:

                    webhook.description = description
                    webhook.url = url
                    webhook.events = json.dumps(events)
                    webhook.active = active

                    cmd_update = cmd.UpdateWebhookCommand(ovok, webhook)
                    commands.append(cmd_update)

    return commands


def update_webhooks(conf,
                    description,
                    url,
                    events,
                    active):
    """
    Create webhooks to repositories.

    Args:
        conf(Configuration): OVOK Configuration.
        namespace(str): Repository namespace.
        description(str): Webhook description.
        url(str): Webhook url.
        events(dict): Webhook events.
        active(bool): Is the webhook active or not.
    """
    key = conf["bitbucket"]["consumer_key"]
    secret = conf["bitbucket"]["consumer_secret"]

    # Create the OVOK Interface ovok
    ovok = ovok_interface.get_client(platform="bitbucket",
                                     consumer_key=key,
                                     consumer_secret=secret)
    ovok.config = conf

    # Set course context
    try:
        utility_scripts.verify_bitbucket(ovok)
    except VerificationError, e:
        print("Bitbucket authentication error: {}".format(str(e)))
        return
    except Exception, e:
        print("Suffered an error while authenticating: {}".format(str(e)))
        return

    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    repositories = ovok.session().query(orm.GitRepository).all()
    commands = prepare_webhooks(ovok,
                                repositories,
                                description,
                                url,
                                events,
                                active)

    if len(commands) > 0:

        print("The following actions are to be taken:")
        print commands.describe()

        ans = raw_input('Continue? [y/n] ')
        if ans in (["Y", "y"]):
            commands.execute()
            ovok.session().commit()

    else:
        print("No need to create new webhooks.")


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    help_string_namespace = "Namespace where the webhooks are created. " \
                            "Given as BitBucket user/team. "
    parser.add_argument("--namespace",
                        help=help_string_namespace,
                        type=str,
                        default=None,
                        required=False)

    help_string_description = "Webhook description."
    parser.add_argument("--description",
                        help=help_string_description,
                        type=str,
                        default=None,
                        required=True)

    help_string_url = "Webhook url."
    parser.add_argument("--url",
                        help=help_string_url,
                        type=str,
                        default=None,
                        required=True)

    help_string_events = "Webhook events."
    parser.add_argument("--events",
                        help=help_string_events,
                        nargs="+",
                        type=str,
                        default=["repo:push"],
                        required=False)

    help_string_active = "Webhook active status."
    parser.add_argument("--active",
                        help=help_string_active,
                        type=bool,
                        default=True,
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    if args.namespace:
        conf["bitbucket"]["namespace"] = args.namespace

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    update_webhooks(conf,
                    args.description,
                    args.url,
                    args.events,
                    args.active)
