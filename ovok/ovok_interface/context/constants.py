"""Constants for handling database operations."""

import os

# Supported database engines
DATABASE_SQLITE = "sqlite"
DATABASE_MYSQL = "mysql"

# Default database engine to use
DATABASE_DEFAULT_ENGINE = DATABASE_SQLITE

DATABASE_SQLITE_CONN_STR_BASE = "sqlite:///"
DATABASE_MYSQL_CONN_STR_BASE = "mysql://"

# Default path to store local databases. Mainly used for SQLITE.
DATABASE_DEFAULT_PATH = \
    os.path.normpath(os.path.join(os.path.dirname(__file__),
                                  "../database/local_databases/"))
