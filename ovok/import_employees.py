#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for importing employees to the OVOK system."""

import logging
import logging.config
import argparse
import csv
import os
import json
import sys

from ovok_interface import OVOKInterface
from ovok_interface import utility
from ovok_interface import orm
from ovok_interface.util import commands as cmd
from ovok_interface import Configuration
from ovok_interface import json_decoder
from ovok_interface.util import utility_scripts

# Setup logging
logger = logging.getLogger(__name__)
logging.captureWarnings(True)


def create_output(course, groups, filename=None):
    """
    Create csv output from the course.

    Args:
        course(Course): Course instance.
        groups(list(StudentGroup)): List of studentgroups in the course.
    """
    # TODO: Create sorted output by lastname

    # Use a dictionary with email as key to get all unique
    # repositories per student
    student_repo_dict = {}
    for group in groups:

        repositories = []
        if len(group.repositories) > 0:
            repositories = [repo.name for repo in group.repositories]

        for student in group.students:

            if student.email not in student_repo_dict:
                student_repo_dict[student.email] = {}

            student_repo_dict[student.email]["lastname"] = \
                student.lastname.encode('utf-8').strip()
            student_repo_dict[student.email]["firstnames"] = \
                student.firstnames.encode('utf-8').strip()
            student_repo_dict[student.email]["student_number"] = \
                str(student.student_id).encode('utf-8').strip()

            if "repositories" not in student_repo_dict[student.email]:
                student_repo_dict[student.email]["repositories"] = []

            # Append only unique items
            for repo in repositories:
                if repo not in \
                   student_repo_dict[student.email]["repositories"]:
                            student_repo_dict[student.email]["repositories"] \
                                .append(repo.encode('utf-8').strip())

    # Output file will be named by course name and year
    if not filename:
        filename = "_".join([course.code, str(course.year)])
        filename += ".csv"

    try:

        with open(filename, 'wb') as output:
            writer = csv.writer(output)

            for key, value in student_repo_dict.items():
                writer.writerow([value["lastname"],
                                 value["firstnames"],
                                 value["student_number"],
                                 value["repositories"]])

    except Exception as e:
        logger.error("Error writing output csv: {}".format(repr(e)))


def get_existing_employee(employee, employees):
    """
    Return existing student from the provided list of students.

    Args:
        employee(Employee): Employee to find.
        students(list(Student)): List of employees to search.

    Returns:
        Student: The existing employee from the list. None if not found.
    """
    for e in employees:
        if e.email == employee.email:
            return e
    return None


def prepare_import(ovok, existing_course, employees):
    """
    Import employees from input.

    Args:
        ovok(OVOKInterface): Ovok client.
        existing_course(Course): An existing course to import admins to.
        employees(list(Employee)): List of employees to import.

    Returns:
        CommandList: List of commands that will import all students.
    """
    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()
    session = ovok.session()

    # Query existing students
    existing_employees = ovok.query(orm.Employee)

    # Cross check all students for updates and create new ones if missing
    for employee in employees:

        # Check if we need to update
        existing_employee = get_existing_employee(employee, existing_employees)
        if existing_employee:

            cmd_modify = cmd.ModifyCommand(session, existing_employee)
            cmd_modify.modify("email", employee.email)
            cmd_modify.modify("firstnames", employee.firstnames)
            cmd_modify.modify("lastname", employee.lastname)
            if cmd_modify.dirty():
                commands.append(cmd_modify)

            # Check for new usernames
            # TODO: Change the database implementation of usernames to
            # association proxy dictionary. All this can and should be avoided.
            for username in employee.usernames:
                for uname in existing_employee.usernames:
                    if uname.platform == username.platform:
                        if uname.username != username.username:
                            c = cmd.ModifyCommand(session, uname)
                            c.modify("username", username.username)
                            commands.append(c)
                        break
                else:

                    # Append a new username to existing user
                    uname_new = orm.Username(username.username,
                                             username.platform)
                    existing_employee.usernames.append(uname_new)
                    commands.append(cmd.CreateCommand(session, uname_new))

        else:

            # Create student as new
            commands.append(cmd.CreateCommand(session, employee))

    return commands


def import_employees(path_admins, config, output=None):
    """
    Import employees.

    Args:
        path_admins(str): Path to the file containing the employees.
        config(Configuration): Configuration settings for OVOK client.
        output(str): If specified, output will be written to the file.
    """
    # Create the OVOK client
    ovok = OVOKInterface()
    ovok.config = config

    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    # Check how existing assignments would be changed
    course = ovok.query(orm.Course)
    if not course or len(course) == 0:
        print("Couldn't get Course instance from database, please check "
              "that you have imported the course data!")
        return
    course = course[0]

    # Read assignments from json
    try:
        input_employees = \
            json.loads(utility.read_file(path_admins),
                       object_hook=json_decoder.decode)
    except IOError, e:
        print("Couldn't read the input file, err: {}".format(str(e)))
        sys.exit(1)

    commands = prepare_import(ovok, course, input_employees)
    if len(commands) > 0:
        print("The following operations will be performed: ")
        print commands.describe()
        ans = raw_input('Continue? [y/n] ')
        if ans not in (["Y", "y"]):
            print("User cancellation of script.")
            return
        else:
            commands.execute()
            ovok.session().commit()
            print("Employees imported.")
    else:
        print("No employees to import")

    if output:
        create_output(course, ovok.query(orm.StudentGroup), filename=output)

if __name__ == '__main__':

    # Parse command line arguments
    parser = argparse.ArgumentParser()

    # Course XML file path
    admin_help_string = "Administrators for repositories. Given as a " \
                        "path to file containing a JSON list of persons. " \
                        "The provided persons must have usernames specified " \
                        "for platforms the course uses."
    parser.add_argument("admins",
                        help=admin_help_string,
                        action="store")

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    help_string_output = "Write student repository data to file."
    parser.add_argument("--output",
                        help=help_string_output,
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    # Import Course
    import_employees(args.admins, conf, output=args.output)

    logger.info("End of script {}".format(__file__))
