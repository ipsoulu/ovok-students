# -*- coding: utf-8 -*-
"""Utility functions for shared CLI script functionality."""

import logging
from ..core import VerificationError
from ..context import constants as db_const
from ..git_helper.git_helper import GitStrategy

logger = logging.getLogger(__name__)


def _format_context_url(context):
    """
    Format context URL for printing.

    Args:
        context(Context): Context you wish to format.

    Returns:
        str: Formatted string representing the host and database
             of the context.
    """
    return " ".join([context.db_uri.host or "localhost",
                     context.db_uri.database or "No database"])


def select_context_from_list(ovok, course, contexts):
    """
    Perform interactive selection of contexts hosting the specified course.

    Args:
        course(Course): Course to look for.
        contexts(list(Context)): List of contexts to search.

    Returns:
        Context: Return the context if selected, else None.
    """
    options = []
    for context in contexts:
        if context.course_code == course.code and \
           context.course_year == course.year:
            options.append(context)

    if len(options) == 0:
        return None
    else:
        return _perform_select_context(ovok, options)


def select_context(ovok):
    """
    Query context selection from user via raw_input().

    Returns:
        Context: The selected context. Can be None.
    """
    return _perform_select_context(ovok, ovok.get_contexts())


def _perform_select_context(ovok, contexts):
    """
    Query context selection from the user.

    Args:
        ovok(OVOKInterface): OVOK client.
        contexts(list(Context)): List of contexts to select from.

    Returns:
        Context: Return the selected context, or None.
    """
    while True:

        print("Please select an existing course context:")
        for index, context in enumerate(contexts):
            addr = _format_context_url(context)
            print("    {ind}) {code} {year} - {name} ({addr})"
                  .format(ind=index,
                          code=context.course_code,
                          year=context.course_year,
                          name=context.course_name,
                          addr=addr))
        print("    {ind}) None of the above".format(ind=len(contexts)))

        selected_index = None
        try:
            selected_index = int(raw_input())
        except Exception:
            print("Couldn't read the provided index, please try again.")

        if selected_index is not None and \
           0 <= selected_index <= len(contexts) - 1:
            return contexts[selected_index]
        elif selected_index is not None and \
           selected_index == len(contexts):
            return None


def create_context_to_host(ovok, course):
    """
    Create a new course context to specific host.

    Args:
        ovok(OVOKInterface): OVOK client.
        course(Course): Course to be hosted in the database.

    Returns:
        Context: Return the created course context.

    Note: This function does not import any data.
    """
    while True:

        # TODO: Make duplicate hosts with different usernames available?
        print("Please select a host:")

        options = ovok.get_context_hosts()
        for index, option in enumerate(options):
            if option.drivername == db_const.DATABASE_SQLITE:
                print("    {ind}) {host} ({drv})"
                      .format(ind=index,
                              host=option.database,
                              drv=option.drivername))
            else:
                print("    {ind}) {host} ({drv})"
                      .format(ind=index,
                              host=option.host,
                              drv=option.drivername))

        selected_index = None
        try:
            selected_index = int(raw_input())
        except Exception:
            print("Couldn't read the provided index, please try again.")

        if selected_index is not None and \
           0 <= selected_index <= len(options) - 1:

            url = options[selected_index]
            return ovok.create_context(course, url)


def select_assignment(assignments, assignment_tag=None):
    """
    Query an assignment from the user using available assignments.

    Args:
        assignments(list(Assignment)): List of assignments to select from.

    Kwargs:
        assignment_tag(str): Search existing assignments by tag,
                             if not found returns None.

    Returns:
        Assignment: Selected assignment, or None if not available
                    or not found.
    """
    assignment = None
    if assignment_tag:

        # Tag was provided, check that it matches an assignment
        for course_assignment in assignments:
            print course_assignment.tag
            if course_assignment.tag == assignment_tag:
                assignment = course_assignment
                break
        else:
            print("No course assignment matches the provided tag {}"
                  .format(assignment_tag))
            return None

    else:

        # Tag wasn't provided, query it from the user
        print("Please select an assignment:")
        while not assignment:

            for index, assig in enumerate(assignments):
                print("    {index}) {tag} ({deadline})"
                      .format(index=index,
                              tag=assig.tag,
                              deadline=assig.deadline))

            selected_index = None
            try:
                selected_index = int(raw_input())
            except Exception:
                print("Couldn't read the provided index, please try again.")

            if selected_index is not None and \
               0 <= selected_index <= len(assignments) - 1:
                assignment = assignments[selected_index]

    return assignment


def select_git_strategy():
    """
    Query Git strategy from user via raw_input().

    Returns:
        int: The selected Git strategy.
    """
    print("Please select a Git cloning strategy:")
    strategy = -1
    while strategy < 0:

        for index, strategy in enumerate(GitStrategy.STRATEGIES):
            description = GitStrategy.STRATEGY_DESCRIPTIONS_LONG[strategy]
            print("    {index}) {strat}"
                  .format(index=index, strat=description))

        selected_index = None
        try:
            selected_index = int(raw_input())
        except Exception:
            print("Couldn't read the provided index, please try again.")

        if selected_index is not None and \
           0 <= selected_index <= len(GitStrategy.STRATEGIES) - 1:
            strategy = GitStrategy.STRATEGIES[selected_index]

    return strategy


def verify_bitbucket(ovok, manual=False):
    """
    Perform OAuth verification to Bitbucket.

    Args:
        ovok(OVOKClientBitBucket): OVOK Bitbucket client.

    Kwargs:
        manual(bool): Use manual authentication over automatic.

    Raises:
        VerificationError: Error in Bitbucket authentication.
    """
    logger.info("Verifying Bitbucket client...")
    if not manual:

        try:
            ovok.verify()
        except VerificationError:
            # Got an exception, fall back to manual authentication
            logger.exception("Error in Bitbucket authentication")
            pass
        else:
            # Automatic verification completed without errors, return back
            logger.info("Bitbucket client succesfully verified")
            return

    # Either the user wants to authenticate manually, or automatic
    # authentication failed
    try:

        # Get authentication data for Bitbucket
        auth_data = ovok.get_authorization_url()

        # The user needs to visit this URL and input the code
        print("Open the following URL: {}".format(auth_data['url']))
        verifier = raw_input('Enter "oauth_verifier" from url: ')

        # Perform authentication
        ovok.verify_manual(auth_data["access_token"],
                           auth_data["access_token_secret"],
                           verifier)

    except VerificationError:
        logger.exception("Error in Bitbucket manual authentication")
        raise
