""" Defines a client class for working with BitBucket repositories. """

from repository import BitBucketRepositoryClient
from urls import repository_creation_url

class BitBucketRepositoriesClient(object):
  """ Client class representing the repositories under a namespace in bitbucket. """
  def __init__(self, dispatcher, access_token, access_token_secret, namespace):
    self._dispatcher = dispatcher
    self._access_token = access_token
    self._access_token_secret = access_token_secret
    self._namespace = namespace

  @property
  def namespace(self):
    """ Returns the namespace. """
    return self._namespace

  def get(self, repository_name):
    """ Returns a client for interacting with a specific repository. """
    return BitBucketRepositoryClient(self._dispatcher, self._access_token,
                                     self._access_token_secret, self._namespace,
                                     repository_name)

  def create(self, repository_name, request_body):
    """ Extension to the CoreOS wrapper. Creates a repository for the user """
    url = repository_creation_url(self._namespace, repository_name)
    return self._dispatcher.dispatch(
      url,
      method='POST',
      access_token=self._access_token,
      access_token_secret=self._access_token_secret,
      json_body=True,
      data=request_body)
