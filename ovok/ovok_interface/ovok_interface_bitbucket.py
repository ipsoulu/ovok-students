# -*- coding: utf-8 -*-
"""Module for handling BitBucket resources."""

# Libraries
import re
import logging
import json

from ovok_interface import OVOKInterface
from core import VerificationError
from core import RepositoryError

from callbacks.callback import TransferProgressCallback

from json_decoder import json_decoder

# OVOK ORM class definitions
import database.ovok_orm_definitions as orm
import database.ovok_bitbucket_definitions as orm_bb

# BitBucket specific definitions
from database.ovok_bitbucket_definitions import Commit
from database.ovok_bitbucket_definitions import Tag
from database.ovok_bitbucket_definitions import Webhook as Webhook_BB

# OAuth handlers for automatic authentication
from oauth_helper.serverhandler import HTTPServerHandler
from oauth_helper.tokenhandler import TokenHandler

# BitBucket wrapper
from bitbucket import BitBucket

import git_helper.git_helper as githelper
from git_helper.git_helper import GitStrategy
from pygit2 import UserPass

# Setup logging
logger = logging.getLogger(__name__)


class OVOKClientBitBucket(OVOKInterface):
    """A client for interacting with BitBucket provided services."""

    def __init__(self, consumer_key, consumer_secret):
        """
        Construct an OVOKClientBitBucket instance.

        Args:
            consumer_key(str): Bitbucket consumer key.
            consumer_secret(str): Bitbucket consumer_secret.
        """
        super(OVOKClientBitBucket, self).__init__()

        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self._bitbucket = BitBucket(self.consumer_key,
                                    self.consumer_secret,
                                    HTTPServerHandler.REDIRECT_URL,
                                    timeout=5)

        # Create an inner bitbucket wrapper. From the CoreOS project.
        self._platform = "bitbucket"

        self._oauth_access_token = None
        self._oauth_access_secret = None

    def _require_oauth_authentication(func):
        def _decorate(self, *args, **kwargs):
            if not self._oauth_access_token or not self._oauth_access_secret:
                raise Exception("OAuth access token and secret have not been "
                                "set properly!")
            return func(self, *args, **kwargs)
        return _decorate

    def create_repository(self, group, name, namespace):
        """
        Create a BitBucket repository for a specific StudentGroup.

        Args:
            group(StudentGroup): The group to create the repository for.
            name(str): Name of the repository.
            namespace(str): The owner of the repository. Can be a user or a
                            team.

        Returns:
            (GitRepository): The created repository object
        """
        # Create the repository
        logger.info("Creating repository for group {}".format(group.id))

        # Get a client for namespace repositories
        client_repositories = self._get_authorized_bb_client() \
            .for_namespace(namespace) \
            .repositories()

        # POST a new repository
        request_body = \
            {'scm': orm.GitRepository._DEFAULT_SCM,
             'is_private': str(orm.GitRepository._DEFAULT_IS_PRIVATE).lower(),
             'fork_policy': orm.GitRepository._DEFAULT_FORK_POLICY}

        (result, data, error_msg) = client_repositories \
            .create(name, request_body)

        if not result:
            raise RepositoryError("Repository {repo} creation error ({err})"
                                  .format(repo=name, err=error_msg))

        # Return the repository object decoded from response JSON
        return json_decoder.decode_as_object(data,
                                             orm.GitRepository,
                                             platform=self._platform)

    def set_repository_permission(self, permission):
        """
        Set repository permission.

        Args:
            permission(RepositoryPermission): Permission to set.
        """
        logger.debug("Setting repository {repo} {perm} permission for "
                     "user {usr}"
                     .format(repo=permission.repository,
                             perm=permission.privilege,
                             usr=permission.username))

        # Create client for a specific repository
        repository_client = \
            self._get_authorized_bb_client() \
            .for_namespace(permission.namespace) \
            .repositories() \
            .get(permission.repository)

        # POST new repository privilege
        (result, data, error_msg) = repository_client\
            .give_user_permission(permission.username,
                                  permission.privilege)

        if not result:
            raise RepositoryError("There was an error giving user permission "
                                  "to repository, error: {}"
                                  .format(error_msg))

    def delete_repository(self, repository):
        """
        Delete a repository.

        Args:
            repository(Repository): Repository to delete.

        Raises:
            RepositoryError: Raised for an error in repository deletion against
                             platform API.
        """
        logger.info("Deleting repository {}".format(repository.name))

        # Create client for a specific repository
        repository_client = \
            self._get_authorized_bb_client() \
            .for_namespace(repository.owner) \
            .repositories() \
            .get(repository.name)

        (result, data, error_msg) = \
            repository_client.delete()
        if not result:
            raise RepositoryError("Repository {} deletion failed ({})"
                                  .format(repository.name,
                                          error_msg))

    def verify(self):
        """
        Verify BitBucket client automatically.

        The OAuth token is retrieved with a listening HTTP Server.

        Manual authentication is the default OAuth process, where the user
        interacts with the browser. Call get_authorization_url() and visit it,
        then verify() if using manual authentication.

        Raises:
            Exception: Error in OAuth authentication.
        """
        if not self.consumer_key or not self.consumer_secret:
            raise VerificationError("BitBucket consumer "
                                    "values not set properly")
        (result, data, error_msg) = self._bitbucket.get_authorization_url()
        if not result:
            logger.warning("Coudln't get OAuth authorization url: {}"
                           .format(error_msg))
            raise VerificationError("Couldn't retrive OAuth authorization "
                                    "url ({})"
                                    .format(error_msg))

        tokenhandler = TokenHandler(self.consumer_key, self.consumer_secret)
        verifier = tokenhandler.get_access_token(data['url'])

        (result, data, error_msg) = \
            self._bitbucket.verify_token(data["access_token"],
                                         data["access_token_secret"],
                                         verifier)

        if not result:
            raise VerificationError("Couldn't verify OAuth access token ({})"
                                    .format(error_msg))

        self._oauth_access_token = data[0]
        self._oauth_access_secret = data[1]

        logger.info("Token verified")

    def get_authorization_url(self):
        """
        Return the authorization URL for OAuth.

        Returns:
            str: The URL used for verification.
        """
        (result, data, error_msg) = self._bitbucket.get_authorization_url()
        if not result:
            raise VerificationError("Couldn't retrieve the authentication "
                                    " URL, error: {}"
                                    .format(error_msg))
        return data

    def verify_manual(self, oauth_access_token, oauth_access_secret, verifier):
        """
        Verify BitBucket client.

        You can get the OAuth authentication data by calling
        'get_authorization_url' function and visiting the URL.

        Args:
            oauth_access_token (str): OAuth access token.
            oauth_access_secret (str): Oauth access secret.
            verifier (str): OAuth verifier from the visited URL.

        Raises:
            Exception: Error in OAuth authentication.

        Returns:
            Nothing.
        """
        logger.info("Verifying token...")
        (result, data, error_msg) = \
            self._bitbucket.verify_token(oauth_access_token,
                                         oauth_access_secret,
                                         verifier)

        if not result:
            raise VerificationError("Couldn't verify OAuth access token ({})"
                                    .format(error_msg))

        self._oauth_access_token = data[0]
        self._oauth_access_secret = data[1]

        logger.info("Token verified")

    @_require_oauth_authentication
    def get_current_authenticated_user(self):
        """
        Return the BitBucket username of the currently authenticated user.

        Returns:
            The namespace of the currently authenticated user.
        """
        # Obtain current user account
        logger.debug("Checking user information...")

        client = self._get_authorized_bb_client()

        (result, data, error_msg) = client.get_current_user()
        if not result:
            raise Exception("There was an error acquiring current user "
                            "information, error: {}".format(error_msg))

        return data['user']['username']

    @_require_oauth_authentication
    def _get_authorized_bb_client(self):
        """
        Return an authenticated client for using BitBucket API.

        Returns:
            BitBucket: Client for accessing BitBucket resources.
        """
        return self._bitbucket.get_authorized_client(self._oauth_access_token,
                                                     self._oauth_access_secret)

    # TODO: Rename "final_date" and "date"  -->  "not_before" and "not_after". First find out which is which :)
    @_require_oauth_authentication
    def get_tag(self,
                tag,
                repository,
                namespace,
                date=None,
                final_date=None):
        """
        Return SHA identifier of git tag.

        Args:
            repository(str): Repository slug (name-in-url).
            namespace(str): Owner of the repository. BitBucket username. Will
                            default to currently authenticated user.
            date(datetime): Date and time for limiting search. Search will be
                            performed BEFORE to the provided date.
        Returns:
            String or None: Commit SHA1 identifier, or None if not found.
        """
        # If the repository owner was not specified, use authenticated user
        if not namespace:
            namespace = self.get_current_authenticated_user()

        # Get a client for accessing BB API repository data
        repository_client = self._get_authorized_bb_client() \
            .for_namespace(namespace) \
            .repositories() \
            .get(repository)

        # Query tags json through BB API
        (result, data, error_msg) = repository_client.get_tags()
        if not result:
            raise Exception("Error retrieving repository {repo} "
                            "tags: {err}"
                            .format(repo=repository,
                                    err=error_msg))

        # Decode json into objects
        raw_tags = json_decoder.decode_as_object(data,
                                                 Tag,
                                                 platform=self._platform)
        if not raw_tags or len(raw_tags) == 0:
            logger.debug("The repository contains no tags")
            return None

        # TODO: Git tags should be unique by commit, check that this is true
        tags = [t for t in raw_tags if t.name == tag]
        if len(tags) == 0:
            logger.debug("The repository contains no matching tags to {}"
                         .format(tag))
            return None
        elif len(tags) == 1:

            if final_date is not None and tags[0].utctimestamp < final_date:
                logger.debug("The tag {} is too far in the past"
                             .format(tag))
                # The commit is too far in the past
                return None
            elif date is not None:
                return tags[0] if tags[0].utctimestamp <= date else None
            else:
                return tags[0]
        else:

            logger.debug("Multiple tags")

            # In case we get multiple tags, don't know if this is possible
            tags.sort(key=lambda k: k.utctimestamp, reverse=True)

            # If necessary, filter by date
            if date:

                # Get the latest commit made within the certain timeframe
                for t in tags:

                    if final_date and t.date < final_date:
                        # The commit is too far in the past
                        return None
                    elif t.date <= date:
                        return t

            # Todo: If there is no lower boundary for date defined then this
            # doesn't return anything.
            # Should this return the latest as find_latest_commit()
            # does return?
            else:
                return tags[0] if tags[0].date > final_date else None

        return None

    @_require_oauth_authentication
    def find_latest_commit(self,
                           repository,
                           namespace=None,
                           date=None,
                           final_date=None,
                           branchortag=None,
                           include_parents=False):
        """
        Find the latest commit from repository.

        Args:
            repository(str): Repository slug (name-in-url).
            namespace(str): Owner of the repository. BitBucket username. Will
                            default to currently authenticated user.
            date(datetime): Date and time for limiting search. Search will be
                            performed BEFORE to the provided date.
            final_date(datetime): Date and time for limiting search.
                                  Search will be terminated AFTER the provided
                                  date.
            branchortag(str): CURRENTLY ONLY WORKS FOR BRANCHES.
                              Branch OR Tag name used for filtering. If the
                              repository has multiple refspecs named after the
                              parameter, the behaviour of this function cannot
                              be guaranteed to be right! For example: when
                              there exists a branch and a tag with the same
                              name.
            include_parents(bool): NOT IMPLEMENTED YET.
                                   Include commits from parent branches.
                                   For example, if the final commit has a
                                   parent commit in another branch, the other
                                   branch will also be included in the results.
        Returns:
            Commit: The commit found or None.
        """
        # Important note to maintainer!
        #
        # Git allows tags and branches to be named identically.
        # Or not exactly true, but you can name your refspecs the same way,
        # for example: refs/heads/duplicate and refs/tags/duplicate
        #
        # This means we can't know beforehand how the API will behave,
        # as it is controlled by "branchortag" attribute used in querying.
        #
        # If you want to be certain that you get commits that start from
        # the provided tag, you can "cheat" the API and include the same
        # branch name as "branchortag" as a query parameter.
        # Note that this will result the latest tag being the first,
        # which might be of no use if we need to filter by date!
        #
        # It's a bit stupid like that...
        #
        # To replicate, try (replace commit sha with a real value):
        #   git checkout -b testbranch
        #   git tag -a testbranch 8dad165 -m "Message here"
        # Now if you try to push with:
        #   git push --tags origin testbranch
        # Git will throw an error and say it matches multiple refspecs, try:
        #   git push --tags origin refs/tags/testbranch
        # Congratulations, now you have a duplicate branch and tag name.
        #
        # So what does this mean?
        # If there is a branch that shares a same name with a tag:
        #   The current behaviour of this function will retrieve all commits
        #   associated with the branch, without any tag filtering!
        # If there is no duplicates, retrieves a commit pointing to either
        # a tag or a branch, whichever exists.
        #
        # So basically we are left with date filtering over what we got from
        # the BitBucket API.

        # If we don't explicitly exclude parent branches, the commits
        # retrieved will include commits from the branches parents.
        # This means that if the branch was started from master, unless we
        # exclude master, it's commits will be included.

        # Get client for retrieving changesets
        client = self._get_authorized_bb_client()

        # If the repository owner was not specified, use authenticated user
        if not namespace:
            namespace = self.get_current_authenticated_user()

        logger.debug("Starting paginate search in repo: {repo}, "
                     "branchortag: {tag}, date: {date} "
                     "final_date: {fdate}"
                     .format(repo=repository,
                             date=date,
                             fdate=final_date,
                             tag=branchortag))

        # Get a client for accessing BB API repository data
        repository_client = client \
            .for_namespace(namespace) \
            .repositories() \
            .get(repository)

        request_count = 0
        MAX_REQUESTS = 100
        while(request_count < MAX_REQUESTS):

            request_count += 1
            logger.debug("Request count {}".format(request_count))

            # TODO: Fix this, there is some sort of an error with branchortag
            # attribute. And it hasn't been tested what happens if there is no
            # branch named branchortag

            (result, data, error_msg) = \
                repository_client.get_commits(branchortag=branchortag,
                                              page=request_count)

            # Query tags json through BB API
            # (result, data, error_msg) = \
            #     repository_client.get_commits(include=branchortag,
            #                                   branchortag=branchortag,
            #                                   page=request_count)
            if not result:
                raise Exception("Error retrieving repository {repo} "
                                "commits: {err}"
                                .format(repo=repository,
                                        err=error_msg))

            # Decode json into objects
            commits = \
                json_decoder.decode_as_object(data,
                                              Commit,
                                              platform=self._platform)

            # If there are no commits, return
            if len(commits) == 0:
                return None

            # Sort by commit date, descending
            commits.sort(key=lambda k: k.date, reverse=True)

            # If necessary, filter by date
            if date:

                # Get the latest commit made within the certain timeframe
                for commit in commits:

                    if final_date and commit.date < final_date:
                        # The commit is too far in the past
                        return None
                    elif commit.date <= date:
                        return commit

            else:

                if final_date:
                    return commits[0] if commits[0].date > final_date else None
                else:
                    return commits[0]

            # Check if we should terminate loop
            if len(commits) < data["pagelen"]:
                break

        return None

    @_require_oauth_authentication
    def post_comment_to_commit(self, repository, node_id, comment):
        """
        Post a comment to a BitBucket commit.

        Posts a comment to a certain commit through the BitBucket API.
        The authenticated user has to have correct permissions to the
        BitBucket repository.

        Args:
            repository(str): Repository slug (name-in-url).
            node_id(str): Commit node identifier where the comment is to be
                          posted. Given as an SHA1 hash from changeset.
            comment(str): Comment to post.

        Returns:
            bool: Success of the comment post.
        """
        # Validate the length of the commit SHA identifier
        node_id = node_id.strip()
        if not re.match(r"[0-9a-f]{5,40}", node_id):
            raise Exception("The provided SHA node identifier: {} "
                            "is invalid."
                            .format(node_id))

        # Get a BitBucket client
        client = self._get_authorized_bb_client()
        namespace = self.get_current_authenticated_user()

        # Create a client for accessing repository changesets
        changeset_client = client \
            .for_namespace(namespace) \
            .repositories() \
            .get(repository) \
            .changesets()

        (result, data, error_msg) = \
            changeset_client.comment_post(node_id, comment)

        if not result:
            logger.warning("Comment posting failed on commit {sha}, "
                           "error: {err}"
                           .format(sha=node_id,
                                   err=error_msg))
            return False

        logger.info("Posted a comment to BitBucket commit {}".format(node_id))
        return True

    def get_bb_commit_comment_url(self, commit_sha, namespace, repository):
        """Return the URL to examine comments on certain commit."""
        return "/".join(["https://bitbucket.org",
                         namespace,
                         repository,
                         "commits",
                         commit_sha])

    @_require_oauth_authentication
    def update_webhook_data(self, webhook):
        """
        Update webhook data.

        Args:
            webhook(Webhook): Webhook to update.

        Returns:
            Webhook: The updated webhook.
        """
        try:

            webhook_client = self._get_authorized_bb_client() \
                .for_namespace(webhook.repository.owner) \
                .repositories() \
                .get(webhook.repository.name) \
                .webhooks()

            (result, data, error_msg) = \
                webhook_client.update(webhook.uuid,
                                      webhook.description,
                                      webhook.url,
                                      json.loads(webhook.events))

        except Exception:
            raise
        else:

            if not result:
                raise Exception(error_msg)

        return webhook

    @_require_oauth_authentication
    def get_repository_permissions(self, repository):
        """
        Return user permissions for an existing repository.

        Args:
            repository(Repository): Repository to query.

        Returns:
            list(RepositoryPermission): List of repository permissions.

        Raises:
            RepositoryError: Raised when an API error is encountered.
        """
        repository_client = self._get_authorized_bb_client() \
            .for_namespace(repository.owner) \
            .repositories() \
            .get(repository.identifier)

        (result, data, error_msg) = repository_client.list_user_permissions()
        if not result:
            raise RepositoryError("Couldn't retrieve repository {} "
                                  "permissions ({})".format(repository.name,
                                                            error_msg))

        return json_decoder.decode_as_object(data,
                                             orm_bb.RepositoryPermission,
                                             platform=self._platform)

    def clone_repository(self,
                         url,
                         path,
                         username,
                         password,
                         strategy=GitStrategy.DEFAULT):
        """
        Clone repository.

        Args:
            url(str): Repository url.
            parh(str): Path for the cloned repo.
            username(str): Git username.
            password(str): Git password.
            strategy(int): Git clone strategy.

        Returns:
            pygit2.Repository: The cloned repository.
        """
        credentials = UserPass(username, password)
        callbacks = TransferProgressCallback(credentials=credentials)
        return githelper.clone(url,
                               path,
                               callbacks=callbacks,
                               strategy=strategy)

    def checkout_tag(self, path, tag):
        """
        Checkout commit for specified Tag from local repository.

        Args:
            path(str): Path to the repository.
            tag(str): Tag to checkout.
        """
        if not tag.startswith("refs/tags"):
            tag = "/".join(["refs/tags", tag])
        return githelper.checkout_tag(path, tag)

    def create_webhook(self,
                       repository,
                       namespace,
                       description,
                       url,
                       events,
                       active=True):
        """
        Create a new webhook.

        Args:
            repository(str): Repository slug.
            namespace(str): Repository namespace.
            description(str): Webhook description.
            url(str): Webhook url.
            events(dict): Webhook events.
            active(bool): Is the webhook active or not.

        Returns:
            Webhook: The created webhook.
        """
        try:
            webhook_client = self._get_authorized_bb_client() \
                .for_namespace(namespace) \
                .repositories() \
                .get(repository) \
                .webhooks()
            (result, data, error_msg) = \
                webhook_client.create(description, url, events, active)
        except Exception:
            raise
        else:
            if not result:
                raise Exception(error_msg)
            decoded_hook = \
                json_decoder.decode_as_object(data,
                                              Webhook_BB,
                                              platform=self._platform)
            webhook = orm.Webhook()
            webhook.uuid = decoded_hook.uuid
            webhook.url = decoded_hook.url
            webhook.created_at = decoded_hook.created_at
            webhook.description = decoded_hook.description
            webhook.active = decoded_hook.active
            webhook.events = decoded_hook.events
            webhook.subject = decoded_hook.subject

            return webhook
