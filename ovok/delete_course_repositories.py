#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for deleting course repositories."""

import argparse
import logging
import logging.config
import os

import ovok_interface
from ovok_interface import orm
from ovok_interface.util import utility
from ovok_interface.util import utility_scripts
from ovok_interface import VerificationError
from ovok_interface import Configuration
from ovok_interface import commands as cmd

# Setup logging
logging.captureWarnings(True)

logger = logging.getLogger(__name__)


def prepare_deletion(ovok, repositories):
    """
    Prepare commands for repository deletion.

    Args:
        ovok(OVOKClientBitBucket): OVOK client for bitbucket.
        repositories(list(Repository)): List of repositories to delete.

    Returns:
        list(Command): List of commands to execute for repository deletion.
    """
    session = ovok.session()

    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()
    for repository in repositories:

        cmd_delete = cmd.DeleteRepositoryCommand(ovok,
                                                 repository,
                                                 session=session)
        commands.append(cmd_delete)

    return commands


def delete_repositories(config, manualauth=False):
    """
    Delete all repositories from course.

    Args:
        config(Configuration): Configuration for OVOK client.

    Kwargs:
        manualauth(bool): Skip automatic OAuth and do it manually if True.
    """
    key = conf["bitbucket"]["consumer_key"]
    secret = conf["bitbucket"]["consumer_secret"]

    # Create the OVOK Interface ovok
    ovok = ovok_interface.get_client(platform="bitbucket",
                                     consumer_key=key,
                                     consumer_secret=secret)
    ovok.config = config

    # Set course context
    try:
        utility_scripts.verify_bitbucket(ovok, manual=manualauth)
    except VerificationError, e:
        print("Bitbucket authentication error: {}".format(str(e)))
        return
    except Exception, e:
        print("Suffered an error while authenticating: {}".format(str(e)))
        return

    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    # Get Course from database
    course = ovok.session().query(orm.Course).one_or_none()
    if not course:
        print("Couldn't get Course instance from database, please check "
              "that you have imported the course data!")
        return

    repositories = ovok.query(orm.Repository)
    if len(repositories) == 0:
        print("The Course contains no repositories")

    commands = prepare_deletion(ovok, repositories)
    if len(commands) > 0:

        print("The following actions are to be taken:")
        print commands.describe()

        ans = raw_input('Continue? [y/n] ')
        if ans in (["Y", "y"]):
            commands.execute()
            ovok.session().commit()

    else:
        print("There are no repositories to delete")

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    # Authenticate manually?
    oauth_manual_helpstr = "Perform OAuth authentication steps manually."
    parser.add_argument("--manualauth",
                        help=oauth_manual_helpstr,
                        action="store_true",
                        required=False)

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    # Import student groups
    delete_repositories(conf, manualauth=args.manualauth)

    logger.info("End of script {}".format(__file__))
