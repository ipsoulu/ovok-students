"""Module containing the TokenHandler for automatic OAuth token retrieval."""
from BaseHTTPServer import HTTPServer
from serverhandler import HTTPServerHandler
from webbrowser import open_new

# TODO: Figure out if it's worth to close the browser tab. Seems very hard...
# TODO: Integrate serviceproviders from serverhandler.py to this file.


class TokenHandler:
    """Class used to handle OAuth requests automatically."""

    # Port to serve
    PORT = 8080

    def __init__(self, oauth_key, oauth_secret):
        """
        Construct a TokenHandler.

        Args:
            oauth_key(str): OAuth key.
            oauth_secret(str): OAuth secret.
        """
        self._key = oauth_key
        self._secret = oauth_secret

    def get_access_token(self, access_uri):
        """
        Get OAuth access token from provider.

        This function opens an HTTPServer that waits for an OAuth provider
        redirection and then reads the token used for authentication.
        The current thread blocks until the request is served!

        Args:
            access_uri(str): The URI to OAuth provider.

        Returns:
            str: The access token from OAuth.
        """
        # Open the uri in browser tab
        print("A new tab has been opened in your browser for OAuth "
              "verification, please visit your browser and sign in.")
        open_new(access_uri)

        # Create new anonymous handlers that accept oauth keys and secrets
        creator = lambda request, address, server: HTTPServerHandler(
            request, address, server, self._key, self._secret)

        # Create the server and serve a single call
        httpServer = HTTPServer(('localhost', self.PORT), creator)
        httpServer.handle_request()

        # Return the access token from OAuth
        return httpServer.access_token
