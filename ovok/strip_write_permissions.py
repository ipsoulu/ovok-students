#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for setting all write git permissions to read."""

import argparse
import logging
import logging.config
import os

import ovok_interface
from ovok_interface import orm
from ovok_interface import orm_bb
from ovok_interface.util import utility
from ovok_interface.util import utility_scripts
from ovok_interface import VerificationError
from ovok_interface import commands as cmd
from ovok_interface import Configuration
from ovok_interface import RepositoryError

# Setup logging
logging.captureWarnings(True)
logger = logging.getLogger(__name__)


def scan_repository_permissions(ovok, repositories):
    """
    Scan repositories for missing permissions.

    Args:
        ovok(OVOKClientBitBucket): OVOK bitbucket client.
        repositories(list(GitRepository)): List of repositories to scan.

    Returns:
        CommandList: List of commands to carry out synchronization.
    """
    session = ovok.session()

    commands_sync = cmd.CommandList()
    commands_sync.describer = cmd.CommandDescriber()
    for repository in repositories:

        permissions = []
        try:
            permissions.extend(ovok.get_repository_permissions(repository))
        except RepositoryError:
            print("Couldn't get repository {} permissions, skipping repository"
                  .format(repository.name))
            continue
        else:

            for permission in permissions:

                # Reduce Writes to reads
                if permission.privilege == orm_bb.RepositoryPermission.WRITE:

                    privilege = orm_bb.RepositoryPermission.READ
                    namespace = permission.namespace
                    new_perm = \
                        orm_bb.RepositoryPermission(permission.username,
                                                    privilege,
                                                    permission.repository,
                                                    namespace=namespace)
                    cmd_permission = \
                        cmd.RepositoryPermissionCommand(ovok,
                                                        new_perm,
                                                        session=session)
                    commands_sync.append(cmd_permission)

    return commands_sync


def strip_permissions(config, manualauth):
    """
    Downgrade all WRITE permissions to READs.

    Args:
        config(Configuration): Configuration for OVOK client.
        manualauth(bool): Skip automatic OAuth and do it manually if True.
    """
    key = conf["bitbucket"]["consumer_key"]
    secret = conf["bitbucket"]["consumer_secret"]

    # Create the OVOK Interface ovok
    ovok = ovok_interface.get_client(platform="bitbucket",
                                     consumer_key=key,
                                     consumer_secret=secret)
    ovok.config = conf

    # Set course context
    try:
        utility_scripts.verify_bitbucket(ovok, manual=manualauth)
    except VerificationError, e:
        print("Bitbucket authentication error: {}".format(str(e)))
        return
    except Exception, e:
        print("Suffered an error while authenticating: {}".format(str(e)))
        return

    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    repositories = ovok.query(orm.GitRepository)
    commands = scan_repository_permissions(ovok, repositories)
    if len(commands) == 0:
        print("Repository permissions are as they should be.")
    else:

        print("The following actions are to be taken:")
        print commands.describe()

        ans = raw_input('Continue? [y/n] ')
        if ans in (["Y", "y"]):
            commands.execute()
            print("Done.")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    help_string_course_code = "Course code"
    parser.add_argument("--coursecode",
                        help=help_string_course_code,
                        type=str)

    help_string_course_year = "Course year"
    parser.add_argument("--year",
                        help=help_string_course_year,
                        type=int)

    help_string_course_namespace = "The namespace that owns the repositories."
    parser.add_argument("--namespace",
                        help=help_string_course_namespace,
                        type=str,
                        required=False)

    # Authenticate manually?
    oauth_manual_helpstr = "Perform OAuth authentication steps manually."
    parser.add_argument("--manualauth",
                        help=oauth_manual_helpstr,
                        action="store_true",
                        default=False,
                        required=False)

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    # Import student groups
    strip_permissions(conf, args.manualauth)

    logger.info("End of script {}".format(__file__))
