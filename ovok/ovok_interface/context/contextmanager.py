# -*- coding: utf-8 -*-
"""Class for managing course contexts."""

import os
import logging
from sqlalchemy.engine.url import URL

from ..context.contextloader import ContextLoader
import constants as db_const

logger = logging.getLogger(__name__)


class ContextManager(object):
    """Helper class for context management."""

    def __init__(self, sqlite_path=db_const.DATABASE_DEFAULT_PATH):
        """
        Construct a ContextManager instance.

        Kwargs:
            sqlite_path(str): Path to search sqlite databases from. See default
                              from db constants.
        """
        self._contexts = []
        self._curr_context = None
        self._session = None
        self._sqlite_path = sqlite_path
        self._databases = []
        self.contextloader = ContextLoader()
        self.connect_args = {"connect_timeout": 10,
                             "charset": "utf8"}

    def _lazy_load(func):
        def _decorate(self, *args, **kwargs):
            if len(self._contexts) == 0:
                self._load_contexts()
            return func(self, *args, **kwargs)
        return _decorate

    def _load_contexts(self):
        """Load all available Contexts from configured databases."""
        logger.info("Loading contexts, this might take a while")

        # Remove loaded ones and maintain current one
        self._contexts = []
        if self._curr_context:
            self._contexts.append(self._curr_context)

        # Make sure the default database option is always present
        for db_dict in self.databases:
            if db_const.DATABASE_SQLITE in db_dict:
                if db_dict[db_const.DATABASE_SQLITE]["server"] \
                   == db_const.DATABASE_DEFAULT_PATH:
                    break
        else:

            logger.debug("Adding default SQLite to databases.")

            if not os.path.exists(db_const.DATABASE_DEFAULT_PATH):
                try:
                    os.makedirs(db_const.DATABASE_DEFAULT_PATH)
                except Exception:
                    logger.exception("Unable to create default SQLite "
                                     "database path {}"
                                     .format(db_const.DATABASE_DEFAULT_PATH))

            # Default not found, add it
            default_dict_contents = {
                "username": None,
                "password": None,
                "server": db_const.DATABASE_DEFAULT_PATH,
                "port": None
            }
            default_dict = {db_const.DATABASE_SQLITE: default_dict_contents}
            self.databases.append(default_dict)

        for db_dict in self._databases:
            try:
                self._contexts.extend(self.contextloader
                                      .load_contexts(db_dict,
                                                     **self.connect_args))
            except Exception:
                logger.exception("Couldn't load database")

        logger.debug("Contexts loaded: {}".format(str(len(self._contexts))))

    def get_hosts(self):
        """
        Return all unique database URLs.

        Returns:
            list(URL): List of SQLAlchemy URLs from different hosts.
        """
        hosts = []
        for db_dict in self._databases:
            for key, values in db_dict.iteritems():

                url = None
                if key.lower() == db_const.DATABASE_SQLITE:

                    url = URL(key,
                              username=None,
                              password=None,
                              host=None,
                              port=values["port"],
                              database=values["server"])

                else:

                    url = URL(key,
                              username=values["username"],
                              password=values["password"],
                              host=values["server"],
                              port=values["port"])

                if url not in hosts:
                    hosts.append(url)

        return hosts

    @property
    def databases(self):
        """
        Return configured databases.

        Returns:
            list(dict(dict)): List of database dictionaries.
        """
        return self._databases

    @databases.setter
    def databases(self, new_arr):
        # TODO: Figure out how to optimize this.

        self._databases = new_arr
        self._load_contexts()

    @property
    def context(self):
        """
        Current context.

        Args:
            context(Context): Current context.
        """
        return self._curr_context

    @context.setter
    def context(self, context):
        logger.debug("Setting context")

        if context is None:

            self._curr_context = None
            self._session = None

        else:

            # Request database session with the context
            try:
                self._session = self.contextloader \
                    .create_session(context.db_uri)
            except:
                logger.exception("Unable to build database session")
                # TODO: Catch meaningful exceptions and raise proper errors
                raise

            # Set current inner context
            self._curr_context = context
            if context not in self._contexts:
                self._contexts.append(context)

        logger.info("Current context set to {}".format(context))

    @_lazy_load
    def get_contexts(self):
        """Return a list of available contexts."""
        # TODO: Implement force reload.
        # If implemented, the current context loading should check if the
        # context is already present or not before appending it to collection.
        # Also what happens to current context?
        return self._contexts

    @_lazy_load
    def get_context_course(self, course):
        """
        Return a context for certain course.

        Args:
            course(Course): Course to look context for.

        Returns:
            Context: The context or None if not found.
        """
        # Search for proper context
        contexts = []
        for c in self._contexts:
            if c.course_code == course.code and course.year == c.course_year:
                contexts.append(c)
        return contexts

    def session(self):
        """Return database session for the current context."""
        return self._session

    def create_context(self, course, database_url):
        """
        Create a new context.

        Args:
            course(Course): Course to create the context for.
            database_url(URL): SQLAlchemy database URL.

        Returns:
            Context: The new context for the course.
        """
        context = self.contextloader.create_context(database_url,
                                                    course,
                                                    **self.connect_args)

        # TODO: Check if the context already exists
        self._contexts.append(context)
        logger.debug("New context created {}".format(context))

        return context
