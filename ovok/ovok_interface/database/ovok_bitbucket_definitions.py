"""Module for BitBucket specific class representations."""


class RepositoryPermission(object):
    """Defines a single permission in a repository."""

    READ = "read"
    WRITE = "write"
    ADMIN = "admin"
    _allowed_privileges = [READ, WRITE, ADMIN]

    def __init__(self,
                 username,
                 privilege,
                 repository,
                 namespace=None):
        """
        Construct a RepositoryPermission.

        Args:
            username(str): BitBucket username.
            privilege(str): Privilege of the permission.
            repository(str): Slug of the repository.
            namespace(str): Namespace of the repository.

        Returns:
            RepositoryPermission: The constructed instance.
        """
        if privilege not in self._allowed_privileges:
            raise Exception("Undefined privilege {}".format(privilege))

        self.username = username
        self.privilege = privilege
        self.repository = repository
        self.namespace = namespace


class Changeset(object):
    """BitBucket Changeset object."""

    def __init__(self,
                 node,
                 files,
                 raw_author,
                 utctimestamp,
                 timestamp,
                 author,
                 raw_node,
                 parents,
                 branch,
                 message,
                 revision=None,
                 size=-1):

        self.node = node
        self.files = files if files else []
        self.raw_author = raw_author
        self.utctimestamp = utctimestamp
        self.timestamp = timestamp
        self.author = author
        self.raw_node = raw_node
        self.parents = parents
        self.branch = branch
        self.message = message
        self.revision = revision
        self.size = size


class Commit(object):
    """BitBucket Commit object."""

    def __init__(self,
                 hash,
                 repository,
                 links,
                 author,
                 parents,
                 date,
                 message,
                 participants=None):

        self.hash = hash
        self.repository = repository
        self.links = links
        self.author = author
        self.parents = parents
        self.date = date
        self.message = message
        self.participants = participants


class Tag(object):
    """BitBucket Tag object."""

    def __init__(self,
                 name,
                 node,
                 files,
                 branches,
                 raw_author,
                 utctimestamp,
                 author,
                 timestamp,
                 raw_node,
                 parents,
                 branch,
                 message,
                 revision,
                 size=-1):

        self.name = name
        self.node = node
        self.files = files
        self.branches = branches
        self.raw_author = raw_author
        self.utctimestamp = utctimestamp
        self.author = author
        self.timestamp = timestamp
        self.raw_node = raw_node
        self.parents = parents
        self.branches = branches
        self.branch = branch
        self.message = message
        self.revision = revision
        self.size = size


class Webhook(object):
    """BitBucket webhook object."""

    def __init__(self,
                 uuid,
                 url,
                 created_at,
                 description,
                 active,
                 events,
                 subject):

        self.uuid = uuid
        self.url = url
        self.created_at = created_at
        self.description = description
        self.active = active
        self.events = events
        self.subject = subject
