"""Module for decoding WebOodi XML files into OVOK ORM objects."""

import re
import codecs
import xmltodict
import collections

from ..database.ovok_orm_definitions import Course
from ..database.ovok_orm_definitions import Student
from ..database.ovok_orm_definitions import Username

import logging

# Setup logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# Need to define namespaces that we want to exclude from dictionary
# created by xml2dict
# This doesn't remove the xml-objects but removes the namespace
# definition from the created dictionary
namespaces = \
    {
        'http://weboodi.oulu.fi/oodi/mallit/ns/weboodi/': None,
        'http://www.w3.org/2001/XMLSchema-instance': None,
    }


def _convert(xml_file, namespaces, xml_attribs=False, process_namespaces=True):

    with codecs.open(xml_file, "rb", encoding="utf-8", errors="ignore") as f:

        txt = f.read()

        d = xmltodict.parse(txt,
                            xml_attribs=xml_attribs,
                            process_namespaces=process_namespaces,
                            namespaces=namespaces,
                            encoding=None)

    return d


def _remove_non_alphanumeric_chars(string):

    # TODO: This should propably be implemented with some sort of decorator
    # that automatically parses input before decoding functions are called.

    # User a regular expression that substitutes all non-word and whitespace
    # unicode characters. ^ Means negation, \w means characters and the blank
    # after that includes spaces. Unicode flag ensures that finnish special
    # characters are not substituted.
    # string.strip() removes whitespace from beginning and end.
    # This is because input strings are allowed to contain whitespace in them.
    result = re.sub('[^\w -]', '', string.strip(), flags=re.UNICODE)
    return result


def decode_weboodi(xmlfile_path, extract_usernames=False, platform=None):
    return decode_xml_course(xmlfile_path), \
        decode_xml_students(xmlfile_path,
                            extract_usernames=extract_usernames,
                            platform=platform)


def decode_xml_course(xmlfile_path):
    """
    Read and decode WebOodi XML course export into OVOK objects.

    Args:
        xmlfile_path(str): XML file path.

    Returns:
        Course: The course object decoded from XML.
    """
    # Extract xml data to python dictionary
    course_dict = _convert(xmlfile_path, namespaces)

    course_meta_dict = course_dict["opetustapahtumat"]["opettap"]

    code = course_meta_dict["opinkohttunn"]
    prefix = course_meta_dict["opettapnim"][0:3]
    name = course_meta_dict["opinkohtnim"]
    year = int(course_meta_dict["alkpvm"].split("-")[0])

    course = Course(code, prefix, name, year)
    return course


def decode_xml_students(xmlfile, extract_usernames=False, platform=None):
    """
    Read and decode WebOodi XML export students into OVOK objects.

    Args:
        xmlfile_path(str): XML file path.
        extract_usernames(bool): Try to extract username objects from student
                                 records.
        platform(str): Platform for usernames. BitBucket, Github etc. Must be
                       provided if extract_usernames parameter is true!

    Returns:
        list(Student): Students decoded from XML.
    """
    # Extract xml data to python dictionary
    course_dict = _convert(xmlfile, namespaces)

    # Exctract information about persons that have enrolled to course
    students_dict = course_dict["opetustapahtumat"] \
                               ["opettap"] \
                               ["opetustapahtumaosallistujat"] \
                               ["henkilo"]
    
    # Fix a list if there only is a single student
    try:
    	for student in students_dict:
    		student["suknim"]
    except Exception:
    	students_dict = [students_dict]

    studentlist = []
    for student in students_dict:

        lastname = _remove_non_alphanumeric_chars(student["suknim"])
        firstnames = _remove_non_alphanumeric_chars(student["etunim"])
        student_id = int(_remove_non_alphanumeric_chars(student["opisnro"]))
        email = student["sahkpostosoi"]

        student_obj = Student(firstnames, lastname, student_id, email)

        # TODO: Figure out a way to do this properly.
        # There can be multiple different questions.
        # This only reads the first answer!

        # Determine if we try to read Usernames from XML
        if extract_usernames:

            try:

                # TODO: A proper way to check platform.
                # Need to also check if a correct platform is given.
                if not platform:
                    raise Exception("No platform specified for usernames!")

                if "lisatiedot" in student:

                    # TODO: Parse and filter out incorrectly
                    # formatted usernames

                    # Extract usernames
                    extra_data = student["lisatiedot"]

                    if "lisatietokysymykset" in extra_data:

                        uname = ''
                        if isinstance(extra_data["lisatietokysymykset"], collections.OrderedDict):
                            uname = \
                                _remove_non_alphanumeric_chars(extra_data["lisatietokysymykset"]["vastaus"][0])

                        else:
                            uname = \
                                _remove_non_alphanumeric_chars(extra_data["lisatietokysymykset"]["vastaus"])

                        if not uname:
                            raise Exception("No valid username was read.")

                        if " " in uname:
                            raise Exception(u"Username \"{}\" contains spaces "
                                            "after parsing, it might be malformed"
                                            .format(uname))
                        username = Username(uname.lower(), platform)

                        student_obj.usernames.append(username)

                    elif "yleinenlisatieto" in extra_data:

                        uname = \
                            _remove_non_alphanumeric_chars(extra_data["yleinenlisatieto"])
                        if " " in uname:
                            raise Exception(u"Username \"{}\" contains spaces "
                                            "after parsing, it might be malformed"
                                            .format(uname))
                        username = Username(uname.lower(), platform)

                        student_obj.usernames.append(username)

                    # logger.debug(u"Registered a username for {plat} "
                    #              "user {usr}"
                    #              .format(plat=platform,
                    #                      usr=student_obj.get_readable_name()))

                # else:
                #     logger.debug("No extra information specified for person "
                #                  "in XML")
                #     pass

            except Exception:
                logger.exception("Unable to parse username from XML")

        studentlist.append(student_obj)

    return studentlist
