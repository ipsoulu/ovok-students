# -*- coding: utf-8 -*-
"""OVOK course context definitions."""


class Context():
    """OVOK context."""

    def __init__(self, course_code, course_year, db_uri, course_name=None):
        """
        Construct a course context.

        Args:
            course_code(str): Weboodi code for the course.
            course_year(int): Year of the course instance.
            db_uri(sqlalchemy.engine.url.URL): URL for the database.
        """
        self.course_code = course_code
        self.course_year = course_year
        self.course_name = course_name
        self.db_uri = db_uri
