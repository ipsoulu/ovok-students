# -*- coding: utf-8 -*-
"""Module for custom pygit2 callbacks."""

import pygit2


class TransferProgressCallback(pygit2.RemoteCallbacks):

    def transfer_progress(self, stats):

        fetch_percent = (100 * stats.received_objects) / stats.total_objects
        index_percent = (100 * stats.indexed_objects) / stats.total_objects
        kbytes = stats.received_bytes / 1024

        print("\rnetwork {} ( {} kb, {} / {} ) | index {} ( {} / {} )"
              .format(fetch_percent,
                      kbytes,
                      stats.received_objects,
                      stats.total_objects,
                      index_percent,
                      stats.indexed_objects,
                      stats.total_objects)),

    def sideband_progress(self, string):
        print("\rFrom server: {}".format(string)),

    def credentials(self, url, username_from_url, allowed_types):
        """ Credentials callback
            If the remote server requires authentication, this function will
            be called and its return value used for authentication.
        """
        return self.credentials
