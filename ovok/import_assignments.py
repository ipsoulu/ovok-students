#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script for importing OVOK course assignments."""

import logging
import logging.config
import argparse
import json
import sys
import os

from ovok_interface import OVOKInterface
from ovok_interface import Configuration
from ovok_interface import json_decoder
from ovok_interface.util import utility
from ovok_interface.util import utility_scripts
from ovok_interface import orm
from ovok_interface.util import commands as cmd

logger = logging.getLogger(__name__)
logging.captureWarnings(True)


def get_existing_assignment(assignment, assignments):
    """
    Return an existing assignment.

    Args:
        assignment(Assignment): Assignment to search for.
        assignments(list(Assignment)): List of assignments to search.

    Returns:
        Assignment: Return the assignment if found, otherwise None.
    """
    for a in assignments:
        if a.tag == assignment.tag:
            return a
    return None


def prepare_import(ovok, existing_course, assignments):
    """
    Create commands to import / modify the assignments with.

    Args:
        ovok(OVOKInterface): OVOK client.
        existing_course(Course): Existing course instance.
        assignments(list(Assignment)): List of assignments to import.

    Returns:
        CommandList: List of commands to carry out the importing.
    """
    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()
    session = ovok.session()

    existing_assignments = ovok.query(orm.Assignment)

    for assignment in assignments:

        existing_assignment = get_existing_assignment(assignment,
                                                      existing_assignments)
        if existing_assignment:

            # Modify existing
            if existing_assignment not in existing_course.assignments:
                existing_course.assignments.append(existing_assignment)

            cmd_modify = cmd.ModifyCommand(session, existing_assignment)
            cmd_modify.modify("description", assignment.description)
            cmd_modify.modify("deadline", assignment.deadline)
            cmd_modify.modify("not_before", assignment.not_before)
            cmd_modify.modify("root_folder", assignment.root_folder)
            cmd_modify.modify("jenkins_job", assignment.jenkins_job)
            cmd_modify.modify("required_folders", assignment.required_folders)
            if cmd_modify.dirty():
                commands.append(cmd_modify)

        else:

            # Create new
            existing_course.assignments.append(assignment)
            commands.append(cmd.CreateCommand(session, assignment))

    return commands


def import_assignments(path_assignments, config):
    """
    Import assignments.

    Args:
        path_assignments(str): Path to the assignments file.
        config(Configuration): OVOK client configuration.
    """
    # Create the OVOK Interface client
    ovok = OVOKInterface()
    ovok.config = config

    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    # Check how existing assignments would be changed
    course = ovok.query(orm.Course)
    if not course or len(course) == 0:
        print("Couldn't get Course instance from database, please check "
              "that you have imported the course data!")
        return
    course = course[0]

    # Read assignments from json
    try:
        input_assignments = \
            json.loads(utility.read_file(path_assignments),
                       object_hook=json_decoder.decode)
    except IOError, e:
        print("Couldn't read the input file, err: {}".format(str(e)))
        sys.exit(1)

    commands = prepare_import(ovok, course, input_assignments)
    if len(commands) > 0:
        print("The following operations will be performed: ")
        print commands.describe()
        ans = raw_input('Continue? [y/n] ')
        if ans not in (["Y", "y"]):
            print("User cancellation of script.")
            return
        else:
            commands.execute()
            ovok.session().commit()
            print("Assignments imported.")
    else:
        print("No assignments to import")

if __name__ == '__main__':

    # Parse command line arguments
    parser = argparse.ArgumentParser()

    help_string_assignments = "Path for the JSON file containing course " \
                              "assignments."
    parser.add_argument("assignments",
                        help=help_string_assignments,
                        action="store")

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    # Import Course
    import_assignments(args.assignments, conf)

    logger.info("End of script {}".format(__file__))
