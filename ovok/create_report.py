#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Script for creating a report from a certain course assignment."""

# Libraries
import argparse
import logging
import logging.config
import csv
import os

import ovok_interface
from ovok_interface import orm
from ovok_interface.util import utility
from ovok_interface.util import utility_scripts
from ovok_interface import VerificationError
from ovok_interface import Configuration

# Setup logging
logger = logging.getLogger(__name__)
logging.captureWarnings(True)


def extract_data(ovok, group, assignment):
    """
    Turn groups into dictionaries of data.

    Args:
        ovok(OVOKClientBitBucket): OVOK BB client.
        group(StudentGroup): StudentGroup to check.
        assignment(Assignment): Assignment to check.

    Returns:
        dict: Dictionary of related student data.
    """
    output_dict = {}
    for student in group.students:

        if student.student_id not in output_dict:
            output_dict[student.student_id] = {}

        # Name of student
        name = ", ".join([student.lastname, student.firstnames])

        # Student ID
        student_number = student.student_id

        # BitBucket username
        username = ""
        for uname in student.usernames:
            if uname.platform == "bitbucket":
                username = uname.username
                break
        else:
            username = "NOT REGISTERED"

        # Folder where the has been cloned to
        folder = "PLACEHOLDER"

        comment_link = "NOT FOUND"
        if not group.primary_repository:
            comment_link = "NO REPOSITORY"
        else:

            # Link where the user can comment the assignment on BitBucket
            try:

                namespace = group.primary_repository.owner
                repo_slug = group.primary_repository.name

                # Find link based on Tag
                bb_tag = ovok.get_tag(assignment.tag,
                                      repo_slug,
                                      namespace,
                                      date=assignment.deadline)

                if bb_tag:
                    comment_link = \
                        ovok.get_bb_commit_comment_url(bb_tag.raw_node,
                                                       namespace,
                                                       repo_slug)

            except Exception as e:
                print("Error retrieving BitBucket tags for repository "
                      "{repo}: {err}"
                      .format(repo=group.primary_repository.name,
                              err=str(e)))

        # Placeholder if the assignment fulfills all the minimum reqs
        fulfills_requirements = True

        # Print output
        print(u"Name: {name}, StudentID: {id}, Username: {uname}, "
              "folder: {folder}, status: {stat}, link: {link} \n"
              .format(name=name,
                      id=student_number,
                      uname=username,
                      folder=folder,
                      stat=fulfills_requirements,
                      link=comment_link))

        student_dict = {}
        student_dict["name"] = name.encode('utf-8').strip()
        student_dict["student_id"] = student_number
        student_dict["uname"] = username.encode('utf-8').strip()
        student_dict["group"] = group.id

        student_dict["repository"] = None
        if group.primary_repository:
            student_dict["repository"] = \
                group.primary_repository.name.encode('utf-8').strip()
        student_dict["folder"] = folder
        student_dict["status"] = fulfills_requirements
        student_dict["link"] = comment_link
        output_dict[student.student_id] = student_dict

    return output_dict


def create_report(config,
                  tag_input,
                  namespace,
                  output=None,
                  manualauth=False):
    """Create course report file."""
    key = config["bitbucket"]["consumer_key"]
    secret = config["bitbucket"]["consumer_secret"]

    ovok = ovok_interface.get_client(platform="bitbucket",
                                     consumer_key=key,
                                     consumer_secret=secret)
    ovok.config = config

    try:
        utility_scripts.verify_bitbucket(ovok, manual=manualauth)
    except VerificationError, e:
        print("Bitbucket authentication error: {}".format(str(e)))
        return
    except Exception, e:
        print("Suffered an error while authenticating: {}".format(str(e)))
        return

    # Set course context
    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    # Get Course from database
    course = ovok.session().query(orm.Course).one_or_none()
    if not course:
        print("Couldn't get Course instance from database, please check "
              "that you have imported the course data!")
        return

    assignment = utility_scripts.select_assignment(course.assignments,
                                                   assignment_tag=tag_input)
    if not assignment:
        print("Couldn't get matching assignment, exiting.")
        return

    groups = ovok.query(orm.StudentGroup)
    if len(groups) < 1:
        print("The course has no studentgroups")
        return

    output_dict = {}
    for group in groups:
        output_dict[group.id] = extract_data(ovok, group, assignment)

    # Write output to file
    if not output:
        output = "_".join(["report",
                           course.code,
                           str(course.year),
                           assignment.tag])
        output += ".csv"

    if len(output_dict) == 0:
        print("There are no repositories to write, skipping output writing")
    else:
        create_output(output, output_dict)
        print("Output written to {}".format(output))


def create_output(output_path, output_dict):
    """
    Create csv report output to file.

    Args:
        output_path(str): Path of the output file.
        output_dict(dict): Dictionary of dictionaries containing output data.
    """
    try:

        with open(output_path, 'wb') as output:

            keys = output_dict.values()[0].values()[0].keys()
            writer = csv.DictWriter(output, fieldnames=keys)

            writer.writeheader()
            for dict_student in output_dict.values():
                for dict_repo in dict_student.values():
                    writer.writerow(dict_repo)

    except Exception as e:
        print("Error writing output: {}".format(repr(e)))

if __name__ == "__main__":

    # Parse command line arguments

    parser = argparse.ArgumentParser()

    group_help_string = "Git Tag of the assignment."
    parser.add_argument("--tag",
                        help=group_help_string,
                        type=str,
                        required=False)

    help_string_course_namespace = "The namespace that owns the repositories."
    parser.add_argument("--namespace",
                        help=help_string_course_namespace,
                        type=str,
                        required=False)

    help_string_output = "Output file path."
    parser.add_argument("--output",
                        help=help_string_output,
                        type=str,
                        required=False)

    oauth_manual_helpstr = "Perform OAuth authentication steps manually."
    parser.add_argument("--manualauth",
                        help=oauth_manual_helpstr,
                        action="store_true",
                        required=False)

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    if args.namespace:
        conf["bitbucket"]["namespace"] = args.namespace

    if conf["bitbucket"]["namespace"] is None:
        args.namespace = raw_input("Please provide the repository namespace "
                                   "(the account which owns the "
                                   "repositories): ")

    # Import student groups
    create_report(conf,
                  args.tag,
                  namespace=args.namespace,
                  output=args.output,
                  manualauth=args.manualauth)

    logger.info("End of script {}".format(__file__))
