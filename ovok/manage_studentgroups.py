#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Script for managing studentgroup members."""

import logging
import logging.config
import argparse
import os

from ovok_interface import OVOKInterface
from ovok_interface import orm
from ovok_interface.util import utility
from ovok_interface.util import utility_scripts
from ovok_interface import commands as cmd
from ovok_interface import Configuration

logger = logging.getLogger(__name__)


def transfer_to_new(ovok, student):
    """
    Transfer the student to a new group.

    Args:
        ovok(OVOKInterface): OVOK client.
        student(Student): Student to transfer.
    """
    session = ovok.session()

    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()

    for group in student.groups:
        commands.append(cmd.LeaveGroupCommand(session, group, student))

    group = orm.StudentGroup()
    group.students.append(student)

    commands.append(cmd.CreateCommand(session, group))
    commands.append(cmd.JoinGroupCommand(session, group, student))

    print("The following actions are to be taken")
    print commands.describe()
    ans = raw_input("Continue? [y/n] ")
    if ans in ["y", "Y"]:
        commands.execute()
        session.commit()
    else:
        print("Cancelling transfer")
        return


def transfer_to_existing(ovok, student):
    """
    Transfer student to an existing group.

    Args:
        ovok(OVOKInterface): OVOK client.
        student(Student): Student to transfer.
    """
    groups = ovok.query(orm.StudentGroup)
    if len(groups) == 0:
        print("There are no available groups to transfer to.")
        return

    print("Groups available for transfer:")
    for index, grp in enumerate(groups):
        print("{index}) Group: {grp}".format(index=index, grp=grp.id))
    print("Go back with {}".format(len(groups)))

    user_input = read_input(max_index=len(groups))
    if user_input == len(groups):
        return
    target_group = groups[user_input]

    session = ovok.session()

    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()

    for group in student.groups:
        commands.append(cmd.LeaveGroupCommand(session, group, student))

    commands.append(cmd.JoinGroupCommand(session, target_group, student))

    print("The following actions are to be taken")
    print commands.describe()
    ans = raw_input("Continue? [y/n] ")
    if ans in ["y", "Y"]:
        commands.execute()
        session.commit()
    else:
        print("Cancelling transfer")
        return


def remove_from_group(ovok, student):
    """
    Make a student leave a group.

    Args:
        ovok(OVOKInterface): OVOK client.
        student(Student): Student to manage.
    """
    if len(student.groups) == 0:
        print("Student doesn't belong to a group.")
        return

    print(u"Leave group: ")
    for index, grp in enumerate(student.groups):
        print("{index}) Group: {grp}".format(index=index, grp=grp.id))
    print("Go back with {}".format(str(len(student.groups))))

    user_input = read_input(max_index=len(student.groups))
    if user_input == len(student.groups):
        return
    target_group = student.groups[user_input]

    session = ovok.session()

    commands = cmd.CommandList()
    commands.describer = cmd.CommandDescriber()

    commands.append(cmd.LeaveGroupCommand(session, target_group, student))

    print("The following actions are to be taken")
    print commands.describe()
    ans = raw_input("Continue? [y/n] ")
    if ans in ["y", "Y"]:
        commands.execute()
        session.commit()
    else:
        print("Cancelling transfer")
        return


def manage_student(ovok, student):
    """
    Manage a students groups.

    Args:
        ovok(OVOKInterface): OVOK client.
        student(Student): Student to transfer.
    """
    print(u"{name} belongs to groups:"
          .format(name=student.get_readable_name()))
    for group in student.groups:
        print("Group {}".format(group.id))

    run = True
    while run:

        print("0) Transfer to new group")
        print("1) Transfer to existing group")
        print("2) Remove from group")
        print("3) Go back")

        exit_index = 3
        print("Exit with {}".format(exit_index))

        user_input = read_input(max_index=3)

        if user_input == exit_index:
            run = False
        elif user_input == 0:
            transfer_to_new(ovok, student)
        elif user_input == 1:
            transfer_to_existing(ovok, student)
        elif user_input == 2:
            remove_from_group(ovok, student)


def manage_groups(config, manualauth=False):
    """
    Manage studentgroups.

    Args:
        conf(Configuration): Configuration settings for OVOK client.
        manualauth(bool): Skip automatic OAuth and do it manually if True.
    """
    ovok = OVOKInterface()
    ovok.config = config

    context = utility_scripts.select_context(ovok)
    if not context:
        print("Couldn't get a course context, please make sure that you have "
              "imported courses.")
        return
    ovok.set_context(context)

    students = ovok.query(orm.Student)

    run = True
    while run:

        print("Students in the course:")
        for index, student in enumerate(students):
            print(u"{index}) {name}"
                  .format(index=index,
                          name=student.get_readable_name()))

        exit_index = len(students)
        print("Exit with {}".format(exit_index))

        user_input = read_input(max_index=len(students))
        if user_input == exit_index:
            run = False
        else:
            manage_student(ovok, students[user_input])

    groups = ovok.query(orm.StudentGroup)
    create_output(groups,
                  context.course_code,
                  context.course_year)


def create_output(groups, course, year):

    try:

        filename = "_".join([str(course), str(year)])
        filename += ".txt"
        with open(filename, "w") as output:

            for group in groups:
                for student in group.students:

                    # Get student username
                    uname = None
                    for username in student.usernames:
                        if username.platform.lower().strip() == "bitbucket":
                            uname = username.username
                            uname += "\n"

                    if uname:
                        output.write(uname)

                # Separate groups with new line
                output.write("\n")

        print("Output written to {}".format(filename))

    except Exception as e:
        print(e)
        return


def read_input(max_index=None):
    """Read user input."""
    if max_index and max_index < 0:
        raise Exception("Incorrect maximum value provided")

    selected_index = -1
    while selected_index < 0:
        try:

            read_input = int(raw_input())
            if max_index:
                if read_input <= max_index:
                    selected_index = read_input
                else:
                    print("The provided value was too high, please try again.")
            else:
                selected_index = read_input

        except Exception:
            print("Couldn't read the provided value, please try again.")

    return selected_index

if __name__ == "__main__":

    # Parse command line arguments

    parser = argparse.ArgumentParser()

    # Authenticate manually?
    oauth_manual_helpstr = "Perform OAuth authentication steps manually."
    parser.add_argument("--manualauth",
                        help=oauth_manual_helpstr,
                        action="store_true",
                        required=False)

    help_string_configure = "Configure from yaml file"
    parser.add_argument("--config",
                        help=help_string_configure,
                        action="store",
                        required=False)

    args = parser.parse_args()

    # Get configuration file path for automatic config
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    autoconf_path = os.path.join(curr_dir, Configuration.DEFAULT_FILENAME)

    # Create a configuration from specified path or automatically from
    # default config file
    conf = Configuration()
    try:
        conf.from_dict(utility.load_config(args.config or autoconf_path))
    except IOError, e:
        print("Unable to read configuration file, err: {}".format(str(e)))

    # Configure logging
    if "logging" in conf:
        try:
            logging.config.dictConfig(conf["logging"])
        except ValueError:
            print("Couldn't configure logging module, make sure your "
                  "logging configuration is valid. Error: {}".format(str(e)))

    logger.info("Running script {}".format(__file__))

    # Import student groups
    manage_groups(conf, manualauth=args.manualauth)

    logger.info("End of script {}".format(__file__))
