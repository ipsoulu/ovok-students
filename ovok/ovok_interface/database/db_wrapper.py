# -*- coding: utf-8 -*-
"""Wrapper module for database access."""

import logging
from sqlalchemy import and_
from sqlalchemy.orm import class_mapper

from ovok_orm_definitions import Person
from ovok_orm_definitions import Student
from ovok_orm_definitions import StudentGroup
from ovok_orm_definitions import Username


logger = logging.getLogger(__name__)


def wrap_session(func):
    """
    Wrap the database call with a new session.

    Args:
        function: The function to decorate.

    Returns:
        function: Wrapped function.
    """
    def session_wrapper(session, *args, **kwargs):
        """
        Wrap the function in a database session.

        Args:
            session(Session): The session to use in calling the function.
            *args: Arguments to pass.
            **kwargs: Keyword arguments to pass.

        Returns:
            The wrapped value of the function.
        """
        try:

            # Run the function with the specified database session.
            retval = func(session, *args, **kwargs)
            session.commit()
            logger.debug("Session commited")

        except:

            # In case there is an exception, rollback the session.
            session.rollback()
            logger.debug("Session rolled back")
            raise

        return retval

    return session_wrapper


def execute(session, clause, **kwargs):
    """
    Execute SQL clause.

    Args:
        session(Session): Database session to run the query against.
        clause(Executable or str): Query to execute.

    Returns:
        ResultProxy: SQLAlchemy ResultProxy.
    """
    return session.execute(clause, **kwargs)


@wrap_session
def delete(session, obj):
    """
    Delete the object from the database session.

    Args:
        session(Session): The session to use in calling the function.
        obj(object): Object to delete from the database.
    """
    session.delete(obj)


@wrap_session
def get_or_create(session, model, **kwargs):
    """
    Return the object instance from database.

    If the object doesn't exist, create it. Otherwise update object attributes.

    Args:
        session(Session): The session to use in calling the function.
        model (class): ORM class you wish to retrieve.
        **kwargs: The necessary attributes needed to construct
                  the object.

    Returns:
        object: The retrieved or inserted object.
    """
    # Assign primary key values from kwargs to filters dict for correct queries
    primary_keys = [key.name for key in class_mapper(model).primary_key]
    filters = {}
    for key in primary_keys:
        if key in kwargs:
            filters[key] = kwargs[key]

    # Check if the instance exists in the database
    instance = session.query(model).filter_by(**filters).first()
    if instance:

        # The instance exists in database, so update its attributes

        # Check all attributes
        for attr in kwargs:
            if hasattr(instance, attr):
                value = getattr(instance, attr)
                # if value is list:
                #     for item in value:
                #         pass
                if value != kwargs[attr]:
                    logger.debug(u"Setting {model} instance to attribute "
                                 "{attr} from {old} to {new}"
                                 .format(model=model,
                                         attr=attr,
                                         old=value,
                                         new=kwargs[attr]))
                    setattr(instance, attr, kwargs[attr])

                else:
                    # print("Attribute doesn't need updating")
                    pass

        # logger.debug("Updated existing {model} instance to database"
        #              .format(model=model))

        # Return the correct instance
        return instance

    else:

        # The model doesn't exist yet, so create it
        instance = model(**kwargs)
        session.add(instance)
        logger.debug("Added a new {model} instance to database"
                     .format(model=model))
        return instance


@wrap_session
def merge(session, obj):
    """
    Create a record if it is missing from the database.

    Please note that merge uses the primary key for comparing existing
    objects in the database.

    Args:
        session(Session): The session to use in calling the function.
        obj (object): Object you wish to store in the database

    Returns:
        object: The merged object.
    """
    return session.merge(obj)


@wrap_session
def merge_bulk(session, objects):
    """
    Create multiple records if they are missing from the database.

    Please note that merge uses the primary key for comparing existing
    objects in the database.

    Args:
        session(Session): The session to use in calling the function.
        objects (list): Objects you wish to store in the database

    Returns:
        object: The merged objects.
    """
    # A better way to do this?
    # http://stackoverflow.com/questions/25955200/sqlalchemy-performing-a-bulk-upsert-if-exists-update-else-insert-in-postgr

    merged_objs = []
    for obj in objects:
        merged_objs.append(session.merge(obj))

    return merged_objs


@wrap_session
def get_class_objects(session, obj_class):
    """
    Get all Students for a specific course.

    Args:
        session(Session): The session to use in calling the function.
        obj_class(Class): ORM class to query instances of.

    Returns:
        list: List of class objects in the course database.
    """
    return session.query(obj_class).all()


@wrap_session
def get_groups_without_repositories(session):
    """
    Get groups without repositories for certain course.

    Args:
        session(Session): The session to use in calling the function.

    Returns:
        list: List of StudentGroup objects.
    """
    return session.query(StudentGroup) \
        .filter(and_(~StudentGroup.repositories.any())) \
        .all()


@wrap_session
def get_username(session, email, platform):
    """
    Get the platform specific username for person.

    Args:
        session(Session): The session to use in calling the function.
        email(str): Email address of the person.
        platform(str): Platform of the username.

    Returns:
        str or None: The username if it exists, otherwise None.
    """
    person = session.query(Person) \
        .filter(Person.email == email) \
        .one()

    for username in person.usernames:
        if username.platform == platform:
            return username
    else:
        return None


@wrap_session
def get_student_for_username(session, username, platform):
    """Return the person/student with the username."""
    subq = session.query(Username.person_id) \
                  .filter(and_(Username.username == username,
                               Username.platform == platform)) \
                  .subquery()

    student = session.query(Student) \
                     .filter(Student.email == subq) \
                     .first()

    return student


@wrap_session
def get_student_for_student_id(session, student_id):
    """Return the person/student with the username."""
    return session.query(Student) \
        .filter(Student.student_id == student_id) \
        .first()


@wrap_session
def get_person_for_email(session, email):
    """Return the person/student with the username."""
    return session.query(Person) \
        .filter(Person.email == email) \
        .first()


@wrap_session
def get_object(session, model, **kwargs):
    """Get model instance by id."""
    # instance = session.query(model).filter_by(**filters).first()
    return session.query(model) \
        .filter_by(kwargs) \
        .first()
