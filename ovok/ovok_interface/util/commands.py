# -*- coding: utf-8 -*-
"""Commands used to carry out operations in the OVOK system."""

import abc
import collections
import logging

from sqlalchemy import inspect
from sqlalchemy.orm.util import class_mapper
from sqlalchemy.orm.collections import InstrumentedList

from ..core import RepositoryError
from ..git_helper.git_helper import GitStrategy

logger = logging.getLogger(__name__)


class AbstractDescriber(object):
    """Abstract describer baseclass."""

    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def __init__(self):
        """Construct a describer."""
        pass

    @abc.abstractproperty
    def describe(self, obj):
        """Describe an object."""
        return

    @abc.abstractproperty
    def describe_list(self, objs):
        """Describe a list of objects."""
        return


class BaseDescriber(AbstractDescriber):
    """Basic describer that only uses str() do describe objects."""

    def __init__(self):
        """Construct a describer."""
        pass

    def describe(self, obj):
        """Describe an object."""
        return str(obj)

    def describe_list(self, objs):
        """Describe a list of objects."""
        if len(objs) == 0:
            return "The command list is empty."
        return "\n".join([self.describe(obj) for obj in objs])


class CommandDescriber(AbstractDescriber):
    """Class that describes Command objects in understandable strings."""

    def __init__(self):
        """Construct a describer."""
        self._mappings = {
            CreateCommand: self._describe_create_command,
            JoinGroupCommand: self._describe_join_command,
            LeaveGroupCommand: self._describe_leave_command,
            CreateRepositoryCommand: self._describe_create_repository_command,
            DeleteRepositoryCommand: self._describe_delete_repository_command,
            RepositoryPermissionCommand:
            self._describe_repository_permission_command,
            ModifyCommand: self._describe_modify_command,
            DownloadAssignmentCommand: self._describe_download_command,
            CheckoutAssignmentCommand: self._describe_checkout_command,
            CreateWebhookCommand: self._describe_create_webhook_command
        }

    def _describe_default(self, obj):
        return unicode(obj)

    def _describe_create_command(self, obj):
        return self._inspect_obj(obj.obj)

    def _inspect_obj(self, obj, level=0):
        indent = u""
        for x in range(0, level):
            indent += u"    "
        inspector = inspect(obj)
        prefix = u"\n{indent}Create a new '{objtype}' instance with " \
            "following attributes: \n" \
            .format(indent=indent,
                    objtype=obj.__class__.__name__)

        ending = ""
        for column in inspector.attrs:
            if isinstance(column.value, InstrumentedList):
                ending += u"{}: ".format(column.key)
                for item in column.value:
                    ending += u"" \
                        .join([ending,
                               self._inspect_obj(item,
                                                 level=(level + 1))])
            else:

                ending += u"{indent}{attr}: {value}\n" \
                    .format(indent=indent,
                            attr=column.key,
                            value=column.value)

        return u"".join([prefix, ending, ""])

    def _describe_join_command(self, obj):
        grp_id = obj.group.id if obj.group.id else id(obj.group)
        return u"   Student '{student}' will join '{group} {identifier}'" \
            .format(student=obj.student.get_readable_name(),
                    group=obj.group.__class__.__name__,
                    identifier=grp_id)

    def _describe_leave_command(self, obj):
        grp_id = obj.group.id if obj.group.id else id(obj.group)
        return u"   Student '{student}' will leave '{group} {identifier}'" \
            .format(student=obj.student.get_readable_name(),
                    group=obj.group.__class__.__name__,
                    identifier=grp_id)

    def _describe_create_repository_command(self, obj):
        return u"Create a new '{}' instance with temp id '{}'" \
            .format(obj.obj.__class__.__name__,
                    id(obj.obj))

    def _describe_delete_repository_command(self, obj):
        repo_id = \
            obj.repository.name if obj.repository.name else id(obj.repository)
        return u"Delete '{}' '{}'" \
            .format(obj.obj.__class__.__name__,
                    id(repo_id))

    def _describe_repository_permission_command(self, obj):
        return u"Give {perm} to {recipient} in repository {repo}" \
            .format(perm=obj.permission.privilege,
                    recipient=obj.permission.username,
                    repo=obj.permission.repository)

    def _describe_modify_command(self, obj):
        prefix = u"Modify {objtype} instance:\n" \
            .format(objtype=obj.obj.__class__.__name__)
        ending = u"\n".join([u"    attribute: {}, from: {}, to: {}"
                             .format(mod["attr"],
                                     mod["old"],
                                     mod["new"])
                             for mod in obj.modifications])

        return u"".join([prefix, ending])

    def _describe_download_command(self, obj):
        return u"Clone repository {repo} to {path}" \
            .format(repo=obj.url, path=obj.path)

    def _describe_checkout_command(self, obj):
        return u"Checkout Tag {tag} from path {path}" \
            .format(tag=obj.tag, path=obj.path)

    def _describe_create_webhook_command(self, obj):
        prefix = u"Create webhook to repository {repo} for events: " \
            .format(obj.repository)
        ending = u"\n".join([u"   event {}".format(unicode(event))
                            for event in obj.events])

        return u"".join([prefix, ending])

    def describe(self, obj):
        """Describe an object."""
        type_to_describe = type(obj)
        try:
            if type_to_describe not in self._mappings:
                return self._describe_default(obj)
            return self._mappings[type_to_describe](obj)
        except Exception:
            logger.exception("Unable to describe object {}"
                             .format(obj))
        return None

    def describe_list(self, objs):
        """Describe a list of objects."""
        if len(objs) == 0:
            return "The command list is empty."
        return u"\n".join([self.describe(obj) for obj in objs])


class CommandList(collections.MutableSequence):
    """Convenience collection for command objects."""

    def __init__(self, commands=None, describer=BaseDescriber()):
        """
        Construct a CommandList.

        Kwargs:
            commands(list(Command)): List of commands.
        """
        self._commands = commands if commands else []
        self._describer = describer
        self._current = 0
        self._successful_commands = []

    def execute(self, raise_exceptions=False):
        """Execute all commands."""
        self._successful_commands = []
        for command in self._commands:

            try:
                command.execute()
            except Exception:
                if raise_exceptions:
                    raise
                logger.exception("Exception while executing command")
            else:
                self._successful_commands.append(command)

    def undo(self):
        """Undo all commands."""
        for command in reversed(self._successful_commands):
            command.undo

    def get_successful(self):
        return self._successful_commands

    def describe(self):
        return self._describer.describe_list(self._commands)

    @property
    def describer(self):
        return self._describer

    @describer.setter
    def describer(self, describer):
        self._describer = describer

    def __len__(self):
        return len(self._commands)

    def __getitem__(self, i):
        return self._commands[i]

    def __delitem__(self, i):
        del self._commands[i]

    def __setitem__(self, i, v):
        self._commands[i] = v

    def insert(self, i, v):
        self._commands.insert(i, v)

    def __iter__(self):
        return self

    def next(self):
        try:
            result = self._commands[self._current]
        except IndexError:
            raise StopIteration
        self._current += 1
        return result

    def __str__(self):
        return str(self._commands)


class AbstractCommand(object):
    """Command abstract base class."""

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def execute(self):
        """Execute the command."""
        return

    @abc.abstractmethod
    def undo(self):
        """Undo the command."""
        return


class CreateCommand(AbstractCommand):
    """Command for creating an object."""

    # def __init__(self, session, cls, **kwargs):
    def __init__(self, session, obj):
        """
        Construct a creation command.

        Args:
            session(Session): SQLAlchemy session to add objects in.
            cls(Class): Class object to create.

        Kwargs are passed straight to the object constructor.
        """
        self.session = session
        self.obj = obj

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))
        self.session.add(self.obj)

    def undo(self):
        """Undo the command."""
        self.session.expunge(self.obj)

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        return u"Create({} ({}))".format(self.obj.__class__.__name__,
                                         id(self.obj))


class CreateRepositoryCommand(AbstractCommand):
    """Command for creating a repository."""

    def __init__(self, ovok, group, repo_name, repo_namespace, session=None):
        """
        Create a GitRepository for a StudentGroup.

        Args:
            ovok(OVOKClientBitBucket): OVOK client that creates the repository.
            group(StudentGroup): StudentGroup to create the repository for.

        Kwargs:
            session(Session): Session to use. If not specified, the session
                              will be requested from the OVOK client.
        """
        self.ovok = ovok
        self.group = group
        self.repo_name = repo_name
        self.repo_namespace = repo_namespace
        self.session = session if session else ovok.session()
        self.repository = None

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))

        # Create the repository
        try:
            repository = self.ovok.create_repository(self.group,
                                                     self.repo_name,
                                                     self.repo_namespace)
        except RepositoryError:
            repo_repr = "/".join([self.repo_namespace, self.repo_name])
            logger.exception("Error in creating repository {}"
                             .format(repo_repr))
            raise

        self.repository = repository

        # Append to the groups list of repositories
        if repository not in self.group.repositories:
            self.group.repositories.append(repository)

        # Make the repository as primary for the group
        self.group.primary_repository = repository

        # Update session
        self.session.add(self.group)

    def undo(self):
        """Undo the command."""
        # TODO: Fix this
        pass

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        repo_repr = "/".join([self.repo_namespace, self.repo_name])
        return u"Create({} ({}))".format("GitRepository", repo_repr)


class DeleteRepositoryCommand(AbstractCommand):
    """Command for creating a repository."""

    def __init__(self, ovok, repository, session=None):
        """
        Delete a Repository.

        Args:
            ovok(OVOKClientBitBucket): OVOK client that creates the repository.
            repository(Repository): Repository to delete.

        Kwargs:
            session(Session): Session to use. If not specified, the session
                              will be requested from the OVOK client.
        """
        self.ovok = ovok
        self.repository = repository
        self.session = session if session else ovok.session()

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))

        # Delete the repository
        try:
            self.ovok.delete_repository(self.repository)
        except RepositoryError:
            repo_repr = "/".join([self.repository.owner, self.repository.name])
            logger.exception("Error in creating repository {}"
                             .format(repo_repr))
            raise

        self.session.delete(self.repository)

    def undo(self):
        """Undo the command."""
        # TODO: Fix this
        pass

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        if not self.repository:
            return u"Delete(None)"
        repo_repr = "/".join([self.repository.owner, self.repository.name])
        return u"Delete({} ({}))".format(self.repository.__class__.__name__,
                                         repo_repr)


class RepositoryPermissionCommand(AbstractCommand):
    """Command for setting repository privilege."""

    def __init__(self, ovok, permission, session=None):
        """
        """
        self.ovok = ovok
        self.permission = permission
        self.session = session if session else ovok.session()

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))

        # If the person already owns the repository, don't give him
        # a permission. This will result in a bad query.
        if self.permission.namespace == self.permission.username:
            logger.info("The person {person} already owns the "
                        "repository {repo}. Skipping permission."
                        .format(person=self.permission.username,
                                repo=self.permission.repository))
            return

        try:
            self.ovok.set_repository_permission(self.permission)
        except RepositoryError:
            repo_repr = "/".join([self.permission.namespace,
                                  self.permission.repository])
            logger.exception("Error in granting repository {} privileges "
                             "to {}."
                             .format(repo_repr, self.permission.username))
            raise

    def undo(self):
        """Undo the command."""
        # TODO: Fix this
        pass

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        if self.permission is None:
            return u"Permission(None)"
        repo_repr = "/".join([self.permission.namespace,
                              self.permission.repository])
        return u"Permission({} <- {} | {})".format(repo_repr,
                                                   self.permission.username,
                                                   self.permission.privilege)


class JoinGroupCommand(AbstractCommand):
    """Command for joining a student to a studentgroup."""

    def __init__(self, session, group, student):
        """
        Construct a join command.

        Args:
            session(Session): SQLAlchemy session to add objects in.
            group(StudentGroup): Group to join the student to.
            student(Student): Student to join to the group.
        """
        self.session = session
        self.group = group
        self.student = student

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))
        if self.student not in self.group.students:
            self.group.students.append(self.student)
            self.session.add(self.group)

    def undo(self):
        """Undo the command."""
        if self.student in self.group.students:
            self.group.students.remove(self.student)
            self.session.add(self.group)

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        name = str(self.group) if self.group.id \
            else "New group ({})".format(id(self.group))
        return u"Join({} <- {})" \
            .format(name, self.student.get_readable_name())


class LeaveGroupCommand(AbstractCommand):
    """Command for removing a student from a studentgroup."""

    def __init__(self, session, group, student):
        """
        Construct a leave command.

        Args:
            session(Session): SQLAlchemy session to add objects in.
            group(StudentGroup): Group to join the student to.
            student(Student): Student to join to the group.
        """
        self.session = session
        self.group = group
        self.student = student

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))
        if self.student in self.group.students:
            self.group.students.remove(self.student)
            self.session.add(self.group)

    def undo(self):
        """Undo the command."""
        if self.student not in self.group.students:
            self.group.students.append(self.student)
            self.session.add(self.group)

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        name = str(self.group) if self.group.id \
            else "New group ({})".format(id(self.group))
        return u"Leave({} <- {})" \
            .format(name, self.student.get_readable_name())


class ModifyCommand(AbstractCommand):
    """Command for modifying an object."""

    def __init__(self, session, obj):
        """
        Construct a creation command.

        Args:
            session(Session): SQLAlchemy session to add objects in.
            cls(Class): Class object to create.

        Kwargs are passed straight to the object constructor.
        """
        self.session = session
        self.obj = obj
        self.modifications = []

    def dirty(self):
        """
        Determine if the commands contains modifications.

        Returns:
            bool: True if command contains modifications, otherwise False.
        """
        return len(self.modifications) > 0

    def modify(self, attr, value):
        """Modify an attribute on housed object."""
        if not hasattr(self.obj, attr):
            raise AttributeError("The attribute {} doesn't exist on object {}"
                                 .format(str(attr),
                                         str(self.obj)))

        old_value = getattr(self.obj, attr)
        if old_value != value:
            modification = {"attr": attr, "new": value, "old": old_value}
            self.modifications.append(modification)

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))
        for mod in self.modifications:
            setattr(self.obj, mod["attr"], mod["new"])
        self.session.add(self.obj)

    def undo(self):
        """Undo the command."""
        self.session.expunge(self.obj)

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        # TODO: This needs to be fixed. Currently fails if unicode
        # error is encountered.
        prefix = u"Modify {}:\n".format(unicode(self.obj))
        ending = u"\n".join([u"    attr: {}, from: {}, to: {}"
                             .format(unicode(mod["attr"]),
                                     unicode(mod["old"]),
                                     unicode(mod["new"]))
                             for mod in self.modifications])

        return u"".join([prefix, ending])


class DownloadAssignmentCommand(AbstractCommand):
    """Command for downloading an assignment from Git."""

    def __init__(self,
                 ovok,
                 url,
                 path,
                 git_username,
                 git_password,
                 strategy=GitStrategy.DEFAULT):
        """
        Construct a download command.

        Args:

        """
        self.ovok = ovok
        self.url = url
        self.path = path
        self._git_username = git_username
        self._git_password = git_password
        self.strategy = strategy

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))
        self.ovok.clone_repository(self.url,
                                   self.path,
                                   self._git_username,
                                   self._git_password,
                                   strategy=self.strategy)
        logger.info("Assignment downloaded to {}".format(self.path))

    def undo(self):
        """Undo the command."""
        pass

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        return u"Download({} -> {})".format(self.url, self.path)


class CheckoutAssignmentCommand(AbstractCommand):
    """Command for checking out a Git tag from an existing repository."""

    def __init__(self, ovok, path, tag):
        """
        Construct a download command.

        Args:

        """
        self.ovok = ovok
        self.path = path
        self.tag = tag

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))
        try:
            self.ovok.checkout_tag(self.path, self.tag)
            logger.info("Assignment {} checked out in repository {}"
                        .format(self.tag, self.path))
        except KeyError:
            logger.exception("Unable to checkout tag {tag} for repository "
                             "{repo}. It probably doesn't exist."
                             .format(tag=self.tag,
                                     repo=self.path))

    def undo(self):
        """Undo the command."""
        pass

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        return u"Checkout({} ({}))".format(self.tag, self.path)


class CreateWebhookCommand(AbstractCommand):
    """Command for creating a webhook."""

    def __init__(self,
                 ovok,
                 repository,
                 description,
                 url,
                 events,
                 active=True,
                 session=None):
        self.ovok = ovok
        self.repository = repository
        self.description = description
        self.url = url
        self.events = events
        self.active = active
        self.session = session if session else ovok.session()

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))

        # Create the webhook
        try:
            webhook = \
                self.ovok.create_webhook(self.repository.name,
                                         self.repository.owner,
                                         self.description,
                                         self.url,
                                         self.events,
                                         self.active)
        except Exception:
            logger.exception("Couldn't create a webhook")
            raise
        else:
            if webhook not in self.repository.webhooks:
                self.repository.webhooks.append(webhook)
                self.session.add(self.repository)
            else:
                logger.debug("Adding hook without repository.")
                self.session.add(webhook)

    def undo(self):
        """Undo the command."""
        # TODO: Fix this
        pass

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        repo_repr = "/".join([self.repository.owner, self.repository.name])
        return u"Create({} ({}))".format("Webhook", repo_repr)


class UpdateWebhookCommand(AbstractCommand):
    """Command for updating a webhook."""

    def __init__(self, ovok, webhook, session=None):
        self.ovok = ovok
        self.webhook = webhook
        self.session = session if session else ovok.session()

    def execute(self):
        """Execute the command."""
        logger.debug(u"Executing {}: {}".format(self.__class__.__name__,
                                                self.__unicode__()))

        # Update the webhook
        try:
            self.ovok.update_webhook_data(self.webhook)
        except Exception:
            logger.exception("Couldn't update a webhook")
            raise
        else:
            return self.session.merge(self.webhook)

    def undo(self):
        """Undo the command."""
        # TODO: Fix this
        pass

    def __str__(self):
        """Return human readable string representation."""
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        """Return human readable unicode representation."""
        repo_repr = "/".join([self.webhook.repository.owner,
                              self.webhook.repository.name])
        return u"Update({} ({}))".format("Webhook", repo_repr)
