# -*- coding: utf-8 -*-
"""Class for loading context objects from various databases."""

from abc import ABCMeta
from abc import abstractmethod
import os
import logging
import sys
import inspect
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine import reflection
from sqlalchemy_utils import functions
from sqlalchemy.engine.url import URL
from sqlalchemy.exc import OperationalError

import constants as db_const
from context import Context
from ..database.ovok_orm_definitions import Course
from ..database import ovok_orm_definitions as orm

logger = logging.getLogger(__name__)


class DatabaseContext(object):
    """Abstract baseclass for loading contexts with different SQL drivers."""

    __metaclass__ = ABCMeta
    DRIVER = None

    def __init__(self, username, password, server, port):
        """
        Construct a DatabaseContext.

        Args:
            username(str): Database username.
            password(str): Database password.
            server(str): Server url.
            port(int): Database port in remote host.
        """
        self._username = username
        self._password = password
        self._server = server
        self._port = port

    @classmethod
    def from_url(cls, url):
        """
        Create a DatabaseContext from an SQLAlchemy URL.

        Args:
            url(URL): URL to create the DatabaseContext from.

        Returns:
            DatabaseContext: The created DatabaseContext.
        """
        return cls(url.username,
                   url.password,
                   url.host,
                   url.port)

    @abstractmethod
    def load_contexts(self, **kwargs):
        """Load contexts with designated driver."""
        raise NotImplementedError("Context loading is not yet "
                                  " implemented.")

    @abstractmethod
    def create_context(self, db_url, course, **kwargs):
        """Create a course context with a designated driver."""
        raise NotImplementedError("Context creation is not yet "
                                  " implemented.")

    @classmethod
    def _create_engine(self, db_url, **kwargs):
        return create_engine(db_url,
                             connect_args=kwargs,
                             encoding="utf-8",
                             convert_unicode=True)

    @classmethod
    def create_session(cls, db_url, **kwargs):
        """Create a database session with a designated driver."""
        # Get a session factory from the engine
        engine = cls._create_engine(db_url, **kwargs)
        db_session = scoped_session(sessionmaker(bind=engine))

        logger.debug("Built new session {}".format(db_session))

        # Return a session from the factory
        return db_session()

    @abstractmethod
    def _database_url(self, database_name):
        """
        Build a database url from constructor values.

        Args:
            database_name(str): Name of the database.

        Returns:
            str: URL to the database as string.
        """
        raise NotImplementedError("Database url creation is not "
                                  "yet implemented.")

    @abstractmethod
    def _query_contexts(self, database_url, **kwargs):
        """
        Query Contexts from the specified database.

        Queries Course objects from database and if found, builds
        a Context object for them.

        Args:
            database_url(str): URL of the database as string.

        Returns:
            list(Context): List of contexts found.
        """
        raise NotImplementedError("Context querying is not "
                                  "yet implemented.")


class SQLiteDatabaseContext(DatabaseContext):
    """Class for loading contexts from SQLite databases."""

    DRIVER = db_const.DATABASE_SQLITE

    def __init__(self, username, password, server, port):
        """
        Construct a SQLiteDatabaseContext.

        Args:
            username(str): Database username.
            password(str): Database password.
            server(str): Server url.
            port(int): Database port in remote host.
        """
        self._username = username
        self._password = password
        self._server = server
        self._port = port

    def load_contexts(self, **kwargs):
        """
        Load contexts from database.

        Returns:
            list(Context): List of loaded contexts.
        """
        contexts = []
        for db_file in os.listdir(self._server):

            # Skipp all non-database files
            if not db_file.endswith(".db") and db_file.startswith("OVOK"):
                continue

            db_url = self._database_url(db_file)
            contexts.extend(self._query_contexts(db_url))

        return contexts

    def create_context(self, db_url, course, **kwargs):
        """
        Create a course context with SQLite.

        Args:
            db_url(URL): SQLAlchemy url to use for the context.
            course(Course): Course to be hosted in the context.

        Returns:
            Context: The created Context.
        """
        # Format url
        if os.path.isdir(db_url.database):
            filename = "_".join(["OVOK", course.code, str(course.year)])
            filename += ".db"
            db_url.database = os.path.join(db_url.database, filename)

        # Create tables
        engine = self._create_engine(db_url, **kwargs)
        if not functions.database_exists(engine.url):
            logger.info("The provided database does not exist, creating new")
            orm.create_database(engine.url)

        # Create the context
        context = Context(course.code,
                          str(course.year),
                          engine.url,
                          course_name=course.name)
        return context

    def _database_url(self, database_name):
        """
        Build a database url from constructor values.

        Args:
            database_name(str): Name of the database.

        Returns:
            URL: SQLAlchemy database URL.
        """
        db_path = os.path.join(self._server, database_name)
        return URL(db_const.DATABASE_SQLITE, database=db_path)

    def _query_contexts(self, database_url, **kwargs):
        """
        Query Contexts from SQLite database.

        Queries Course objects from database and if found, builds
        a Context object for them.

        Args:
            database_url(str): URL of the database as string.

        Returns:
            list(Context): List of contexts found.
        """
        contexts = []

        # Query the residing course data from database
        s = self.create_session(database_url, **kwargs)
        try:
            courses = s.query(Course).all()
        except Exception:
            logger.exception("Unable to query course information")
            raise
        else:

            for course in courses:

                # Create and append the new context
                contexts.append(Context(course.code,
                                        course.year,
                                        database_url,
                                        course_name=course.name))

                logger.debug("Loaded context for {} {} ({})"
                             .format(course.code,
                                     str(course.year),
                                     self.DRIVER))

        return contexts


class MySQLDatabaseContext(DatabaseContext):
    """Class for loading contexts from MySQL databases."""

    DRIVER = db_const.DATABASE_MYSQL

    def __init__(self, username, password, server, port):
        """
        Construct a MySQLDatabaseContext.

        Args:
            username(str): Database username.
            password(str): Database password.
            server(str): Server url.
            port(int): Database port in remote host.
        """
        self._username = username
        self._password = password
        self._server = server
        self._port = port

    def load_contexts(self, **kwargs):
        """
        Load contexts from database.

        Returns:
            list(Context): List of loaded contexts.
        """
        contexts = []

        database_url = self._database_url(None)

        schemas = self._get_schema_names(database_url, **kwargs)
        for schema in schemas:

            # Skip the schemas we shouldn't be touching
            if not schema.startswith("OVOK_"):
                continue

            contexts.extend(self._query_contexts(self._database_url(schema),
                                                 **kwargs))

        return contexts

    def create_context(self, db_url, course, **kwargs):
        """
        Create a course context with MySQL.

        Args:
            db_url(URL): SQLAlchemy url to use for the context.
            course(Course): Course to be hosted in the context.

        Returns:
            Context: The created Context.
        """
        # Make sure some sort of database is selected
        if not db_url.database:
            db_url.database = "_".join(["OVOK", course.code, str(course.year)])

        # Make sure the database exists
        temp_url = URL(db_url.drivername,
                       username=db_url.username,
                       password=db_url.password,
                       host=db_url.host,
                       port=db_url.port,
                       query=db_url.query)
        engine = self._create_engine(temp_url, **kwargs)
        engine.execute("CREATE DATABASE IF NOT EXISTS {}"
                       .format(db_url.database))

        # Create all necessary tables.
        orm.create_database(db_url)

        return Context(course.code,
                       str(course.year),
                       db_url,
                       course_name=course.name)

    def _database_url(self, database_name):
        """
        Build a database url from constructor values.

        Args:
            database_name(str): Name of the database.

        Returns:
            URL: SQLAlchemy datbase URL.
        """
        return URL(db_const.DATABASE_MYSQL,
                   username=self._username,
                   password=self._password,
                   host=self._server,
                   port=self._port,
                   database=database_name)

    def _query_contexts(self, database_url, **kwargs):
        contexts = []

        # Query the residing course data from database
        s = self.create_session(database_url, **kwargs)
        try:
            courses = s.query(Course).all()
        except Exception:
            logger.exception("Unable to query course information")
            raise
        else:

            for course in courses:

                # Create and append the new context
                contexts.append(Context(course.code,
                                        course.year,
                                        database_url,
                                        course_name=course.name))

                logger.debug("Loaded context for {} {} ({})"
                             .format(course.code,
                                     str(course.year),
                                     self.DRIVER))

        return contexts

    def _get_schema_names(self, db_url, **kwargs):
        engine = self._create_engine(db_url, **kwargs)
        inspector = reflection \
            .Inspector \
            .from_engine(engine)
        return inspector.get_schema_names()


class ContextLoader(object):
    """Class for loading all configured course contexts."""

    def __init__(self):
        """Construct a ContextLoader."""
        self.database_mappings = {}

        classmembers = inspect.getmembers(sys.modules[__name__],
                                          inspect.isclass)
        for (name, value) in classmembers:
            if issubclass(value, DatabaseContext) \
               and value is not DatabaseContext:
                self.database_mappings[value.DRIVER] = value

    def load_contexts(self, db_dict, **kwargs):
        """
        Load contexts from database.

        Args:
            db_dict(dict(dict)): Dictionary of dictionaries. First level
                                 keys are driver names and second level
                                 values are database definitions.

        Returns:
            list(Context): List of loaded contexts.

        An example input dictionary would be:
            {'mysql': {
                'username': 'username',
                'password': 'password',
                'server': 'serveraddress',
                'port': 3306}}
        """
        contexts = []
        for key, values in db_dict.iteritems():
            if key in self.database_mappings:
                try:
                    databasecontext = \
                        self.database_mappings[key](values["username"],
                                                    values["password"],
                                                    values["server"],
                                                    values["port"])
                    contexts.extend(databasecontext.load_contexts(**kwargs))
                except OperationalError:
                    logger.exception("Unable to load contexts from {}."
                                     .format(values["server"]))
            else:
                logger.info("Unknown database driver {}".format(key))
        return contexts

    def create_context(self, db_url, course, **kwargs):
        """
        Create a new course context.

        Args:
            db_url(URL): SQLAlchemy url to use for the context.
            course(Course): Course to be hosted in the context.

        Returns:
            Context: The created Context. Returns None if the
                     database driver was not recognized.
        """
        for driver in self.database_mappings.keys():
            if db_url.drivername.lower() == driver.lower():
                return self.database_mappings[driver] \
                    .from_url(db_url) \
                    .create_context(db_url, course, **kwargs)
        return None

    def create_session(self, db_url, **kwargs):
        """
        Create a database session with the provided URL.

        Args:
            db_url(URL): SQLAlchemy database URL.

        Returns:
            Session: SQLAlchemy session for the URL.
        """
        for driver in self.database_mappings.keys():
            if db_url.drivername.lower() == driver.lower():
                return self.database_mappings[driver] \
                    .from_url(db_url) \
                    .create_session(db_url, **kwargs)
        return None
