import logging
import dateutil.parser
import json
from ..database.ovok_orm_definitions import GitRepository
from ..database.ovok_bitbucket_definitions import Changeset
from ..database.ovok_bitbucket_definitions import Commit
from ..database.ovok_bitbucket_definitions import RepositoryPermission
from ..database.ovok_bitbucket_definitions import Tag
from ..database.ovok_bitbucket_definitions import Webhook

# Setup logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

_platform = "bitbucket"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


def _decoding_mappings():
    """ Bind functions to handle specific class decoding """

    return {
        GitRepository.__name__.lower(): _decode_gitrepository,
        RepositoryPermission.__name__.lower(): _decode_repositorypermission,
        Changeset.__name__.lower(): _decode_changeset,
        Commit.__name__.lower(): _decode_commit,
        Tag.__name__.lower(): _decode_tag,
        Webhook.__name__.lower(): _decode_webhook
    }


def decode_as_object(obj, objClass):
    """ Decode json object as python object """

    if not obj:
        return None

    mappings = _decoding_mappings()
    classname = objClass.__name__.lower()

    return mappings[classname](obj) if classname in mappings else None


def _decode_gitrepository(obj):
    """ Returns GitRepository object from json object """

    repo = GitRepository(obj["uuid"],
                         obj["owner"]["username"],
                         obj["description"],
                         obj["name"],
                         _platform)

    repo.scm = obj["scm"]
    repo.fork_policy = obj["fork_policy"]
    repo.is_private = obj["is_private"]
    repo.url = obj["links"]["html"]["href"]

    return repo


def _decode_repositorypermission(obj):
    """ Returns RepositoryPermission object from json object """

    permissions = []
    for perm in obj:
        privilege = perm["privilege"]
        username = perm["user"]["username"]
        repository = perm["repository"]["slug"]
        namespace = perm["repository"]["owner"]["username"]
        permission = RepositoryPermission(username,
                                          privilege,
                                          repository,
                                          namespace=namespace)

        permissions.append(permission)

    return permissions


def _decode_changeset(obj):
    """Return Changeset object from json object."""
    changesets = []
    for chset in obj["changesets"]:

        changeset = Changeset(chset["node"],
                              chset["files"],
                              chset["raw_author"],
                              _string_to_datetime(chset["utctimestamp"]),
                              _string_to_datetime(chset["timestamp"]),
                              chset["author"],
                              chset["raw_node"],
                              chset["parents"],
                              chset["branch"],
                              chset["message"],
                              revision=chset["revision"],
                              size=chset["size"])

        changesets.append(changeset)

    return changesets


def _decode_commit(obj):
    """Return Commit object from json object."""
    commits = []
    for comm in obj["values"]:

        commit = Commit(comm["hash"],
                        comm["repository"],
                        comm["links"],
                        comm["author"],
                        comm["parents"],
                        _string_to_datetime(comm["date"]),
                        comm["message"])

        if "participants" in comm:
            commit.participants = comm["participants"]

        commits.append(commit)

    return commits


def _decode_tag(obj):
    """Return Tag object from json object."""
    tags = []
    for tag in obj:

        tag_attributes = obj[tag]
        tag = Tag(tag,
                  tag_attributes["node"],
                  tag_attributes["files"],
                  tag_attributes["branch"],
                  tag_attributes["raw_author"],
                  _string_to_datetime(tag_attributes["utctimestamp"]),
                  tag_attributes["author"],
                  _string_to_datetime(tag_attributes["timestamp"]),
                  tag_attributes["raw_node"],
                  tag_attributes["parents"],
                  tag_attributes["branch"],
                  tag_attributes["message"],
                  tag_attributes["revision"],
                  size=tag_attributes["size"])

        tags.append(tag)

    return tags


def _decode_webhook(obj):
    """Return webhook object from json object."""
    webhook = Webhook(obj["uuid"],
                      obj["url"],
                      _string_to_datetime(obj["created_at"]),
                      obj["description"],
                      True if obj["active"] else False,
                      json.dumps(obj["events"]),
                      str(obj["subject"]))

    return webhook


def _string_to_datetime(datetime_string):
    """
    Convert datetime object to string.

    The datetime is returned as naive UTC.

    Args:
        datetime_obj(datetime): datetime object to convert.

    Returns:
        str: Converted datetime object.
    """
    # Parse UTC time from input
    datetime_utc = dateutil.parser.parse(datetime_string)

    # Strip timezone from the utc time and return it as naive UTC
    datetime_naive_utc = datetime_utc.replace(tzinfo=None)
    return datetime_naive_utc
